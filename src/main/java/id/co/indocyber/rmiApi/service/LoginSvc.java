package id.co.indocyber.rmiApi.service;

import id.co.indocyber.rmiApi.dto.LoginVMDto;
import id.co.indocyber.rmiApi.dto.RegImeiDto;

import java.util.HashMap;


public interface LoginSvc {
	public HashMap<String, Object> login (LoginVMDto loginVMDto); 
	public HashMap<String, Object> registerImei (RegImeiDto dto); 
}
