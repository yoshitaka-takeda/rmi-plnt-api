package id.co.indocyber.rmiApi.service.impl;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.rmiApi.dao.SptaDao;
import id.co.indocyber.rmiApi.dto.TrackingDto;
import id.co.indocyber.rmiApi.entity.MobSpta;
import id.co.indocyber.rmiApi.entity.MobWeighbridgeResult;
import id.co.indocyber.rmiApi.service.DriverSvc;

@Service("driverSvc")
@Transactional
public class DriverSvcImpl implements DriverSvc {
	
	@Autowired
	SptaDao sptaDao;
	
	@Override
	public HashMap<String, Object> loading(TrackingDto dto) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		MobSpta spta = sptaDao.findSptaById(dto.getSptaNum());
		try {
			if (spta != null) {
				spta.setLoadingSugarcaneLatitude(dto.getLatitude());
				spta.setLoadingSugarcaneLongitude(dto.getLongitude());
				spta.setLoadingSugarcaneBy(dto.getUsername());
				spta.setLoadingSugarcaneDate(now);
				sptaDao.save(spta);
				
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> destination(TrackingDto dto) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		MobSpta spta = sptaDao.findSptaById(dto.getSptaNum());
		try {
			if (spta != null) {
				spta.setDestinationLatitude(dto.getLatitude());
				spta.setDestinationLongitude(dto.getLongitude());
				spta.setDestinationArrivedBy(dto.getUsername());
				spta.setDestinationArrivedDate(now);
				sptaDao.save(spta);
				
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> finish(TrackingDto dto) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		MobSpta spta = sptaDao.findSptaById(dto.getSptaNum());
		MobWeighbridgeResult weighbridgeResult = sptaDao.findResultById(dto.getSptaNum());
		try {
			if (weighbridgeResult != null) {
				if (spta != null) {
					spta.setExitLatitude(dto.getLatitude());
					spta.setExitLongitude(dto.getLongitude());
					spta.setExitBy(dto.getUsername());
					spta.setExitDate(now);
					sptaDao.save(spta);
					
					result.put("status", 1);
					result.put("message", "Sukses");
					result.put("Object", null);
				}
			} else {
				result.put("status", 4);
				result.put("message", "Belum Timbang Keluar");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
}
