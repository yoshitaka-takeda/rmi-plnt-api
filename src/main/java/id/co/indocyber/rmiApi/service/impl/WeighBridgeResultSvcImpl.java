package id.co.indocyber.rmiApi.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.api.client.util.DateTime;

import id.co.indocyber.rmiApi.dao.MobFarmerDao;
import id.co.indocyber.rmiApi.dao.MobWeighbridgeResultDao;
import id.co.indocyber.rmiApi.dto.MobWeighbridgeResultDto;
import id.co.indocyber.rmiApi.dto.WeighBridgeResultDriverDto;
import id.co.indocyber.rmiApi.entity.MobWeighbridgeResult;
import id.co.indocyber.rmiApi.service.WeighBridgeResultSvc;

@Service("weighBridgeResultSvc")
@Transactional
public class WeighBridgeResultSvcImpl implements WeighBridgeResultSvc {
	
	@Autowired
	MobWeighbridgeResultDao weighbridgeResultDao;
	
	@Autowired
	MobFarmerDao farmerDao;
	
	@Override
	public HashMap<String, Object> listWeighBridgeResult(String username, String fromDate, String toDate) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		try {
			String farmerCode = farmerDao.findRefCodeByUsername(username);
			List<Object[]> list = weighbridgeResultDao.listWeighbridgeResultNew(farmerCode, fromDate, toDate);
			List<MobWeighbridgeResultDto> dtos = new ArrayList<MobWeighbridgeResultDto>();
			if (list.isEmpty() || list.size() == 0) {
				result.put("status", Integer.valueOf(99));
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
			for (Object[] data : list) {
				DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
				MobWeighbridgeResultDto dto = new MobWeighbridgeResultDto();
				dto.setNoSpta((Integer) data[0] == null ? 0 : (Integer) data[0]);
				dto.setKodeMitra((String) data[1] == null ? "" : (String) data[1]);
				dto.setNamaMitra((String) data[2] == null ? "" : (String) data[2]);
				dto.setNoPolisi((String) data[3] == null ? "" : (String) data[3]);
				dto.setNamaSupir((String) data[4] == null ? "" : (String) data[4]);
				dto.setNamaPetani((String) data[5] == null ? "" : (String) data[5]);
				dto.setNoKontrak((String) data[6] == null ? "" : (String) data[6]);
				dto.setContractNo2((String) data[7] == null ? "" : (String) data[7]);
				try {
					String strTanggalMasuk = dateFormat.format((Date) data[8]);
					dto.setTanggalMasuk(strTanggalMasuk + " " + (String) data[9]);
					String strTangalKeluar = dateFormat.format((Date) data[10]);
					dto.setTangalKeluar(strTangalKeluar + " " + (String) data[11]);
				} catch (Exception e) {
					dto.setTanggalMasuk("");
					dto.setTangalKeluar("");
				}
				dto.setStatus((String) data[12] == null ? "" : (String) data[12]);
				dto.setPosPantau((String) data[13]== null ? "" : (String) data[13]);
				dto.setBeratKotor(((BigDecimal) data[14]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[14]).doubleValue());
				dto.setNetto(((BigDecimal) data[15]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[15]).doubleValue());
				dto.setBeratKendaraan(((BigDecimal) data[16]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[16]).doubleValue());
				dto.setBeratSetelahKualitas(((BigDecimal) data[17]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[17]).doubleValue());
				dto.setRafaksi(((BigDecimal) data[18]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[18]).doubleValue());
				dto.setJumlahDiBayar(((BigDecimal) data[19]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[19]).doubleValue());
				
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", Integer.valueOf(1));
				result.put("message", "Sukses");
				result.put("Object", dtos);
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listWeighBridgeResultDriver(String username, String fromDate, String toDate) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			String driverId = farmerDao.findIdCodeByUsername(username);
			List<Object[]> list = weighbridgeResultDao.listWeighbridgeResultDriverNew(driverId, fromDate, toDate);
			if (list.isEmpty() || list.size() == 0) {
				result.put("status", Integer.valueOf(99));
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			} else {
				List<WeighBridgeResultDriverDto> dtos = new ArrayList<>();
				for (Object[] data : list) {
					DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
					WeighBridgeResultDriverDto dto = new WeighBridgeResultDriverDto();
					dto.setNoSpta((Integer) data[0] == null ? 0 : (Integer) data[0]);
					dto.setKodeMitra((String) data[1] == null ? "" : (String) data[1]);
					dto.setNamaMitra((String) data[2] == null ? "" : (String) data[2]);
					dto.setNoPolisi((String) data[3] == null ? "" : (String) data[3]);
					dto.setNamaSupir((String) data[4] == null ? "" : (String) data[4]);
					dto.setNamaPetani((String) data[5] == null ? "" : (String) data[5]);
					dto.setNoKontrak((String) data[6] == null ? "" : (String) data[6]);
					dto.setContractNo2((String) data[7] == null ? "" : (String) data[7]);
					try {
						String strTanggalMasuk = dateFormat.format((Date) data[8]);
						dto.setTanggalMasuk(strTanggalMasuk + " " + (String) data[9]);
						String strTangalKeluar = dateFormat.format((Date) data[10]);
						dto.setTangalKeluar(strTangalKeluar + " " + (String) data[11]);
					} catch (Exception e) {
						dto.setTanggalMasuk("");
						dto.setTangalKeluar("");
					}
					dto.setStatus((String) data[12] == null ? "" : (String) data[12]);
					dto.setPosPantau((String) data[13]== null ? "" : (String) data[13]);
					dto.setBeratKotor(((BigDecimal) data[14]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[14]).doubleValue());
					dto.setNetto(((BigDecimal) data[15]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[15]).doubleValue());
					dto.setBeratKendaraan(((BigDecimal) data[16]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[16]).doubleValue());
					dto.setBeratSetelahKualitas(((BigDecimal) data[17]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[17]).doubleValue());
					dto.setRafaksi(((BigDecimal) data[18]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data[18]).doubleValue());
					
					dtos.add(dto);
				}
				if (dtos != null) {
					result.put("status", Integer.valueOf(1));
					result.put("message", "Sukses");
					result.put("Object", dtos);
				}
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
}
