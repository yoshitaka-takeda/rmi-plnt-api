package id.co.indocyber.rmiApi.service;

import java.util.HashMap;

public interface MasterDataSvc {
	public HashMap<String, Object> addressType();

	public HashMap<String, Object> educationType();

	public HashMap<String, Object> farmType();

	public HashMap<String, Object> farmerType();

	public HashMap<String, Object> sugarCaneFarmType();

	public HashMap<String, Object> sugarCaneParamScoring();

	public HashMap<String, Object> sugarCaneType();

	public HashMap<String, Object> vehicle();

	public HashMap<String, Object> vehicleType();

	public HashMap<String, Object> driver();

	public HashMap<String, Object> foundType();

	public HashMap<String, Object> gender();

	public HashMap<String, Object> martialStatus();

	public HashMap<String, Object> religion();

	public HashMap<String, Object> plantingPeriod();

	public HashMap<String, Object> reason();

	public HashMap<String, Object> driverRiwayat();

	public HashMap<String, Object> farmerAndFarm(String username);
}
