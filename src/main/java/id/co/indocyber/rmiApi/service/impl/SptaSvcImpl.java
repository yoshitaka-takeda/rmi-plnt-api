package id.co.indocyber.rmiApi.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import org.json.JSONObject;

import id.co.indocyber.rmiApi.config.ConstantVariabel;
import id.co.indocyber.rmiApi.dao.MVehicleDao;
import id.co.indocyber.rmiApi.dao.MobDriverDao;
import id.co.indocyber.rmiApi.dao.MobRequestForCancelDao;
import id.co.indocyber.rmiApi.dao.SptaDao;
import id.co.indocyber.rmiApi.dao.TokenUserDao;
import id.co.indocyber.rmiApi.dto.CancelSptaDto;
import id.co.indocyber.rmiApi.dto.DriverSptaDto;
import id.co.indocyber.rmiApi.dto.EditSptaDriverTrukDto;
import id.co.indocyber.rmiApi.dto.SptaEditDto;
import id.co.indocyber.rmiApi.dto.SptaHistoryFarmerDto;
import id.co.indocyber.rmiApi.dto.SptaVMDto;
import id.co.indocyber.rmiApi.entity.MobDriver;
import id.co.indocyber.rmiApi.entity.MobRequestForCancel;
import id.co.indocyber.rmiApi.entity.MobSpta;
import id.co.indocyber.rmiApi.entity.MobVehicle;
import id.co.indocyber.rmiApi.service.NotificationSvc;
import id.co.indocyber.rmiApi.service.SptaSvc;

@Service("sptaSvc")
@Transactional
public class SptaSvcImpl implements SptaSvc {
	
	@Autowired
	SptaDao sptaDao;
	@Autowired
	MVehicleDao vehicleDao;
	@Autowired
	MobDriverDao driverDao;
	@Autowired
	MobRequestForCancelDao requestForCancelDao;
	@Autowired
	TokenUserDao tokenUserDao;
	
	@Autowired
	NotificationSvc notificationSvc;
	
	HttpResponse response;
	HttpClient httpClient = HttpClientBuilder.create().build();
	JSONObject jobj = new JSONObject();
	
	@Override
	public HashMap<String, Object> listSpta(String username, String status) {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<Object[]> list = sptaDao.listSpta(username, status);
			List<SptaVMDto> dtos = new ArrayList<>();
			if (list == null) {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			} else {
				for (Object[] data : list) {
					SptaVMDto dto = new SptaVMDto();
					dto.setSptaNum((Integer) data[0]);
					dto.setFarmerCode((Integer) data[1]);
					dto.setQty((BigDecimal) data[2]);
					dto.setStatus((String) data[3] == null ? "" : (String) data[3]);
					dto.setVehicleNo((String) data[4] == null ? "" : (String) data[4]);
					dto.setDriverId((Integer) data[5] == null ? 0 : (int) data[5]);
					dto.setVehicleType((Integer) data[6] == null ? 0 : (int) data[6]);
					
					DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
					try {
						String strTanggalMasuk = dateFormat.format((Date) data[7]);
						dto.setDocumentDate(strTanggalMasuk);
						String strTangalKeluar = dateFormat.format((Date) data[8]);
						dto.setExpiredDate(strTangalKeluar);
					} catch (Exception e) {
						dto.setDocumentDate("");
						dto.setExpiredDate("");
					}
					
					dto.setRendemen(((BigDecimal) data[9]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data[9]);
					dto.setBrix(((BigDecimal) data[10]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data[10]);
					dto.setPol(((BigDecimal) data[11]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data[11]);
					dto.setSource(String.valueOf(data[12]));
					dto.setBaseEntry((Integer) data[13]);
					dto.setBaseLine((Integer) data[14]);
					dto.setReferenceCode((String) data[15]);
					dto.setFarmerName((String) data[16]);
					dto.setContractNo2((String) data[17]);
					dto.setContractNo1((String) data[18]);
					dtos.add(dto);
				}
				
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listHistorySpta(String username) {
		HashMap<String, Object> result = new HashMap<>();
		
		try {
			List<Object[]> list = sptaDao.listHistorySpta(username);
			List<SptaHistoryFarmerDto> dtos = new ArrayList<>();
			for (Object[] data : list) {
				SptaHistoryFarmerDto dto = new SptaHistoryFarmerDto();
				dto.setSptaNum((Integer) data[0]);
				dto.setFarmerCode((Integer) data[1]);
				dto.setQty((BigDecimal) data[2]);
				dto.setStatus((String) data[3] == null ? "" : (String) data[3]);
				dto.setVehicleNo((String) data[4]);
				dto.setDriverId((Integer) data[5] == null ? 0 : (int) data[5]);
				dto.setVehicleType((Integer) data[6] == null ? 0 : (int) data[6]);
				
				DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
				try {
					String strTanggalMasuk = dateFormat.format((Date) data[7]);
					dto.setDocumentDate(strTanggalMasuk);
					String strTangalKeluar = dateFormat.format((Date) data[8]);
					dto.setExpiredDate(strTangalKeluar);
				} catch (Exception e) {
					dto.setDocumentDate("");
					dto.setExpiredDate("");
				}
				
				dto.setRendemen(((BigDecimal) data[9]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data[9]);
				dto.setBrix(((BigDecimal) data[10]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data[10]);
				dto.setPol(((BigDecimal) data[11]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data[11]);
				dto.setSource(String.valueOf(data[12]));
				dto.setBaseEntry((Integer) data[13]);
				dto.setBaseLine((Integer) data[14]);
				dto.setReferenceCode((String) data[15]);
				dto.setFarmerName((String) data[16]);
				Double d17 = ((BigDecimal) data[17]).doubleValue();
				Double d18 = ((BigDecimal) data[18]).doubleValue();
				Double d19 = ((BigDecimal) data[19]).doubleValue();
				Double d20 = ((BigDecimal) data[20]).doubleValue();
				Double d21 = ((BigDecimal) data[21]).doubleValue();
				Double d22 = ((BigDecimal) data[22]).doubleValue();
				if (d17 != 0 && d18 != 0) {
					dto.setLatitude(d17);
					dto.setLongitude(d18);
				} else if (d19 != 0 && d20 != 0) {
					dto.setLatitude(d19);
					dto.setLongitude(d20);
				} else if (d21 != 0 && d22 != 0) {
					dto.setLatitude(d21);
					dto.setLongitude(d22);
				} else {
					dto.setLatitude(0d);
					dto.setLongitude(0d);
				}
				dto.setDriverNo((String) data[23] == null ? "" : (String) data[23]);
				dto.setContractNo2((String) data[24] == null ? "" : (String) data[24]);
				dto.setContractNo1((String) data[25] == null ? "" : (String) data[25]);
				dto.setMonitoringPos((String) data[26] == null ? "" : (String) data[26]);
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> editSpta(SptaEditDto dto, String username) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		String plat = platNo(dto.getVehicleNo()).toUpperCase();
		
		MobSpta spta = sptaDao.findSptaById(dto.getSptaNum());
		MobDriver dataDriver = supir(dto.getNoDriver());
		MobVehicle dataVehicle = truk(plat);
		try {
			if (dataDriver != null) {
				spta.setDriverId(dataDriver.getId());
			} else {
				Map<String, Object> map = addSupir(dto.getNoDriver(), dto.getDriverName(), username, now);
				if (map.get("status").equals(1)) {
					MobDriver driver = supir(dto.getNoDriver());
					spta.setDriverId(driver.getId());
				}
			}
			if (dataVehicle != null) {
				spta.setVehicleNo(dataVehicle.getVehicleNo());
			}else{
				Map<String, Object> map = addTruk(plat, dto.getVehicleType(), username, now);
				if(map.get("status").equals(1)){
					MobVehicle vehicle = truk(plat);
					spta.setVehicleNo(vehicle.getVehicleNo());
				}
			}
			spta.setVehicleType(dto.getVehicleType());
			sptaDao.save(spta);
			updateSptaSAP(spta.getVehicleNo(), dataDriver.getFullname() , spta.getVehicleType().toString(), spta.getSptaNum().toString());
			result.put("status", 1);
			result.put("message", "Sucess");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	public MobDriver supir(String noHp) {
		MobDriver dataDriver = driverDao.findOneByNoHp(cekNoIndo(noHp));
		if (dataDriver != null) {
			return dataDriver;
		} else {
			return null;
		}
	}
	
	public HashMap<String, Object> addSupir(String noHp, String driverName, String username, Date now) {
		HashMap<String, Object> result = new HashMap<>();
		MobDriver driver = new MobDriver();
		try {
			driver.setFullname(driverName);
			driver.setPhoneNum(cekNoIndo(noHp));
			driver.setMartialStatus("1");
			driver.setGender("1");
			driver.setReligion("1");
			driver.setDriverType("Petani");
			driver.setIsActive(Byte.parseByte("1"));
			driver.setCreatedBy(username);
			driver.setCreatedDate(now);
			driver.setModifiedBy(username);
			driver.setModifiedDate(now);
			
			driverDao.save(driver);
			result.put("status", 1);
			result.put("message", "Sucess");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal " + e.getMessage());
			result.put("Object", null);
		}
		return result;
	}
	
	public MobVehicle truk(String plat) {
		MobVehicle dataVehicle = vehicleDao.findOneBy(plat);
		if (dataVehicle != null) {
			return dataVehicle;
		} else {
			return null;
		}
	}
	
	public HashMap<String, Object> addTruk(String plat, Integer vehicleType, String username, Date now) {
		HashMap<String, Object> result = new HashMap<>();
		MobVehicle vehicle = new MobVehicle();
		try {
			vehicle.setVehicleNo(plat);
			vehicle.setVehicleType(vehicleType);
			vehicle.setCreated(username);
			vehicle.setModified(username);
			vehicle.setIsActive(Byte.parseByte("1"));
			vehicle.setModifiedDate(now);
			vehicle.setCreatedDate(now);
			
			vehicleDao.save(vehicle);
			result.put("status", 1);
			result.put("message", "Sucess");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal " + e.getMessage());
			result.put("Object", null);
		}
		return result;
		
	}
	
	public String getToken() {
		HttpGet request = new HttpGet(ConstantVariabel._tokenSync);
		
		String token = "";
		try {
			response = httpClient.execute(request);
			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(jobj.get("data").toString(), HashMap.class);
			token = yourHashMap.get("token").toString();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return token;
	}
	
	public void updateSptaSAP(String vehicleNo, String driverName, String vehicleType, String sptaNum) {
		
		HttpPost post = new HttpPost(ConstantVariabel._updateSpta);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("token", getToken()));
		urlParameters.add(new BasicNameValuePair("vehicle_no", vehicleNo));
		urlParameters.add(new BasicNameValuePair("driver_name", driverName));
		urlParameters.add(new BasicNameValuePair("vehicle_type", vehicleType));
		urlParameters.add(new BasicNameValuePair("spta_num", sptaNum));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(urlParameters));
			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);
			System.out.println(responseString2);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
	
	public String platNo(String plat) {
		String[] platNo = plat.split(" ");
		String resultPlatNo = "";
		for (int i = 0; i < platNo.length; i++) {
			resultPlatNo = resultPlatNo + platNo[i];
		}
		return resultPlatNo;
	}
	
	public String cekNoIndo(String noHp) {
		if (noHp.equals("-")) {
			return null;
		} else if (noHp.substring(0, 2).equals("08")) {
			return "0" + noHp.substring(1);
		} else if (noHp.substring(0, 3).equals("628")) {
			return "0" + noHp.substring(2);
		} else if (noHp.substring(0, 1).equals("8")) {
			return "0" + noHp;
		} else if (noHp.substring(0, 4).equals("+628")) {
			return "0" + noHp.substring(3);
		} else {
			return null;
		}
	}
	
	@Override
	public HashMap<String, Object> cancelSpta(CancelSptaDto dto) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Date now = new Date();
		MobRequestForCancel cekSpta = requestForCancelDao.findOneBy(dto.getSptaNum());
		try {
			if (cekSpta != null) {
				result.put("status", 2);
				result.put("message", "Sudah pernah melakukan request cancel spta");
				result.put("Object", null);
			} else {
				MobRequestForCancel spta = new MobRequestForCancel();
				spta.setSptaNum(dto.getSptaNum());
				spta.setReasonForCancel(dto.getReason());
				spta.setLocationForCancel(dto.getLocationCancel());
				spta.setTaskActivity("03");
				spta.setCreatedBy(dto.getUsername());
				spta.setCreatedDate(now);
				requestForCancelDao.save(spta);
				
				result.put("status", Integer.valueOf(1));
				result.put("message", "Sukses");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listSptaDriver(String username, String status) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		// Date now = new Date();
		List<DriverSptaDto> listSpta = new ArrayList<>();
		try {
			List<Object[]> listObjSpta = sptaDao.listSptaDriver(username, status);
			if (listObjSpta.isEmpty()) {
				result.put("status", Integer.valueOf(99));
				result.put("message", "Data kosong");
				result.put("Object", null);
			} else {
				for (Object[] o : listObjSpta) {
					DriverSptaDto sptaDriver = new DriverSptaDto();
					
					DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
					try {
						String strTanggalMasuk = dateFormat.format((Date) o[0]);
						sptaDriver.setTanggalDokumen(strTanggalMasuk);
						String strTangalKeluar = dateFormat.format((Date) o[1]);
						sptaDriver.setTanggalExpired(strTangalKeluar);
					} catch (Exception e) {
						sptaDriver.setTanggalDokumen("");
						sptaDriver.setTanggalExpired("");
					}
					
					sptaDriver.setSptaNum((Integer) o[2]);
					sptaDriver.setTipePetani((String) o[3] == null ? "" : (String) o[3]);
					sptaDriver.setRegister((Integer) o[4]);
					sptaDriver.setNamaPemilik((String) o[5] == null ? "" : (String) o[5]);
					sptaDriver.setKecamatan((String) o[6] == null ? "" : (String) o[6]);
					sptaDriver.setDesa((String) o[7] == null ? "" : (String) o[7]);
					sptaDriver.setNamaDriver((String) o[8] == null ? "" : (String) o[8]);
					sptaDriver.setNoHp((String) o[9] == null ? "" : (String) o[9]);
					sptaDriver.setNoKendaraan((String) o[10] == null ? "" : (String) o[10]);
					sptaDriver.setTipeTruk((String) o[11] == null ? "" : (String) o[11]);
					if ((String) o[12] != null && (String) o[13] == null && (String) o[14] == null) {
						sptaDriver.setStatus(1);
					} else if ((String) o[12] != null && (String) o[13] != null && (String) o[14] == null) {
						sptaDriver.setStatus(2);
					} else if ((String) o[12] != null && (String) o[13] != null && (String) o[14] != null) {
						sptaDriver.setStatus(3);
					} else {
						sptaDriver.setStatus(0);
					}
					
					sptaDriver.setContractNo1((String) o[15] == null ? "" : (String) o[15]);
					sptaDriver.setContractNo2((String) o[16] == null ? "" : (String) o[16]);
					sptaDriver.setFarmerCode((String) o[17] == null ? "" : (String) o[17]);
					sptaDriver.setCoopName((String) o[18] == null ? "" : (String) o[18]);
					sptaDriver.setMonitoringPos((String) o[19] == null ? "" : (String) o[19]);
					try {
						sptaDriver.setBrix(((BigDecimal) o[20]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[20]);
					} catch (Exception e) {
						sptaDriver.setBrix(BigDecimal.valueOf(0d));
					}
					listSpta.add(sptaDriver);
				}
				result.put("status", Integer.valueOf(1));
				result.put("message", "Sukses");
				result.put("Object", listSpta);
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> editSptaDriverTruk(EditSptaDriverTrukDto dto, String username) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Date now = new Date();
		String plat = platNo(dto.getVehicleNo()).toUpperCase();
		
		MobSpta spta = sptaDao.findSptaById(dto.getSptaNum());
		MobDriver dataDriver = supir(dto.getNoDriver());
		MobVehicle dataVehicle = truk(plat);
		try {
			if (dataDriver != null) {
				spta.setDriverId(dataDriver.getId());
			} else {
				Map<String, Object> map = addSupir(dto.getNoDriver(), dto.getDriverName(), username, now);
				if (map.get("status").equals(1)) {
					MobDriver driver = supir(dto.getNoDriver());
					spta.setDriverId(driver.getId());
				}
			}
			if (dataVehicle != null) {
				spta.setVehicleNo(dataVehicle.getVehicleNo());
			}else{
				Map<String, Object> map = addTruk(plat, dto.getVehicleType(), username, now);
				if(map.get("status").equals(1)){
					MobVehicle vehicle = truk(plat);
					spta.setVehicleNo(vehicle.getVehicleNo());
				}
			}
			spta.setVehicleType(dto.getVehicleType());
			sptaDao.save(spta);
			result.put("status", 1);
			result.put("message", "Sucess");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
}
