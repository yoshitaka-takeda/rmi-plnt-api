package id.co.indocyber.rmiApi.service;

import id.co.indocyber.rmiApi.dto.CancelSptaDto;
import id.co.indocyber.rmiApi.dto.EditSptaDriverTrukDto;
import id.co.indocyber.rmiApi.dto.SptaEditDto;

import java.util.HashMap;

public interface SptaSvc {
	public HashMap<String, Object> listSpta(String username, String status);
	public HashMap<String, Object> listHistorySpta(String username);
	public HashMap<String, Object> editSpta(SptaEditDto dto, String username);
	public HashMap<String, Object> cancelSpta(CancelSptaDto dto);
	public HashMap<String, Object> listSptaDriver(String username, String status);
	public HashMap<String, Object> editSptaDriverTruk(EditSptaDriverTrukDto dto, String username);
}
