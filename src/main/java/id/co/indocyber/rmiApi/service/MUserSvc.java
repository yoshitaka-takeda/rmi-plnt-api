package id.co.indocyber.rmiApi.service;

import id.co.indocyber.rmiApi.dto.ProfileDto;

import java.util.Map;

public interface MUserSvc {
	public Map<String, Object> getProfile(String userName);
	public Map<String, Object> changePassword(String userId, String pass,String newPass);
	public Map<String, Object> updateProfile(ProfileDto profileDto, String email);
}
