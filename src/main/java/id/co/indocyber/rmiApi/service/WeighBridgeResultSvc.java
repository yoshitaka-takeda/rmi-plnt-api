package id.co.indocyber.rmiApi.service;

import java.util.HashMap;

public interface WeighBridgeResultSvc {
	public HashMap<String, Object> listWeighBridgeResult(String username,String fromDate, String toDate);
	public HashMap<String, Object> listWeighBridgeResultDriver(String username,String fromDate, String toDate);
}
