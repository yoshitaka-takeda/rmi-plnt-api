package id.co.indocyber.rmiApi.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.rmiApi.dao.MTaskActivityDao;
import id.co.indocyber.rmiApi.dao.MUserDao;
import id.co.indocyber.rmiApi.dao.MobEmployeeDao;
import id.co.indocyber.rmiApi.dao.MobFarmDao;
import id.co.indocyber.rmiApi.dao.MobFarmerDao;
import id.co.indocyber.rmiApi.dao.MobPaddyFieldDao;
import id.co.indocyber.rmiApi.dao.MobScheduleVisitToFarmerDetailDao;
import id.co.indocyber.rmiApi.dao.MobScheduleVisitToFarmerHeaderDao;
import id.co.indocyber.rmiApi.dao.MobScheduleVisitToFarmerHistoryDao;
import id.co.indocyber.rmiApi.dao.MobStandDao;
import id.co.indocyber.rmiApi.dao.VisitAppraisementDao;
import id.co.indocyber.rmiApi.dao.VisitDao;
import id.co.indocyber.rmiApi.dao.VisitOtherActivityDao;
import id.co.indocyber.rmiApi.dao.VisitScoringDao;
import id.co.indocyber.rmiApi.dto.AllVisitDto;
import id.co.indocyber.rmiApi.dto.AllVisitListDto;
import id.co.indocyber.rmiApi.dto.DelegasiDto;
import id.co.indocyber.rmiApi.dto.FarmDto;
import id.co.indocyber.rmiApi.dto.InputVisitAllDto;
import id.co.indocyber.rmiApi.dto.MobFarmDto;
import id.co.indocyber.rmiApi.dto.MobFarmerDto;
import id.co.indocyber.rmiApi.dto.MobPaddyFieldDto;
import id.co.indocyber.rmiApi.dto.MobScheduleVisitToFarmerHeaderDto;
import id.co.indocyber.rmiApi.dto.MobStandDto;
import id.co.indocyber.rmiApi.dto.NewInputVisitAllDto;
import id.co.indocyber.rmiApi.dto.NewVisitDto;
import id.co.indocyber.rmiApi.dto.NewVisitListDto;
import id.co.indocyber.rmiApi.dto.NewVisitOtherActivityDto;
import id.co.indocyber.rmiApi.dto.NewVisitScoringDto;
import id.co.indocyber.rmiApi.dto.NewVisitStandDto;
import id.co.indocyber.rmiApi.dto.PaddyFieldAreaDto;
import id.co.indocyber.rmiApi.dto.VisitAppraisementDto;
import id.co.indocyber.rmiApi.dto.VisitDto;
import id.co.indocyber.rmiApi.dto.VisitHistoryHeaderDto;
import id.co.indocyber.rmiApi.dto.VisitOtherActivityDto;
import id.co.indocyber.rmiApi.dto.VisitScoringDto;
import id.co.indocyber.rmiApi.entity.MTaskActivity;
import id.co.indocyber.rmiApi.entity.MUser;
import id.co.indocyber.rmiApi.entity.MobEmployee;
import id.co.indocyber.rmiApi.entity.MobFarmer;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitAppraisement;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitOtherActivity;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitScoring;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerDetail;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHeader;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHistory;
import id.co.indocyber.rmiApi.entity.MobStand;
import id.co.indocyber.rmiApi.service.NotificationSvc;
import id.co.indocyber.rmiApi.service.VisitSvc;

@Service("visitSvc")
@Transactional
public class VisitSvcImpl implements VisitSvc {
	
	@Autowired
	VisitDao visitDao;
	@Autowired
	MobFarmDao farmDao;
	@Autowired
	MobFarmerDao farmerDao;
	@Autowired
	MobPaddyFieldDao paddyFieldDao;
	@Autowired
	VisitOtherActivityDao visitOtherActivityDao;
	@Autowired
	VisitScoringDao visitScoringDao;
	@Autowired
	VisitAppraisementDao visitAppraisementDao;
	@Autowired
	MobStandDao mobStandDao;
	@Autowired
	MobScheduleVisitToFarmerHeaderDao mobScheduleVisitToFarmerHeaderDao;
	@Autowired
	MobScheduleVisitToFarmerDetailDao mobScheduleVisitToFarmerDetailDao;
	@Autowired
	MobScheduleVisitToFarmerHistoryDao mobScheduleVisitToFarmerHistoryDao;
	@Autowired
	MTaskActivityDao activityDao;
	@Autowired
	MUserDao mUserDao;
	@Autowired
	MobEmployeeDao employeeDao;
	
	@Autowired
	NotificationSvc notification;
	
	@Override
	public HashMap<String, Object> listVisit(int personId) {
		List<VisitDto> visitDtos = new ArrayList<>();
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String visitDate = dateFormat.format(now);
		String visitExpiredDate = dateFormat.format(now);
		List<Object[]> list = visitDao.getListVisit(personId, visitDate, visitExpiredDate);
		
		DateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy");
		if (list.size() != 0) {
			for (Object[] o : list) {
				VisitDto dto = new VisitDto();
				dto.setVisitNo((String) o[0]);
				dto.setFullname((String) o[1]);
				dto.setFarmerId((int) o[2]);
				dto.setVillage((String) o[3]);
				dto.setFarmerCode((String) o[4]);
				dto.setVisitDate(dateFormat2.format((Date) o[5]));
				dto.setVisitExpiredDate(dateFormat2.format((Date) o[6]));
				dto.setPhotoText((String) o[7] == null ? "" : (String) o[7]);
				dto.setStatus((String) o[8]);
				visitDtos.add(dto);
			}
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", visitDtos);
		} else {
			result.put("status", 99);
			result.put("message", "Data tidak tersedia");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listFarmByFarmer(int farmerId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Object[]> list = this.farmDao.listFarmByFarmer(farmerId);
		List<MobFarmDto> dtos = new ArrayList<MobFarmDto>();
		Double scoring = 0d;
		if (list.size() != 0) {
			for (Object[] o : list) {
				MobFarmDto dto = new MobFarmDto();
				dto.setFarmerId((Integer) o[0] == null ? 0 : (Integer) o[0]);
				dto.setFarmCode((String) o[1] == null ? "" : (String) o[1]);
				dto.setFieldArea(((BigDecimal) o[2]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) o[2]).doubleValue());
				dto.setVillage((String) o[3] == null ? "" : (String) o[3]);
				// dto.setFieldArea(((BigDecimal) o[2]).equals(BigDecimal.ZERO)
				// ? 0d : ((BigDecimal) o[2]).doubleValue());
				
				List<Object[]> list2 = farmDao.listPaddyFieldByFarm((String) o[1]);
				List<PaddyFieldAreaDto> areaDtos = new ArrayList<>();
				for (Object[] ob : list2) {
					PaddyFieldAreaDto areaDto = new PaddyFieldAreaDto();
					areaDto.setPaddyFieldCode((String) ob[0]);
					areaDto.setFieldArea((BigDecimal) ob[1]);
					
					DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
					try {
						String strTanggalMasuk = dateFormat.format((Date) ob[2]);
						areaDto.setPlantingDate(strTanggalMasuk);
					} catch (Exception e) {
						areaDto.setPlantingDate("");
					}
					
					areaDto.setPlantingPeriod((String) ob[3]);
					areaDto.setScoring(scoring == null ? 0 : scoring);
					areaDto.setRanking(ranking(scoring == null ? 0 : scoring));
					areaDtos.add(areaDto);
				}
				dto.setFieldAreaDto(areaDtos);
				dtos.add(dto);
			}
			result.put("status", Integer.valueOf(1));
			result.put("message", "Sukses");
			result.put("Object", dtos);
		} else {
			result.put("status", 99);
			result.put("message", "Data tidak tersedia");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listFarmByFarmerDetail(int farmCode) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public HashMap<String, Object> visitOtherActivity(VisitOtherActivityDto dto, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobScheduleVisitOtherActivity visitOtherActivity = new MobScheduleVisitOtherActivity();
		Date now = new Date();
		try {
			visitOtherActivity.setVisitNo(dto.getVisitNo());
			visitOtherActivity.setActivityText(dto.getActivityText());
			visitOtherActivity.setEntryDate(dto.getEntryDate());
			visitOtherActivity.setCreatedBy(username);
			visitOtherActivity.setCreatedDate(now);
			visitOtherActivity.setIsActive((byte) 1);
			visitOtherActivityDao.save(visitOtherActivity);
			
			MobScheduleVisitToFarmerHeader farmerHeader = mobScheduleVisitToFarmerHeaderDao.findOneBy(dto.getVisitNo());
			farmerHeader.setTaskActivity("00");
			MobScheduleVisitToFarmerHistory farmerHeaderHystory = mobScheduleVisitToFarmerHistoryDao.findOneBy(dto.getVisitNo(), farmerHeader.getFarmerId(), farmerHeader.getAssignTo());
			farmerHeaderHystory.setTaskAction("00");
			
			List<MobScheduleVisitToFarmerDetail> mobScheduleVisitToFarmerDetail = mobScheduleVisitToFarmerDetailDao.getByVisitNo(dto.getVisitNo());
			for (MobScheduleVisitToFarmerDetail o : mobScheduleVisitToFarmerDetail) {
				o.setTaskActivity("03");
				mobScheduleVisitToFarmerDetailDao.save(o);
			}
			
			mobScheduleVisitToFarmerHeaderDao.save(farmerHeader);
			mobScheduleVisitToFarmerHistoryDao.save(farmerHeaderHystory);
			visitOtherActivityDao.save(visitOtherActivity);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> visitScoring(VisitScoringDto dto, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobScheduleVisitScoring scheduleVisitScoring = new MobScheduleVisitScoring();
		Date now = new Date();
		try {
			scheduleVisitScoring.setScheduleVisitDetailId(dto.getScheduleVisitDetailId());
			scheduleVisitScoring.setParamCode(dto.getParamCode());
			scheduleVisitScoring.setParamValue(dto.getParamValue());
			scheduleVisitScoring.setCriteria(dto.getCriteria());
			scheduleVisitScoring.setWeight(dto.getWeight());
			scheduleVisitScoring.setScore(dto.getScore());
			scheduleVisitScoring.setNotes(dto.getNotes());
			scheduleVisitScoring.setIsActive((byte) 1);
			scheduleVisitScoring.setCreatedBy(username);
			scheduleVisitScoring.setCreatedDate(now);
			visitScoringDao.save(scheduleVisitScoring);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> visitAppraisement(VisitAppraisementDto dto, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobScheduleVisitAppraisement scheduleVisitAppraisement = new MobScheduleVisitAppraisement();
		Date now = new Date();
		try {
			scheduleVisitAppraisement.setScheduleVisitDetailId(dto.getScheduleVisitDetailId());
			scheduleVisitAppraisement.setPeriodName(dto.getPeriodName());
			scheduleVisitAppraisement.setCurrentSugarcaneHeight(dto.getTinggiBatangSekarang());
			scheduleVisitAppraisement.setCutdownSugarcaneHeight(dto.getTinggiBatangDitebang());
			scheduleVisitAppraisement.setSugarcaneStemWeight(dto.getBeratBatangperMeter());
			scheduleVisitAppraisement.setWeightPerSugarcane(dto.getBeratPerBatang());
			scheduleVisitAppraisement.setNumberOfStemPerLeng(dto.getJumlahBatangTiapLeng());
			scheduleVisitAppraisement.setLeng_factor_per_HA(dto.getJumlahLengperHA());
			scheduleVisitAppraisement.setNumber_of_sugarcane_per_HA(dto.getJumlahBatangperHA());
			scheduleVisitAppraisement.setProdPerHectarKU(dto.getProdPerHektarKu());
			scheduleVisitAppraisement.setAmountOfProduction(dto.getJumlahProduksiKU());
			scheduleVisitAppraisement.setIsActive((byte) 1);
			scheduleVisitAppraisement.setCuttingSchedule(dto.getJadwalTebangPeriode());
			scheduleVisitAppraisement.setAvgBrixNumber(dto.getRerataAngkaBrix());
			scheduleVisitAppraisement.setRendemenEstimation(dto.getEstimasiRendemen());
			scheduleVisitAppraisement.setKuHablurPerHa(dto.getKuHablurPerHa());
			scheduleVisitAppraisement.setAmountOfHablur(dto.getJumlahHablur());
			scheduleVisitAppraisement.setCreatedBy(username);
			scheduleVisitAppraisement.setCreatedDate(now);
			visitAppraisementDao.save(scheduleVisitAppraisement);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> inputStand(MobStandDto dto, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobStand mobStand = new MobStand();
		Date now = new Date();
		try {
			mobStand.setVisitNo(dto.getVisitNo());
			mobStand.setEntryDate(new Date());
			mobStand.setFarmArea(dto.getFarmArea());
			mobStand.setTonnageEstimation(dto.getTonnageEstimation());
			mobStand.setLastStandTonnage(dto.getLastStandTonnage());
			mobStand.setLastStandArea(dto.getLastStandArea());
			mobStand.setStandTonnage(dto.getStandTonnage());
			mobStand.setStandArea(dto.getStandArea());
			mobStand.setActivityText(null);
			mobStand.setIsActive((byte) 1);
			mobStand.setCreatedBy(username);
			mobStand.setCreatedDate(now);
			
			MobScheduleVisitToFarmerHeader farmerHeader = mobScheduleVisitToFarmerHeaderDao.findOneBy(dto.getVisitNo());
			farmerHeader.setTaskActivity("00");
			
			MobScheduleVisitToFarmerHistory farmerHeaderHystory = mobScheduleVisitToFarmerHistoryDao.findOneBy(dto.getVisitNo(), farmerHeader.getFarmerId(), farmerHeader.getAssignTo());
			farmerHeaderHystory.setTaskAction("00");
			
			List<MobScheduleVisitToFarmerDetail> mobScheduleVisitToFarmerDetail = mobScheduleVisitToFarmerDetailDao.getByVisitNo(dto.getVisitNo());
			for (MobScheduleVisitToFarmerDetail o : mobScheduleVisitToFarmerDetail) {
				o.setTaskActivity("03");
				mobScheduleVisitToFarmerDetailDao.save(o);
			}
			mobScheduleVisitToFarmerHeaderDao.save(farmerHeader);
			mobScheduleVisitToFarmerHistoryDao.save(farmerHeaderHystory);
			mobStandDao.save(mobStand);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> inputVisitHeader(MobScheduleVisitToFarmerHeaderDto dto, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobScheduleVisitToFarmerHeader mobScheduleVisitToFarmerHeader = new MobScheduleVisitToFarmerHeader();
		Date now = new Date();
		try {
			// mobScheduleVisitToFarmerHeader.setEstimateHarvestTime(dto
			// .getEstimateHarvestTime());
			// mobScheduleVisitToFarmerHeader.setPlanText(dto.getPlanText());
			// mobScheduleVisitToFarmerHeader.setEvaluationText(dto
			// .getEvaluationText());
			mobScheduleVisitToFarmerHeader.setModifiedBy(username);
			mobScheduleVisitToFarmerHeader.setModifiedDate(now);
			mobScheduleVisitToFarmerHeaderDao.save(mobScheduleVisitToFarmerHeader);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listOfFarm(int farmerId, String farmCode) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Object[]> list = farmDao.listOfFarm(farmerId, farmCode);
		List<MobPaddyFieldDto> dtos = new ArrayList<MobPaddyFieldDto>();
		for (Object[] o : list) {
			MobPaddyFieldDto dto = new MobPaddyFieldDto();
			dto.setId((Integer) o[0] == null ? 0 : (Integer) o[0]);
			dto.setFarmerId((Integer) o[1] == null ? 0 : (Integer) o[1]);
			dto.setFarmCode((String) o[2] == null ? "" : (String) o[2]);
			dto.setPaddyFieldCode((String) o[3] == null ? "" : (String) o[3]);
			dto.setLineNum((Integer) o[4] == null ? 0 : (Integer) o[4]);
			dto.setFieldArea(((BigDecimal) o[5]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[5]);
			dto.setLatitude(((BigDecimal) o[6]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[6]);
			dto.setLongitude(((BigDecimal) o[7]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[7]);
			dto.setWeightEstimation(((BigDecimal) o[8]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[8]);
			dto.setSugarcaneType((String) o[9] == null ? "" : (String) o[9]);
			dto.setAgeOfSugarcane((Integer) o[10] == null ? 0 : (Integer) o[10]);
			dto.setPlantingDate((Date) o[11]);
			dto.setSugarcaneSpecies((String) o[12] == null ? "" : (String) o[12]);
			dto.setSugarcaneScore(((BigDecimal) o[13]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[13]);
			dto.setSugarcaneQty(((BigDecimal) o[14]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[14]);
			dto.setPlantingPeriod((String) o[15] == null ? "" : (String) o[15]);
			dto.setWeighEstimation(((BigDecimal) o[16]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) o[16]);
			dto.setStatus((Character) o[17] == null ? 0 : (Character) o[17]);
			dto.setReferenceCode((String) o[18] == null ? "" : (String) o[18]);
			dtos.add(dto);
		}
		result.put("status", Integer.valueOf(1));
		result.put("message", "Sukses");
		result.put("Object", dtos);
		return result;
	}
	
	@Override
	public HashMap<String, Object> listVisitDetail(String visitNo, int farmerId, int personId) {
		HashMap<String, Object> result = new HashMap<>();
		FarmDto farmDto = new FarmDto();
		Double tot = 0d;
		Double scoring = 0d;
		List<Object[]> listo = visitDao.getListVisitDetail(personId, visitNo);
		
		if (listo == null || listo.size() == 0) {
			result.put("status", 99);
			result.put("message", "Data tidak tersedia");
			result.put("Object", null);
		} else {
			for (Object[] obj : listo) {
				farmDto.setVisitNo((String) obj[0] == null ? "" : (String) obj[0]);
				farmDto.setFullName((String) obj[1] == null ? "" : (String) obj[1]);
				farmDto.setFarmerId((Integer) obj[2] == null ? 0 : (Integer) obj[2]);
				farmDto.setVillage((String) obj[3] == null ? "" : (String) obj[3]);
				DateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy");
				farmDto.setVisitDate(dateFormat2.format((Date) obj[4]));
				farmDto.setVisitExpiredDate(dateFormat2.format((Date) obj[5]));
				farmDto.setPhotoText((String) obj[6] == null ? "" : (String) obj[6]);
				farmDto.setTaskActivity((String) obj[7] == null ? "" : (String) obj[7]);
				//farmDto.setFarmCode((String) obj[8] == null ? "" : (String) obj[8]); masih ragu bener ga si
				farmDto.setFarmCode((String) obj[9] == null ? "" : (String) obj[9]);
			}
		}
		
		List<Object[]> list = farmDao.detailVisitFarm(visitNo);
		if (list == null || list.size() == 0) {
			result.put("status", 99);
			result.put("message", "Data tidak tersedia");
			result.put("Object", null);
		} else {
			for (Object[] o : list) {
				farmDto.setFarmVillage((String) o[0]);
				
				List<Object[]> list2 = farmDao.listPaddyField(visitNo);
				List<PaddyFieldAreaDto> areaDtos = new ArrayList<>();
				for (Object[] ob : list2) {
					PaddyFieldAreaDto areaDto = new PaddyFieldAreaDto();
					scoring = visitDao.getScoringByDetail((Integer) ob[2]);
					areaDto.setPaddyFieldCode((String) ob[0]);
					areaDto.setFieldArea((BigDecimal) ob[1]);
					areaDto.setVisitDetailId((int) ob[2]);
					
					DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
					try {
						String strTanggalMasuk = dateFormat.format((Date) ob[3]);
						areaDto.setPlantingDate(strTanggalMasuk);
					} catch (Exception e) {
						areaDto.setPlantingDate("");
					}
					areaDto.setPlantingPeriod((String) ob[4]);
					
					tot += ((BigDecimal) ob[1]).doubleValue();
					areaDto.setScoring(scoring == null ? 0 : scoring);
					areaDto.setRanking(ranking(scoring == null ? 0 : scoring));
					areaDtos.add(areaDto);
				}
				farmDto.setFieldAreaTotal(tot);
				farmDto.setAreaDto(areaDtos);
			}
		}
		// System.out.println("rank " + ranking(scoring));
		if ((listo.size() != 0) && (list.size() != 0)) {
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", farmDto);
		} else {
			result.put("status", 99);
			result.put("message", "Data tidak tersedia");
			result.put("Object", null);
		}
		return result;
	}
	
	private Double ranking(Double score) {
		List<Double> listData = visitDao.getRankingByDetail();
		Object[] arr = listData.toArray();
		Double ranking = 0d;
		for (int i = 0; i < arr.length; i++) {
			System.out.print("Buah ke-" + (i + 1) + ": ");
			// ranking = (i).a
			// belum fix ranking :)
			
		}
		return ranking;
	}
	
	@Override
	public HashMap<String, Object> visitAll(AllVisitListDto dto, String username, int appraisement, int scoring) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		List<AllVisitDto> list = dto.getAllVisitDtos();
		try {
			for (AllVisitDto data : list) {
				
				// set data new
				if ((data.getPeriodName()).equals(null) || (data.getPeriodName()).equals("") || data.getPeriodName().length() == 0) {
					appraisement = 0;
				} else {
					appraisement = 1;
				}
				
				if (data.getVisitScoring() == null || data.getVisitScoring().size() == 0) {
					scoring = 0;
				} else {
					scoring = 1;
				}
				
				// Taksasi
				MobScheduleVisitAppraisement scheduleVisitAppraisement = new MobScheduleVisitAppraisement();
				if (appraisement == 1) {
					
					try {
						scheduleVisitAppraisement.setScheduleVisitDetailId(data.getScheduleVisitDetailId());
						scheduleVisitAppraisement.setPeriodName(data.getPeriodName());
						scheduleVisitAppraisement.setCurrentSugarcaneHeight(data.getTinggiBatangSekarang());
						scheduleVisitAppraisement.setCutdownSugarcaneHeight(data.getTinggiBatangDitebang());
						scheduleVisitAppraisement.setSugarcaneStemWeight(data.getBeratBatangperMeter());
						scheduleVisitAppraisement.setWeightPerSugarcane(data.getBeratPerBatang());
						scheduleVisitAppraisement.setNumberOfStemPerLeng(data.getJumlahBatangTiapLeng());
						scheduleVisitAppraisement.setLeng_factor_per_HA(data.getJumlahLengperHA());
						scheduleVisitAppraisement.setNumber_of_sugarcane_per_HA(data.getJumlahBatangperHA());
						scheduleVisitAppraisement.setProdPerHectarKU(data.getProdPerHektarKu());
						scheduleVisitAppraisement.setAmountOfProduction(data.getJumlahProduksiKU());
						scheduleVisitAppraisement.setIsActive((byte) 1);
						scheduleVisitAppraisement.setCuttingSchedule(data.getJadwalTebangPeriode());
						scheduleVisitAppraisement.setAvgBrixNumber(data.getRerataAngkaBrix());
						scheduleVisitAppraisement.setRendemenEstimation(data.getEstimasiRendemen());
						scheduleVisitAppraisement.setKuHablurPerHa(data.getKuHablurPerHa());
						scheduleVisitAppraisement.setAmountOfHablur(data.getJumlahHablur());
						scheduleVisitAppraisement.setCreatedBy(username);
						scheduleVisitAppraisement.setCreatedDate(now);
					} catch (Exception e) {
						result.put("status", Integer.valueOf(0));
						result.put("message", e);
						result.put("Object", null);
					}
				}
				
				MobScheduleVisitToFarmerDetail mobScheduleVisitToFarmerDetail = mobScheduleVisitToFarmerDetailDao.findOne(data.getScheduleVisitDetailId());
				try {
					mobScheduleVisitToFarmerDetail.setEstimateHarvestTime(data.getEstimateHarvestTime());
					mobScheduleVisitToFarmerDetail.setPlanText(data.getPlanText());
					mobScheduleVisitToFarmerDetail.setEvaluationText(data.getEvaluationText());
					mobScheduleVisitToFarmerDetail.setLatitude(data.getLatitude());
					mobScheduleVisitToFarmerDetail.setLongitude(data.getLongitude());
					mobScheduleVisitToFarmerDetail.setTaskActivity("00");
				} catch (Exception e) {
					result.put("status", Integer.valueOf(0));
					result.put("message", e);
					result.put("Object", null);
				}
				
				MobScheduleVisitToFarmerHeader mobScheduleVisitToFarmerHeader = mobScheduleVisitToFarmerHeaderDao.findOneBy(data.getVisitNo());
				try {
					mobScheduleVisitToFarmerHeader.setTaskActivity("00");
					mobScheduleVisitToFarmerHeader.setModifiedBy(username);
					mobScheduleVisitToFarmerHeader.setModifiedDate(now);
				} catch (Exception e) {
					result.put("status", Integer.valueOf(0));
					result.put("message", e);
					result.put("Object", null);
				}
				
				MobScheduleVisitToFarmerHistory farmerHeaderHystory = mobScheduleVisitToFarmerHistoryDao.findOneBy(data.getVisitNo(), mobScheduleVisitToFarmerHeader.getFarmerId(),
						mobScheduleVisitToFarmerHeader.getAssignTo());
				farmerHeaderHystory.setTaskAction("00");
				
				MobScheduleVisitScoring scheduleVisitScoring = new MobScheduleVisitScoring();
				List<VisitScoringDto> listD = data.getVisitScoring();
				try {
					if (scoring == 1) {
						for (VisitScoringDto o : listD) {
							scheduleVisitScoring.setScheduleVisitDetailId(o.getScheduleVisitDetailId());
							scheduleVisitScoring.setParamCode(o.getParamCode());
							scheduleVisitScoring.setParamValue(o.getParamValue());
							scheduleVisitScoring.setCriteria(o.getCriteria());
							scheduleVisitScoring.setWeight(o.getWeight());
							scheduleVisitScoring.setScore(o.getScore());
							scheduleVisitScoring.setNotes(o.getNotes());
							scheduleVisitScoring.setIsActive((byte) 1);
							scheduleVisitScoring.setCreatedBy(username);
							scheduleVisitScoring.setCreatedDate(now);
							
							this.visitScoringDao.save(scheduleVisitScoring);
						}
					}
					
					if (appraisement == 1) {
						this.visitAppraisementDao.save(scheduleVisitAppraisement);
					}
					mobScheduleVisitToFarmerDetailDao.save(mobScheduleVisitToFarmerDetail);
					mobScheduleVisitToFarmerHeaderDao.save(mobScheduleVisitToFarmerHeader);
					mobScheduleVisitToFarmerHistoryDao.save(farmerHeaderHystory);
					
					result.put("status", Integer.valueOf(1));
					result.put("message", "Sukses");
					result.put("Object", null);
				} catch (Exception e) {
					result.put("status", Integer.valueOf(0));
					result.put("message", e);
					result.put("Object", null);
				}
				
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		
		return result;
	}
	
	@Override
	public HashMap<String, Object> visitHistory(String search, String dateFrom, String dateTo, int personId) {
		HashMap<String, Object> result = new HashMap<>();
		List<VisitHistoryHeaderDto> dtos = new ArrayList<>();
		search = "%" + search + "%";
		List<Object[]> list = visitDao.getListVisitHistory(personId, search, dateFrom, dateTo);
		for (Object[] o : list) {
			VisitHistoryHeaderDto dto = new VisitHistoryHeaderDto();
			dto.setVisitNo((String) o[0]);
			dto.setFullname((String) o[1]);
			dto.setFarmerId((int) o[2]);
			dto.setVillage((String) o[3]);
			dto.setPhotoText((String) o[4] == null ? "" : (String) o[4]);
			MTaskActivity activity = activityDao.findOneBy((String) o[5]);
			dto.setStatus(activity.getCode());
			dto.setStatusSort(activity.getShortText());
			dto.setStatusLong(activity.getLongText());
			Integer totalFarm = visitDao.getTotalFarmByDetail((String) o[0]);
			dto.setTotalFarm(totalFarm);
			dtos.add(dto);
		}
		result.put("status", 1);
		result.put("message", "Sukses");
		result.put("Object", dtos);
		return result;
	}
	
	@Override
	public HashMap<String, Object> listFarmer(String username) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			Integer personId = mUserDao.getIdUser(username);
			List<Integer> id = getAllMyTeam(personId);
			List<Integer> farmerId = employeeDao.findFarmer(id);
			List<Object[]> list = farmerDao.getFarmerByEmployeeByRiki(farmerId);
			List<MobFarmerDto> dtos = new ArrayList<MobFarmerDto>();
			if (list.size() == 0) {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", dtos);
			} else {
				for (Object[] data : list) {
					MobFarmerDto dto = new MobFarmerDto();
					dto.setId((Integer) data[0] == null ? 0 : (Integer) data[0]);
					dto.setFullname((String) data[1] == null ? "" : (String) data[1]);
					dto.setVillage((String) data[2] == null ? "" : (String) data[2]);
					dto.setFarmerCode((String) data[3] == null ? "" : (String) data[3]);
					dto.setPhoto_text((String) data[4] == null ? "" : (String) data[4]);
					dtos.add(dto);
				}
				if (dtos != null) {
					result.put("status", Integer.valueOf(1));
					result.put("message", "Sukses");
					result.put("Object", dtos);
				}
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	public List<Integer> getAllMyTeam(Integer personId) {
		List<Integer> listParam1 = new ArrayList<>();
		listParam1.add(personId);
		List<MobEmployee> list1 = employeeDao.findManagedEmployees(listParam1);
		
		List<Integer> listId = new ArrayList<>();
		for (MobEmployee e : list1) {
			listId.add(e.getId());
		}
		
		List<MobEmployee> list2 = new ArrayList<>();
		if (listId != null && listId.size() > 0) {
			for (int i = 1; i < 10; i++) {
				list2 = employeeDao.findManagedEmployees(listId);
				if (list2.size() > 0) {
					for (MobEmployee f : list2) {
						listId.add(f.getId());
					}
				}
			}
		}
		
		listId.add(personId);
		
		LinkedHashSet<Integer> hashSet = new LinkedHashSet<>(listId);
		List<Integer> listOutput = new ArrayList<>(hashSet);
		return listOutput;
	}
	
	@Override
	public HashMap<String, Object> ListVisitOtherActivity(String visitNo) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			MobScheduleVisitOtherActivity list = visitOtherActivityDao.findActive(visitNo);
			if (list != null) {
				VisitOtherActivityDto dto = new VisitOtherActivityDto();
				dto.setActivityText(list.getActivityText());
				
				result.put("status", Integer.valueOf(1));
				result.put("message", "Sukses");
				result.put("Object", dto);
			} else {
				result.put("status", Integer.valueOf(2));
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listStand(String visitNo, Integer farmerId, String farmCode) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			MobStandDto mobStandDto = new MobStandDto();
			BigDecimal farmArea = mobStandDao.getFieldArea(farmerId, visitNo);
			BigDecimal tonnageEstimation = BigDecimal.ZERO;
			BigDecimal lastStandTonnage = BigDecimal.ZERO;
			BigDecimal lastStandArea = BigDecimal.ZERO;
			
			if (farmArea != null) {
				mobStandDto.setFarmArea(farmArea);
			} else {
				// asdsf
			}
			
			List<String> listPaddyFieldCode = mobStandDao.getListPaddyFieldCode(farmCode);
			List<String> listId = new ArrayList<>();
			if (listPaddyFieldCode != null) {
				for (String data : listPaddyFieldCode) {
					listId.add(data);
				}
			}
			
			if (listId != null) {
				for (String data : listId) {
					BigDecimal tonnage = mobStandDao.getTonnage(data);
					if (tonnage != null) {
						tonnageEstimation = tonnageEstimation.add(tonnage);
					} else {
						tonnageEstimation = tonnageEstimation.add(BigDecimal.ZERO);
					}
				}
			}
			mobStandDto.setTonnageEstimation(tonnageEstimation);
			
			List<Object[]> lastStand = mobStandDao.getLastStand(farmCode);
			if (lastStand != null) {
				for (Object[] data : lastStand) {
					lastStandTonnage = (BigDecimal) data[0];
					lastStandArea = (BigDecimal) data[1];
				}
			} else {
				lastStandTonnage = tonnageEstimation;
				lastStandArea = farmArea;
			}
			mobStandDto.setLastStandTonnage(lastStandTonnage);
			mobStandDto.setLastStandArea(lastStandArea);
			mobStandDto.setVisitNo("");
			mobStandDto.setEntryDate("");
			mobStandDto.setStandTonnage(BigDecimal.ZERO);
			mobStandDto.setStandArea(BigDecimal.ZERO);
			mobStandDto.setActivityText("");
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", mobStandDto);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listHistoryStand(String visitNo) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			MobStand list = mobStandDao.historyStand(visitNo);
			if (list != null) {
				MobStandDto dto = new MobStandDto();
				dto.setVisitNo(list.getVisitNo() == null ? "" : list.getVisitNo());
				dto.setFarmArea(list.getFarmArea());
				dto.setTonnageEstimation(list.getTonnageEstimation());
				dto.setLastStandArea(list.getLastStandArea());
				dto.setLastStandTonnage(list.getLastStandTonnage());
				dto.setStandTonnage(list.getStandTonnage());
				dto.setStandArea(list.getStandArea());
				
				result.put("status", Integer.valueOf(1));
				result.put("message", "Sukses");
				result.put("Object", dto);
			} else {
				result.put("status", Integer.valueOf(2));
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
			
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listVisitHistoryHeader(String visitNo, String paddyFieldCode) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		AllVisitDto allVisitDto = new AllVisitDto();
		try {
			List<Object[]> objectsHeader = mobScheduleVisitToFarmerHeaderDao.getScheduleVisitToFarmerHeader(visitNo,paddyFieldCode);
			if (objectsHeader != null) {
				for (Object[] o : objectsHeader) {
					allVisitDto.setEstimateHarvestTime((Date) o[0]);
					allVisitDto.setEvaluationText((String) o[1]);
					allVisitDto.setPlanText((String) o[2]);
				}
			} else {
				result.put("status", Integer.valueOf(1));
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
			List<Object[]> appraisementDto = visitAppraisementDao.getAppraisement(visitNo, paddyFieldCode);
			if (appraisementDto != null) {
				for (Object[] o : appraisementDto) {
					allVisitDto.setPeriodName((String) o[0]);
					allVisitDto.setTinggiBatangSekarang((BigDecimal) o[1]);
					allVisitDto.setTinggiBatangDitebang((BigDecimal) o[2]);
					allVisitDto.setBeratBatangperMeter((BigDecimal) o[3]);
					allVisitDto.setJumlahBatangTiapLeng((Integer) o[4]);
					allVisitDto.setJumlahLengperHA((Integer) o[5]);
					allVisitDto.setJumlahBatangperHA((BigDecimal) o[6]);
					allVisitDto.setScheduleVisitDetailId((Integer) o[7]);
					allVisitDto.setBeratPerBatang((BigDecimal) o[8]);
					allVisitDto.setProdPerHektarKu((BigDecimal) o[9]);
					allVisitDto.setJumlahProduksiKU((BigDecimal) o[10]);
					allVisitDto.setJadwalTebangPeriode((String) o[11]);
					allVisitDto.setRerataAngkaBrix((BigDecimal) o[12]);
					allVisitDto.setEstimasiRendemen((BigDecimal) o[13]);
					allVisitDto.setKuHablurPerHa((BigDecimal) o[14]);
					allVisitDto.setJumlahHablur((BigDecimal) o[15]);
										
				}
			} else {
				result.put("status", Integer.valueOf(1));
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
			List<Object[]> scoring = visitAppraisementDao.getScoring(visitNo, paddyFieldCode);
			if (scoring != null) {
				List<VisitScoringDto> dtos = new ArrayList<>();
				for (Object[] o : scoring) {
					VisitScoringDto dto = new VisitScoringDto();
					dto.setScheduleVisitDetailId((Integer) o[0]);
					dto.setParamCode((String) o[1]);
					dto.setParamValue((String) o[2]);
					dto.setCriteria((String) o[3]);
					dto.setWeight((BigDecimal) o[4]);
					dto.setScore((BigDecimal) o[5]);
					dto.setNotes((String) o[6]);
					dtos.add(dto);
				}
				allVisitDto.setVisitScoring(dtos);
			}
			
			if (appraisementDto == null) {
				allVisitDto.setPeriodName("");
				allVisitDto.setTinggiBatangSekarang(BigDecimal.ZERO);
				allVisitDto.setTinggiBatangDitebang(BigDecimal.ZERO);
				allVisitDto.setBeratBatangperMeter(BigDecimal.ZERO);
				allVisitDto.setBeratPerBatang(BigDecimal.ZERO);
				allVisitDto.setJumlahBatangTiapLeng(0);
				allVisitDto.setJumlahLengperHA(0);
				allVisitDto.setJumlahBatangperHA(BigDecimal.ZERO);
				allVisitDto.setProdPerHektarKu(BigDecimal.ZERO);
				allVisitDto.setJumlahProduksiKU(BigDecimal.ZERO);
				allVisitDto.setJadwalTebangPeriode("");
				allVisitDto.setRerataAngkaBrix(BigDecimal.ZERO);
				allVisitDto.setEstimasiRendemen(BigDecimal.ZERO);
				allVisitDto.setKuHablurPerHa(BigDecimal.ZERO);
				allVisitDto.setJumlahHablur(BigDecimal.ZERO);
				//tambah
				allVisitDto.setScheduleVisitDetailId(0);
			}
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", allVisitDto);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> inputVisitAll(InputVisitAllDto inputVisitAll, String username, int otherActivity, int stand, int visit, int appraisement, int scoring) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		if (otherActivity == 1 && stand == 1 && visit == 0) {
			try {
				visitOtherActivity(inputVisitAll.getOtherActivityDto(), username);
				inputStand(inputVisitAll.getStandDto(), username);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (otherActivity == 1 && visit == 1 && stand == 0) {
			try {
				visitOtherActivity(inputVisitAll.getOtherActivityDto(), username);
				visitAll(inputVisitAll.getVisitDto(), username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (stand == 1 && visit == 1 && otherActivity == 0) {
			try {
				inputStand(inputVisitAll.getStandDto(), username);
				visitAll(inputVisitAll.getVisitDto(), username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (otherActivity == 1 && stand == 1 && visit == 1) {
			try {
				visitOtherActivity(inputVisitAll.getOtherActivityDto(), username);
				inputStand(inputVisitAll.getStandDto(), username);
				visitAll(inputVisitAll.getVisitDto(), username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> listStandNewVisit(Integer farmerId, String farmCode) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			BigDecimal area = mobStandDao.getNewFieldArea(farmerId, farmCode);
			MobStand mobStand = mobStandDao.getStand(farmCode);
			MobStandDto mobStandDto = new MobStandDto();
			if (mobStand == null) {
				mobStandDto.setFarmArea(area);
				mobStandDto.setLastStandArea(BigDecimal.ZERO);
				mobStandDto.setLastStandTonnage(BigDecimal.ZERO);
				mobStandDto.setTonnageEstimation(BigDecimal.ZERO);
				mobStandDto.setVisitNo("");
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					String strTanggalMasuk = dateFormat.format(new Date());
					mobStandDto.setEntryDate(strTanggalMasuk);
					
				} catch (Exception e) {
					mobStandDto.setEntryDate("");
				}
				mobStandDto.setStandTonnage(BigDecimal.ZERO);
				mobStandDto.setStandArea(BigDecimal.ZERO);
				mobStandDto.setActivityText("");
				
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", mobStandDto);
			} else {
				mobStandDto.setLastStandArea(mobStand.getStandArea().equals(null) ? BigDecimal.ZERO : mobStand.getStandArea());
				mobStandDto.setTonnageEstimation(BigDecimal.ZERO);
				mobStandDto.setLastStandTonnage(mobStand.getStandTonnage().equals(null) ? BigDecimal.ZERO : mobStand.getStandTonnage());
				mobStandDto.setFarmArea(area.equals(null) ? BigDecimal.ZERO : area);
				
				mobStandDto.setVisitNo("");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					String strTanggalMasuk = dateFormat.format(new Date());
					mobStandDto.setEntryDate(strTanggalMasuk);
					
				} catch (Exception e) {
					mobStandDto.setEntryDate("");
				}
				mobStandDto.setStandTonnage(BigDecimal.ZERO);
				mobStandDto.setStandArea(BigDecimal.ZERO);
				mobStandDto.setActivityText("");
				
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", mobStandDto);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> newInputVisitAll(NewInputVisitAllDto inputVisitAll, String username, int otherActivity, int stand, int visit, int appraisement, int scoring) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Date now = new Date();
		String visitNo = "%" + generateVIsitNo();
		String lastNoVisit = mobScheduleVisitToFarmerHeaderDao.getLastVisitNoHeader(visitNo);
		MobFarmer farmer = farmerDao.findOneByFarmerCode(inputVisitAll.getFarmerCode());
		MUser user = mUserDao.findUserByUsername(username);
		String newVisitNo = "";
		if (lastNoVisit != null) {
			String[] lastVisit = lastNoVisit.split("/");
			newVisitNo = generateVIsitNo(Integer.parseInt(lastVisit[0]) + 1);
		} else {
			newVisitNo = generateVIsitNo(1);
		}
		
		try {
			MobScheduleVisitToFarmerHeader header = new MobScheduleVisitToFarmerHeader();
			header.setVisitNo(newVisitNo);
			header.setVisitDate(now);
			header.setVisitExpiredDate(now);
			header.setFarmerId(farmer.getId());
			header.setAssignTo(user.getPersonId());
			header.setNotes(null);
			header.setTaskActivity("00");
			header.setIsActive(Byte.parseByte("1"));
			header.setCreatedBy(username);
			header.setCreatedDate(now);
			header.setModifiedBy(username);
			header.setModifiedDate(now);
			mobScheduleVisitToFarmerHeaderDao.save(header);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		
		try {
			// List<String> listCode = paddyFieldDao.listPaddyField(inputVisitAll.getFarmCode());
			List<Object[]> list = paddyFieldDao.listPaddyField(inputVisitAll.getFarmCode());
			List<MobScheduleVisitToFarmerDetail> details = new ArrayList<>();
			// MobScheduleVisitToFarmerDetail detail = new MobScheduleVisitToFarmerDetail();
			for (Object[] data : list) {
				MobScheduleVisitToFarmerDetail detail = new MobScheduleVisitToFarmerDetail();
				detail.setVisitNo(newVisitNo);
				detail.setFarmCode(inputVisitAll.getFarmCode());
				detail.setPaddyFieldCode((String) data[3]);
				detail.setVisitStartDate(now);
				detail.setVisitEndDate(now);
				detail.setLatitude(null);
				detail.setLongitude(null);
				detail.setEstimateHarvestTime(null);
				detail.setPlanText(null);
				detail.setEvaluationText(null);
				detail.setTaskActivity("00");
				detail.setIsActive(Byte.parseByte("1"));
				detail.setCreatedBy(username);
				detail.setCreatedDate(now);
				// mobScheduleVisitToFarmerDetailDao.save(detail);
				details.add(detail);
				// ganti ke banyak save
			}
			mobScheduleVisitToFarmerDetailDao.save(details);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		
		try {
			MobScheduleVisitToFarmerHistory history = new MobScheduleVisitToFarmerHistory();
			history.setVisitNo(newVisitNo);
			history.setFarmerId(farmer.getId());
			history.setAssignTo(user.getPersonId());
			history.setAssignDate(now);
			history.setTaskAction("00");
			history.setIsActive(Byte.parseByte("1"));
			history.setCreatedBy(username);
			history.setCreatedDate(now);
			mobScheduleVisitToFarmerHistoryDao.save(history);
			
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		
		if (otherActivity == 1 && stand == 0 && visit == 0) {
			try {
				NewVisitOtherActivity(inputVisitAll.getOtherActivityDto(), newVisitNo, username);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (stand == 1 && otherActivity == 0 && visit == 0) {
			try {
				NewInputStand(inputVisitAll.getStandDto(), newVisitNo, username);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (visit == 1 && stand == 0 && otherActivity == 0) {
			try {
				NewVisitAll(inputVisitAll.getVisitDto(), newVisitNo, username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (otherActivity == 1 && stand == 1 && visit == 0) {
			try {
				NewVisitOtherActivity(inputVisitAll.getOtherActivityDto(), newVisitNo, username);
				NewInputStand(inputVisitAll.getStandDto(), newVisitNo, username);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (otherActivity == 1 && visit == 1 && stand == 0) {
			try {
				NewVisitOtherActivity(inputVisitAll.getOtherActivityDto(), newVisitNo, username);
				NewVisitAll(inputVisitAll.getVisitDto(), newVisitNo, username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (stand == 1 && visit == 1 && otherActivity == 0) {
			try {
				NewInputStand(inputVisitAll.getStandDto(), newVisitNo, username);
				NewVisitAll(inputVisitAll.getVisitDto(), newVisitNo, username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		} else if (otherActivity == 1 && stand == 1 && visit == 1) {
			try {
				NewVisitOtherActivity(inputVisitAll.getOtherActivityDto(), newVisitNo, username);
				NewInputStand(inputVisitAll.getStandDto(), newVisitNo, username);
				NewVisitAll(inputVisitAll.getVisitDto(), newVisitNo, username, appraisement, scoring);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} catch (Exception e) {
				result.put("status", 0);
				result.put("message", e);
				result.put("Object", null);
			}
		}
		
		return result;
	}
	
	private String[] roman = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII" };
	
	public String generateVIsitNo(Integer lastVisit) {
		String[] numPart = new String[3];
		LocalDate now = LocalDate.now();
		numPart[0] = lastVisit.toString();
		if (numPart[0].length() < 4) {
			for (int i = numPart[0].length(); i < 4; i++) {
				numPart[0] = "0" + numPart[0];
			}
		}
		numPart[1] = roman[now.getMonthValue() - 1];
		numPart[2] = Integer.toString(now.getYear());
		return String.join("/", numPart);
	}
	
	public String generateVIsitNo() {
		String[] numPart = new String[3];
		LocalDate now = LocalDate.now();
		numPart[0] = "";
		numPart[1] = roman[now.getMonthValue() - 1];
		numPart[2] = Integer.toString(now.getYear());
		return String.join("/", numPart);
	}
	
	public HashMap<String, Object> NewInputStand(NewVisitStandDto dto, String visitNo, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobStand mobStand = new MobStand();
		Date now = new Date();
		try {
			mobStand.setVisitNo(visitNo);
			mobStand.setEntryDate(dto.getEntryDate());
			mobStand.setFarmArea(dto.getFarmArea());
			mobStand.setTonnageEstimation(dto.getTonnageEstimation());
			mobStand.setLastStandTonnage(dto.getLastStandTonnage());
			mobStand.setLastStandArea(dto.getLastStandArea());
			mobStand.setStandTonnage(dto.getStandTonnage());
			mobStand.setStandArea(dto.getStandArea());
			mobStand.setActivityText(null);
			mobStand.setIsActive((byte) 1);
			mobStand.setCreatedBy(username);
			mobStand.setCreatedDate(now);
			
			mobStandDao.save(mobStand);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	public HashMap<String, Object> NewVisitOtherActivity(NewVisitOtherActivityDto dto, String visitNo, String username) {
		HashMap<String, Object> result = new HashMap<>();
		MobScheduleVisitOtherActivity visitOtherActivity = new MobScheduleVisitOtherActivity();
		Date now = new Date();
		try {
			visitOtherActivity.setVisitNo(visitNo);
			visitOtherActivity.setActivityText(dto.getActivityText());
			visitOtherActivity.setEntryDate(dto.getEntryDate());
			visitOtherActivity.setCreatedBy(username);
			visitOtherActivity.setCreatedDate(now);
			visitOtherActivity.setIsActive((byte) 1);
			visitOtherActivityDao.save(visitOtherActivity);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		return result;
	}
	
	public HashMap<String, Object> NewVisitAll(NewVisitListDto dto, String visitNo, String username, int appraisement, int scoring) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		
		try {
			for (NewVisitDto data : dto.getNewVisitDtos()) {
				
				// set data new
				if ((data.getPeriodName()).equals(null) || (data.getPeriodName()).equals("") || data.getPeriodName().length() == 0) {
					appraisement = 0;
				} else {
					appraisement = 1;
				}
				
				if (data.getVisitScoring() == null || data.getVisitScoring().size() == 0) {
					scoring = 0;
				} else {
					scoring = 1;
				}
				// Taksasi
				MobScheduleVisitToFarmerDetail detail = mobScheduleVisitToFarmerDetailDao.getOneByVisitNoDetail(visitNo, data.getPaddyFieldCode());
				MobScheduleVisitAppraisement scheduleVisitAppraisement = new MobScheduleVisitAppraisement();
				if (appraisement == 1) {
					
					try {
						scheduleVisitAppraisement.setScheduleVisitDetailId(detail.getId());
						scheduleVisitAppraisement.setPeriodName(data.getPeriodName());
						scheduleVisitAppraisement.setCurrentSugarcaneHeight(data.getTinggiBatangSekarang());
						scheduleVisitAppraisement.setCutdownSugarcaneHeight(data.getTinggiBatangDitebang());
						scheduleVisitAppraisement.setSugarcaneStemWeight(data.getBeratBatangperMeter());
						scheduleVisitAppraisement.setWeightPerSugarcane(data.getBeratPerBatang());
						scheduleVisitAppraisement.setNumberOfStemPerLeng(data.getJumlahBatangTiapLeng());
						scheduleVisitAppraisement.setLeng_factor_per_HA(data.getJumlahLengperHA());
						scheduleVisitAppraisement.setNumber_of_sugarcane_per_HA(data.getJumlahBatangperHA());
						scheduleVisitAppraisement.setProdPerHectarKU(data.getProdPerHektarKu());
						scheduleVisitAppraisement.setAmountOfProduction(data.getJumlahProduksiKU());
						scheduleVisitAppraisement.setCuttingSchedule(data.getJadwalTebangPeriode());
						scheduleVisitAppraisement.setAvgBrixNumber(data.getRerataAngkaBrix());
						scheduleVisitAppraisement.setRendemenEstimation(data.getEstimasiRendemen());
						scheduleVisitAppraisement.setKuHablurPerHa(data.getKuHablurPerHa());
						scheduleVisitAppraisement.setAmountOfHablur(data.getJumlahHablur());
						scheduleVisitAppraisement.setIsActive((byte) 1);
						scheduleVisitAppraisement.setCreatedBy(username);
						scheduleVisitAppraisement.setCreatedDate(now);
					} catch (Exception e) {
						result.put("status", Integer.valueOf(0));
						result.put("message", e);
						result.put("Object", null);
					}
				}
				
				MobScheduleVisitToFarmerDetail mobScheduleVisitToFarmerDetail = mobScheduleVisitToFarmerDetailDao.findOne(detail.getId());
				try {
					mobScheduleVisitToFarmerDetail.setEstimateHarvestTime(data.getEstimateHarvestTime());
					mobScheduleVisitToFarmerDetail.setPlanText(data.getPlanText());
					mobScheduleVisitToFarmerDetail.setEvaluationText(data.getEvaluationText());
					mobScheduleVisitToFarmerDetail.setLatitude(data.getLatitude());
					mobScheduleVisitToFarmerDetail.setLongitude(data.getLongitude());
					mobScheduleVisitToFarmerDetail.setTaskActivity("00");
				} catch (Exception e) {
					result.put("status", Integer.valueOf(0));
					result.put("message", e);
					result.put("Object", null);
				}
				
				MobScheduleVisitScoring scheduleVisitScoring = new MobScheduleVisitScoring();
				List<NewVisitScoringDto> list = data.getVisitScoring();
				try {
					if (scoring == 1) {
						for (NewVisitScoringDto o : list) {
							scheduleVisitScoring.setScheduleVisitDetailId(detail.getId());
							scheduleVisitScoring.setParamCode(o.getParamCode());
							scheduleVisitScoring.setParamValue(o.getParamValue());
							scheduleVisitScoring.setCriteria(o.getCriteria());
							scheduleVisitScoring.setWeight(o.getWeight());
							scheduleVisitScoring.setScore(o.getScore());
							scheduleVisitScoring.setNotes(o.getNotes());
							scheduleVisitScoring.setIsActive((byte) 1);
							scheduleVisitScoring.setCreatedBy(username);
							scheduleVisitScoring.setCreatedDate(now);
							
							visitScoringDao.save(scheduleVisitScoring);
						}
					}
					
					if (appraisement == 1) {
						visitAppraisementDao.save(scheduleVisitAppraisement);
					}
					mobScheduleVisitToFarmerDetailDao.save(mobScheduleVisitToFarmerDetail);
					
					result.put("status", Integer.valueOf(1));
					result.put("message", "Sukses");
					result.put("Object", null);
				} catch (Exception e) {
					result.put("status", Integer.valueOf(0));
					result.put("message", e);
					result.put("Object", null);
				}
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", e);
			result.put("Object", null);
		}
		
		return result;
	}
	
	@Override
	public HashMap<String, Object> getListDelegasi(String username) {
		
		HashMap<String, Object> result = new HashMap<>();
		
		Integer id = visitDao.getListIdDelegasi(username);
		
		List<Integer> data = getIdMember(id);
		if (data != null && data.size() > 0) {
			List<Object[]> list = visitDao.getListDelegasi(data);
			List<DelegasiDto> dtos = new ArrayList<>();
			if (list.size() != 0) {
				for (Object[] o : list) {
					DelegasiDto dto = new DelegasiDto();
					dto.setId((Integer) o[0]);
					dto.setFullname((String) o[1]);
					dto.setNoRegister((String) o[2]);
					dto.setPhotoText((String) o[3] == null ? "" : (String) o[3]);
					dto.setPosition((String) o[4] == null ? "" : (String) o[4]);
					dtos.add(dto);
				}
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} else {
			result.put("status", 2);
			result.put("message", "Petugas tidak mempunyai bawahan.");
			result.put("Object", null);
		}
		
		return result;
	}
	
	public List<Integer> getIdMember(Integer id) {
		List<Integer> listParam = new ArrayList<>();
		listParam.add(id);
		List<Integer> employees = employeeDao.findOneByIdList2(listParam);
		List<Integer> listId = new ArrayList<>();
		for (Integer e : employees) {
			listId.add(e);
		}
		
		List<Integer> employees2 = new ArrayList<>();
		if (listId != null && listId.size() != 0) {
			for (int i = 0; i < 10; i++) {
				employees2 = employeeDao.findOneByIdList2(listId);
				if (employees2.size() > 0) {
					for (Integer data : employees2) {
						listId.add(data);
					}
				}
			}
		}
		return listId;
	}
	
	@Override
	public HashMap<String, Object> terimaTugas(String username, String visitNo) {
		HashMap<String, Object> result = new HashMap<>();
		try {
			MobScheduleVisitToFarmerHeader farmerHeader = mobScheduleVisitToFarmerHeaderDao.findOneBy(visitNo);
			farmerHeader.setTaskActivity("03");
			
			MobScheduleVisitToFarmerHistory farmerHeaderHystory = mobScheduleVisitToFarmerHistoryDao.findOneBy(visitNo, farmerHeader.getFarmerId(), farmerHeader.getAssignTo());
			if (farmerHeaderHystory != null) {
				farmerHeaderHystory.setTaskAction("03");
			}
			
			List<MobScheduleVisitToFarmerDetail> mobScheduleVisitToFarmerDetail = mobScheduleVisitToFarmerDetailDao.getByVisitNo(visitNo);
			for (MobScheduleVisitToFarmerDetail o : mobScheduleVisitToFarmerDetail) {
				o.setTaskActivity("03");
				mobScheduleVisitToFarmerDetailDao.save(o);
			}
			
			mobScheduleVisitToFarmerHeaderDao.save(farmerHeader);
			mobScheduleVisitToFarmerHistoryDao.save(farmerHeaderHystory);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		
		return result;
	}
	
	@Override
	public HashMap<String, Object> delegasikan(String username, String visitNo, Integer onFarmId) {
		// farmerId id petugas
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		try {
			MobScheduleVisitToFarmerHeader farmerHeader = mobScheduleVisitToFarmerHeaderDao.findOneBy(visitNo);
			MobScheduleVisitToFarmerHistory farmerHeaderHystory = mobScheduleVisitToFarmerHistoryDao.findOneBy(visitNo, farmerHeader.getFarmerId(), farmerHeader.getAssignTo());
			
			farmerHeader.setAssignTo(onFarmId);
			farmerHeader.setTaskActivity("02");
			
			farmerHeaderHystory.setTaskAction("04");
			
			MobScheduleVisitToFarmerHistory history = new MobScheduleVisitToFarmerHistory();
			history.setVisitNo(visitNo);
			history.setFarmerId(farmerHeaderHystory.getFarmerId());
			history.setAssignTo(onFarmId);
			history.setAssignDate(now);
			history.setTaskAction("02");
			history.setIsActive(Byte.parseByte("1"));
			history.setCreatedBy(username);
			history.setCreatedDate(now);
			
			mobScheduleVisitToFarmerHeaderDao.save(farmerHeader);
			mobScheduleVisitToFarmerHistoryDao.save(farmerHeaderHystory);
			mobScheduleVisitToFarmerHistoryDao.save(history);
			
			String token = employeeDao.findTokenPetugas(farmerHeader.getAssignTo());
			notification.notifDelegasi(token, visitNo);
			
			result.put("status", 1);
			result.put("message", "Sukses");
			result.put("Object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e);
			result.put("Object", null);
		}
		
		return result;
	}
}
