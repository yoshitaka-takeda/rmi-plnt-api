package id.co.indocyber.rmiApi.service;

import id.co.indocyber.rmiApi.dto.TrackingDto;

import java.util.HashMap;

public interface DriverSvc {
	public HashMap<String, Object> loading(TrackingDto dto);
	public HashMap<String, Object> destination (TrackingDto dto); 
	public HashMap<String, Object> finish(TrackingDto dto);
}
