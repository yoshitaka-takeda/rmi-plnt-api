package id.co.indocyber.rmiApi.service.impl;

import java.io.Console;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.rmiApi.config.ConstantVariabel;
import id.co.indocyber.rmiApi.dao.MUserDao;
import id.co.indocyber.rmiApi.dao.MobDriverDao;
import id.co.indocyber.rmiApi.dao.MobEmployeeDao;
import id.co.indocyber.rmiApi.dao.MobFarmerDao;
import id.co.indocyber.rmiApi.dto.ProfileDto;
import id.co.indocyber.rmiApi.entity.MUser;
import id.co.indocyber.rmiApi.entity.MobDriver;
import id.co.indocyber.rmiApi.entity.MobEmployee;
import id.co.indocyber.rmiApi.entity.MobFarmer;
import id.co.indocyber.rmiApi.service.MUserSvc;

@Service("mUserSvc")
@Transactional
public class MUserSvcImpl implements MUserSvc {
	
	@Autowired
	MUserDao mUserDao;
	
	@Autowired
	MobDriverDao driverDao;
	
	@Autowired
	MobFarmerDao farmerDao;
	
	@Autowired
	MobEmployeeDao employeeDao;
	
	@Override
	public Map<String, Object> getProfile(String phoneNum) {
		ProfileDto profileDto = new ProfileDto();
		HashMap<String, Object> result = new HashMap<>();
		int role = mUserDao.cekRole(phoneNum);
		
		if (checkDuplicateUser(phoneNum)) {
			if (role == 1 || role == 5 || role == 6 || role == 7 || role == 8 || role == 11) {
				// code block employee selalu update ke case
				List<Object[]> dataEmployee = mUserDao.getEmployee(phoneNum);
				for (Object[] o : dataEmployee) {
					
					profileDto.setFullName((String) o[0]);
					profileDto.setAddress((String) o[1] == null ? "" : (String) o[1]);
					profileDto.setRtrw((String) o[2] == null ? "" : (String) o[2]);
					profileDto.setVillage((String) o[3] == null ? "" : (String) o[3]);
					profileDto.setSub_district((String) o[4] == null ? "" : (String) o[4]);
					profileDto.setDistrict((String) o[5] == null ? "" : (String) o[5]);
					profileDto.setCity((String) o[6] == null ? "" : (String) o[6]);
					profileDto.setProvince((String) o[7] == null ? "" : (String) o[7]);
					profileDto.setZipcode((String) o[8] == null ? "" : (String) o[8]);
					profileDto.setGender(String.valueOf(o[9]));
					profileDto.setNo_tlp((String) o[10] == null ? "" : (String) o[10]);
					profileDto.setNo_ktp((String) o[17] == null ? "" : (String) o[17]);
					
					Integer referenCode = Integer.parseInt((String) o[20]);
					Integer id = (Integer) o[11];
					if (referenCode == 0) {
						profileDto.setNo_register(id);
					} else {
						profileDto.setNo_register(referenCode);
					}
					//
					profileDto.setPhotoText((String) o[18] == null ? "" : (String) o[18]);
					profileDto.setUsername((String) o[19] == null ? "" : (String) o[19]);
					// profileDto.setUserId((int) o[20]);
					// profileDto.setAddress_type(String.valueOf(o[12]));
					// profileDto.setMarital_status(String.valueOf(o[13]));
					// profileDto.setReligion(String.valueOf(o[14]));
					// profileDto.setLast_education(String.valueOf((char)o[15]));
					// profileDto.setBirthday((Date) o[16]);
				}
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", profileDto);
			} else if (role == 2) {
				// code block farmer
				List<Object[]> dataFarmer = mUserDao.getFarmer(phoneNum);
				for (Object[] o : dataFarmer) {
					profileDto.setFullName((String) o[0]);
					profileDto.setAddress((String) o[1] == null ? "" : (String) o[1]);
					profileDto.setRtrw((String) o[2] == null ? "" : (String) o[2]);
					profileDto.setVillage((String) o[3] == null ? "" : (String) o[3]);
					profileDto.setSub_district((String) o[4] == null ? "" : (String) o[4]);
					profileDto.setDistrict((String) o[5] == null ? "" : (String) o[5]);
					profileDto.setCity((String) o[6] == null ? "" : (String) o[6]);
					profileDto.setProvince((String) o[7] == null ? "" : (String) o[7]);
					profileDto.setZipcode((String) o[8] == null ? "" : (String) o[8]);
					profileDto.setGender(String.valueOf(o[9]));
					profileDto.setNo_tlp((String) o[10] == null ? "" : (String) o[10]);
					profileDto.setNo_ktp((String) o[17] == null ? "" : (String) o[17]);
					
					Integer referenCode = Integer.parseInt((String) o[20]);
					Integer id = (Integer) o[11];
					if (referenCode == 0) {
						profileDto.setNo_register(id);
					} else {
						profileDto.setNo_register(referenCode);
					}
					
					profileDto.setPhotoText((String) o[18] == null ? "" : (String) o[18]);
					profileDto.setUsername((String) o[19] == null ? "" : (String) o[19]);
					// profileDto.setUserId((int) o[20]);
					// profileDto.setAddress_type(String.valueOf(o[12]));
					// profileDto.setMarital_status(String.valueOf(o[13]));
					// profileDto.setReligion(String.valueOf(o[14]));
					// profileDto.setLast_education(String.valueOf(o[15]));
					// profileDto.setBirthday((Date) o[16]);
				}
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", profileDto);
			} else if (role == 3) {
				// code block driver
				List<Object[]> dataDriver = mUserDao.getDriver(phoneNum);
				for (Object[] o : dataDriver) {
					profileDto.setFullName((String) o[0]);
					profileDto.setAddress((String) o[1] == null ? "" : (String) o[1]);
					profileDto.setRtrw((String) o[2] == null ? "" : (String) o[2]);
					profileDto.setVillage((String) o[3] == null ? "" : (String) o[3]);
					profileDto.setSub_district((String) o[4] == null ? "" : (String) o[4]);
					profileDto.setDistrict((String) o[5] == null ? "" : (String) o[5]);
					profileDto.setCity((String) o[6] == null ? "" : (String) o[6]);
					profileDto.setProvince((String) o[7] == null ? "" : (String) o[7]);
					profileDto.setZipcode((String) o[8] == null ? "" : (String) o[8]);
					profileDto.setGender(String.valueOf(o[9]));
					profileDto.setNo_tlp((String) o[10] == null ? "" : (String) o[10]);
					profileDto.setNo_ktp((String) o[17] == null ? "" : (String) o[17]);
					
					Integer referenCode = Integer.parseInt((String) o[20]);
					Integer id = (Integer) o[11];
					if (referenCode == 0) {
						profileDto.setNo_register(id);
					} else {
						profileDto.setNo_register(referenCode);
					}
					
					profileDto.setPhotoText((String) o[18] == null ? "" : (String) o[18]);
					profileDto.setUsername((String) o[19] == null ? "" : (String) o[19]);
					// profileDto.setUserId((int) o[20]);
					// profileDto.setAddress_type(String.valueOf(o[12]));
					// profileDto.setMarital_status(String.valueOf(o[13]));
					// profileDto.setReligion(String.valueOf(o[14]));
					// profileDto.setLast_education(String.valueOf(o[15]));
					// profileDto.setBirthday((Date) o[16]);
				}
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", profileDto);
			} else {
				result.put("status", 99);
				result.put("message", "Username tidak ditemukan");
				result.put("Object", null);
			}
		} else {
			result.put("status", 99);
			result.put("message", "Username duplikat, atau no hp sudah ada yang menggunakan");
			result.put("Object", null);
		}
		
		return result;
	}
	
	@Override
	public Map<String, Object> changePassword(String phoneNum, String pass, String newPass) {
		
		String sha_pwd = hash(phoneNum, pass);
		String sha_newpwd = hash(phoneNum, newPass);
		Map<String, Object> result = new HashMap<>();
		if (checkDuplicateUser(phoneNum)) {
			if (checkPassword(phoneNum, sha_pwd)) {
				MUser mUser = mUserDao.checkUser(phoneNum);
				mUser.setPassword(sha_newpwd);
				mUser.setModifiedBy(mUser.getUsername());
				mUserDao.save(mUser);
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} else {
				result.put("status", 99);
				result.put("message", "Username atau password salah");
				result.put("Object", null);
			}
		} else {
			result.put("status", 99);
			result.put("message", "Username duplikat, atau no hp sudah ada yang menggunakan");
			result.put("Object", null);
		}
		return result;
	}
	
	public boolean checkDuplicateUser(String phoneNum) {
		boolean msg;
		Long mUser = mUserDao.checkDuplicateUser(phoneNum);
		if (mUser == 1) {
			msg = true;
		} else {
			msg = false;
		}
		return msg;
	}
	
	public boolean checkPassword(String email, String pass) {
		
		boolean valPass = false;
		MUser mUser = mUserDao.checkPassword(email, pass);
		if (mUser != null) {
			valPass = true;
		} else {
			valPass = false;
		}
		return valPass;
	}
	
	@Override
	public Map<String, Object> updateProfile(ProfileDto profileDto, String phoneNum) {
		
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date();
		
		if (checkDuplicateUser(phoneNum)) {
			int role = mUserDao.cekRole(phoneNum);
			MUser mUser = mUserDao.checkUser(phoneNum);
			int id = mUser.getPersonId();
			String Pic = "";
			
			if (role == 1 || role == 5 || role == 6 || role == 7 || role == 8 || role == 11) {
				// code block employee
				
				MobEmployee employee = employeeDao.findOneBy(id);
				if (employee != null) {
					if (profileDto.getAddress().equals(null) || profileDto.getAddress().equals("")) {
						
					} else {
						employee.setAddress(profileDto.getAddress());
					}
					// employee.setAddressType(profileDto.getAddress_type());
					// employee.setBirthday(profileDto.getBirthday());
					if (profileDto.getCity().equals(null) || profileDto.getCity().equals("")) {
						
					} else {
						employee.setCity(profileDto.getCity());
					}
					if (profileDto.getDistrict().equals(null) || profileDto.getDistrict().equals("")) {
						
					} else {
						employee.setDistrict(profileDto.getDistrict());
					}
					if (profileDto.getFullName().equals(null) || profileDto.getFullName().equals("")) {
						
					} else {
						employee.setFullname(profileDto.getFullName());
					}
					if (profileDto.getGender().equals(null) || profileDto.getGender().equals("")) {
						
					} else {
						employee.setGender(profileDto.getGender());
					}
					// employee.setLastEducation(profileDto.getLast_education());
					// employee.setMartialStatus(profileDto.getMarital_status());
					if (profileDto.getNo_tlp().equals(null) || profileDto.getNo_tlp().equals("")) {
						
					} else {
						employee.setPhoneNum(profileDto.getNo_tlp());
					}
					if (profileDto.getProvince().equals(null) || profileDto.getProvince().equals("")) {
						
					} else {
						employee.setProvince(profileDto.getProvince());
					}
					// employee.setReligion(profileDto.getReligion());
					if (profileDto.getRtrw().equals(null) || profileDto.getRtrw().equals("")) {
						
					} else {
						employee.setRtrw(profileDto.getRtrw());
					}
					if (profileDto.getSub_district().equals(null) || profileDto.getSub_district().equals("")) {
						
					} else {
						employee.setSubDistrict(profileDto.getSub_district());
					}
					if (profileDto.getVillage().equals(null) || profileDto.getVillage().equals("")) {
						
					} else {
						employee.setVillage(profileDto.getVillage());
					}
					if (profileDto.getZipcode().equals(null) || profileDto.getZipcode().equals("")) {
						
					} else {
						employee.setZipcode(profileDto.getZipcode());
					}
					if (profileDto.getNo_ktp().equals(null) || profileDto.getNo_ktp().equals("")) {
						
					} else {
						employee.setNoKtp(profileDto.getNo_ktp());
					}
					employee.setModifiedBy(mUser.getUsername());
					employee.setModifiedDate(now);
					
					if (profileDto.getPhotoText().equals("") || profileDto.getPhotoText().equals(null)) {
						
					} else {
						String a = Base64ToImage(profileDto.getPhotoText(), mUser.getUsername());
						Pic = ConstantVariabel._ipServer + a.substring(ConstantVariabel._subStr).replace("\\", "/");
						
					}
					
					MUser mUser1 = mUserDao.checkUser(phoneNum);
					if (profileDto.getFullName().equals(null) || profileDto.getFullName().equals("")) {
						
					} else {
						mUser1.setFullname(profileDto.getFullName());
					}
					mUser1.setModifiedBy(mUser.getUsername());
					if (profileDto.getPhotoText().equals(null) || profileDto.getPhotoText().equals("")) {
						
					} else {
						mUser1.setPhotoText(Pic);
					}
					mUser1.setModifiedDate(now);
					
					mUserDao.save(mUser1);
					employeeDao.save(employee);
					
					result.put("status", 1);
					result.put("message", "Sukses");
					result.put("Object", null);
				}
			} else if (role == 2) {
				// code block farmer
				MobFarmer farmer = farmerDao.findOneBy(id);
				if (farmer != null) {
					if (profileDto.getAddress().equals(null) || profileDto.getAddress().equals("")) {
						
					} else {
						farmer.setAddress(profileDto.getAddress());
					}
					// farmer.setAddressType(profileDto.getAddress_type());
					// farmer.setBirthday(profileDto.getBirthday());
					if (profileDto.getCity().equals(null) || profileDto.getCity().equals("")) {
						
					} else {
						farmer.setCity(profileDto.getCity());
					}
					if (profileDto.getDistrict().equals(null) || profileDto.getDistrict().equals("")) {
						
					} else {
						farmer.setDistrict(profileDto.getDistrict());
					}
					if (profileDto.getFullName().equals(null) || profileDto.getFullName().equals("")) {
						
					} else {
						farmer.setFullname(profileDto.getFullName());
					}
					if (profileDto.getGender().equals(null) || profileDto.getGender().equals("")) {
						
					} else {
						farmer.setGender(profileDto.getGender());
					}
					// farmer.setLastEducation(profileDto.getLast_education());
					// farmer.setMartialStatus(profileDto.getMarital_status());
					if (profileDto.getNo_tlp().equals(null) || profileDto.getNo_tlp().equals("")) {
						
					} else {
						farmer.setPhoneNum(profileDto.getNo_tlp());
					}
					if (profileDto.getProvince().equals(null) || profileDto.getProvince().equals("")) {
						
					} else {
						farmer.setProvince(profileDto.getProvince());
					}
					// farmer.setReligion(profileDto.getReligion());
					if (profileDto.getRtrw().equals(null) || profileDto.getRtrw().equals("")) {
						
					} else {
						farmer.setRtrw(profileDto.getRtrw());
					}
					if (profileDto.getSub_district().equals(null) || profileDto.getSub_district().equals("")) {
						
					} else {
						farmer.setSubDistrict(profileDto.getSub_district());
					}
					if (profileDto.getVillage().equals(null) || profileDto.getVillage().equals("")) {
						
					} else {
						farmer.setVillage(profileDto.getVillage());
					}
					if (profileDto.getZipcode().equals(null) || profileDto.getZipcode().equals("")) {
						
					} else {
						farmer.setZipcode(profileDto.getZipcode());
					}
					if (profileDto.getNo_ktp().equals(null) || profileDto.getNo_ktp().equals("")) {
						
					} else {
						farmer.setNoKtp(profileDto.getNo_ktp());
					}
					farmer.setModifiedBy(mUser.getUsername());
					farmer.setModifiedDate(now);
					
					if (profileDto.getPhotoText().equals(null) || profileDto.getPhotoText().equals("")) {
						
					} else {
						String a = Base64ToImage(profileDto.getPhotoText(), mUser.getUsername());
						Pic = ConstantVariabel._ipServer + a.substring(ConstantVariabel._subStr).replace("\\", "/");
					}
					
					MUser mUser2 = mUserDao.checkUser(phoneNum);
					if (profileDto.getFullName().equals(null) || profileDto.getFullName().equals("")) {
						
					} else {
						mUser2.setFullname(profileDto.getFullName());
					}
					mUser2.setModifiedBy(mUser.getUsername());
					if (profileDto.getPhotoText().equals(null) || profileDto.getPhotoText().equals("")) {
						
					} else {
						mUser2.setPhotoText(Pic);
					}
					mUser2.setModifiedDate(now);
					
					mUserDao.save(mUser2);
					farmerDao.save(farmer);
					
					result.put("status", 1);
					result.put("message", "Sukses");
					result.put("Object", null);
					
				}
			} else if (role == 3) {
				// code block driver
				MobDriver driver = driverDao.findOneBy(id);
				if (driver != null) {
					if (profileDto.getAddress().equals(null) || profileDto.getAddress().equals("")) {
						
					} else {
						driver.setAddress(profileDto.getAddress());
					}
					// driver.setAddressType(profileDto.getAddress_type());
					// driver.setBirthday(profileDto.getBirthday());
					if (profileDto.getCity().equals(null) || profileDto.getCity().equals("")) {
						
					} else {
						driver.setCity(profileDto.getCity());
					}
					if (profileDto.getDistrict().equals(null) || profileDto.getDistrict().equals("")) {
						
					} else {
						driver.setDistrict(profileDto.getDistrict());
					}
					if (profileDto.getFullName().equals(null) || profileDto.getFullName().equals("")) {
						
					} else {
						driver.setFullname(profileDto.getFullName());
					}
					if (profileDto.getGender().equals(null) || profileDto.getGender().equals("")) {
						
					} else {
						driver.setGender(profileDto.getGender());
					}
					// driver.setLastEducation(profileDto.getLast_education());
					// driver.setMartialStatus(profileDto.getMarital_status());
					if (profileDto.getNo_tlp().equals(null) || profileDto.getNo_tlp().equals("")) {
						
					} else {
						driver.setPhoneNum(profileDto.getNo_tlp());
					}
					if (profileDto.getProvince().equals(null) || profileDto.getProvince().equals("")) {
						
					} else {
						driver.setProvince(profileDto.getProvince());
					}
					// farmer.setReligion(profileDto.getReligion());
					if (profileDto.getRtrw().equals(null) || profileDto.getRtrw().equals("")) {
						
					} else {
						driver.setRtrw(profileDto.getRtrw());
					}
					if (profileDto.getSub_district().equals(null) || profileDto.getSub_district().equals("")) {
						
					} else {
						driver.setSubDistrict(profileDto.getSub_district());
					}
					if (profileDto.getVillage().equals(null) || profileDto.getVillage().equals("")) {
						
					} else {
						driver.setVillage(profileDto.getVillage());
					}
					if (profileDto.getZipcode().equals(null) || profileDto.getZipcode().equals("")) {
						
					} else {
						driver.setZipcode(profileDto.getZipcode());
					}
					if (profileDto.getNo_ktp().equals(null) || profileDto.getNo_ktp().equals("")) {
						
					} else {
						driver.setNoKtp(profileDto.getNo_ktp());
					}
					driver.setModifiedBy(mUser.getUsername());
					driver.setModifiedDate(now);
					
					if (profileDto.getPhotoText().equals(null) || profileDto.getPhotoText().equals("")) {
						
					} else {
						String a = Base64ToImage(profileDto.getPhotoText(), mUser.getUsername());
						Pic = ConstantVariabel._ipServer + a.substring(ConstantVariabel._subStr).replace("\\", "/");
					}
					
					MUser mUser3 = mUserDao.checkUser(phoneNum);
					if (profileDto.getFullName().equals(null) || profileDto.getFullName().equals("")) {
						
					} else {
						mUser3.setFullname(profileDto.getFullName());
					}
					mUser3.setModifiedBy(mUser.getUsername());
					if (profileDto.getPhotoText().equals(null) || profileDto.getPhotoText().equals("")) {
						
					} else {
						mUser3.setPhotoText(Pic);
					}
					mUser3.setModifiedDate(now);
					
					mUserDao.save(mUser3);
					driverDao.save(driver);
					
					result.put("status", 1);
					result.put("message", "Sukses");
					result.put("Object", null);
					
				}
			} else {
				// code block
				result.put("status", 99);
				result.put("message", "Username tidak ditemukan");
				result.put("Object", null);
			}
		} else {
			result.put("status", 99);
			result.put("message", "Username duplikat, atau no hp sudah ada yang menggunakan");
			result.put("Object", null);
		}
		return result;
	}
	
	private String Base64ToImage(String base64, String username) {
		
		String extension = "jpg";
		String pathFile = "";
		try {
			String[] strings = base64.split(",");
			byte[] decodedBytes = Base64.getDecoder().decode(strings[1]);
			pathFile = ConstantVariabel._pathImg + username.replace(".", "-") + "." + extension;
			FileOutputStream imageOutFile = null;
			imageOutFile = new FileOutputStream(pathFile);
			imageOutFile.write(decodedBytes);
			imageOutFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return pathFile;
	}
	
	private String hash(String email, String password) {
		MUser mUser = mUserDao.checkUser(email);
		String username = mUser.getUsername();
		String toHash = username + "rejoso" + password + "manisindo";
		return DigestUtils.sha1Hex(toHash);
	}
}
