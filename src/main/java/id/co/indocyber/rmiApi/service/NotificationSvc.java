package id.co.indocyber.rmiApi.service;

import java.util.Map;

import id.co.indocyber.rmiApi.dto.TokenDto;

public interface NotificationSvc {
	
	public void notifSpta(String token, String sptaNum);
	
	public void notifWeighBridge(TokenDto token, String sptaNum);
	
	public void notifVisit(String token, String visitNo);
	
	public void notifDelegasi(String token, String visitNo);
	
	public Map<String, Object> notifSptaFcm(String token, String sptaNum);
	
	public Map<String, Object> notifWeighBridgeFcm(TokenDto token, String sptaNum);
	
	public Map<String, Object> notifVisitFcm(String token, String visitNo);
	
	public Map<String, Object> notifDelegasiFcm(String token, String visitNo);
}
