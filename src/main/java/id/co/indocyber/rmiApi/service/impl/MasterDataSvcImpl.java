package id.co.indocyber.rmiApi.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.rmiApi.dao.MAddressTypeDao;
import id.co.indocyber.rmiApi.dao.MEducationDao;
import id.co.indocyber.rmiApi.dao.MFarmTypeDao;
import id.co.indocyber.rmiApi.dao.MFarmerTypeDao;
import id.co.indocyber.rmiApi.dao.MGenderDao;
import id.co.indocyber.rmiApi.dao.MMartialStatusDao;
import id.co.indocyber.rmiApi.dao.MPlantingPeriodDao;
import id.co.indocyber.rmiApi.dao.MReasonDao;
import id.co.indocyber.rmiApi.dao.MReligionDao;
import id.co.indocyber.rmiApi.dao.MSugarCaneFarmTypeDao;
import id.co.indocyber.rmiApi.dao.MSugarCaneParamScoringDao;
import id.co.indocyber.rmiApi.dao.MSugarCaneTypeDao;
import id.co.indocyber.rmiApi.dao.MUserDao;
import id.co.indocyber.rmiApi.dao.MVehicleDao;
import id.co.indocyber.rmiApi.dao.MVehicleTypeDao;
import id.co.indocyber.rmiApi.dao.MobDriverDao;
import id.co.indocyber.rmiApi.dao.MobEmployeeDao;
import id.co.indocyber.rmiApi.dao.MobFarmDao;
import id.co.indocyber.rmiApi.dao.MobFarmerDao;
import id.co.indocyber.rmiApi.dto.DriverDto;
import id.co.indocyber.rmiApi.dto.FarmOfflineDto;
import id.co.indocyber.rmiApi.dto.FarmerOfflineDto;
import id.co.indocyber.rmiApi.dto.MAddressTypeDto;
import id.co.indocyber.rmiApi.dto.MFarmTypeDto;
import id.co.indocyber.rmiApi.dto.MMartialStatusDto;
import id.co.indocyber.rmiApi.dto.MPlantingPeriodDto;
import id.co.indocyber.rmiApi.dto.MReasonDto;
import id.co.indocyber.rmiApi.dto.MReligionDto;
import id.co.indocyber.rmiApi.dto.MEducationTypeDto;
import id.co.indocyber.rmiApi.dto.MFarmerTypeDto;
import id.co.indocyber.rmiApi.dto.MGenderDto;
import id.co.indocyber.rmiApi.dto.MSugarcaneParamScoringDto;
import id.co.indocyber.rmiApi.dto.MVehilceDto;
import id.co.indocyber.rmiApi.dto.MVehicleTypeDto;
import id.co.indocyber.rmiApi.dto.MobDriverDto;
import id.co.indocyber.rmiApi.dto.MobFarmerDto;
import id.co.indocyber.rmiApi.dto.PaddyFieldAreaOfflineDto;
import id.co.indocyber.rmiApi.entity.MAddressType;
import id.co.indocyber.rmiApi.entity.MFarmFieldsType;
import id.co.indocyber.rmiApi.entity.MMaritalStatus;
import id.co.indocyber.rmiApi.entity.MPlantingPeriod;
import id.co.indocyber.rmiApi.entity.MReason;
import id.co.indocyber.rmiApi.entity.MReligion;
import id.co.indocyber.rmiApi.entity.MEducationType;
import id.co.indocyber.rmiApi.entity.MFarmerType;
import id.co.indocyber.rmiApi.entity.MGender;
import id.co.indocyber.rmiApi.entity.MSugarcaneFarmType;
import id.co.indocyber.rmiApi.entity.MSugarcaneParamScoring;
import id.co.indocyber.rmiApi.entity.MSugarcaneType;
import id.co.indocyber.rmiApi.entity.MVehicleType;
import id.co.indocyber.rmiApi.entity.MobDriver;
import id.co.indocyber.rmiApi.entity.MobEmployee;
import id.co.indocyber.rmiApi.entity.MobVehicle;
import id.co.indocyber.rmiApi.service.MasterDataSvc;

@Service("masterDataSvc")
@Transactional
public class MasterDataSvcImpl implements MasterDataSvc {
	
	@Autowired
	MAddressTypeDao addressTypeDao;
	@Autowired
	MEducationDao educationDao;
	@Autowired
	MFarmerTypeDao farmerTypeDao;
	@Autowired
	MGenderDao genderDao;
	@Autowired
	MMartialStatusDao martialStatusDao;
	@Autowired
	MReligionDao religionDao;
	@Autowired
	MFarmTypeDao farmTypeDao;
	@Autowired
	MSugarCaneFarmTypeDao sugarCaneFarmTypeDao;
	@Autowired
	MSugarCaneParamScoringDao sugarCaneParamScoringDao;
	@Autowired
	MSugarCaneTypeDao sugarCaneTypeDao;
	@Autowired
	MVehicleDao vehicleDao;
	@Autowired
	MVehicleTypeDao vehicleTypeDao;
	@Autowired
	MobDriverDao mobDriverDao;
	@Autowired
	MPlantingPeriodDao mPlantingPeriodDao;
	@Autowired
	MReasonDao reasonDao;
	@Autowired
	MobFarmerDao farmerDao;
	@Autowired
	MUserDao mUserDao;
	@Autowired
	MobEmployeeDao employeeDao;
	@Autowired
	MobFarmDao farmDao;
	
	@Override
	public HashMap<String, Object> addressType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MAddressType> list = addressTypeDao.findAll();
			List<MAddressTypeDto> dtos = new ArrayList<>();
			for (MAddressType data : list) {
				MAddressTypeDto dto = new MAddressTypeDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> educationType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MEducationType> list = educationDao.findAll();
			List<MEducationTypeDto> dtos = new ArrayList<>();
			for (MEducationType data : list) {
				MEducationTypeDto dto = new MEducationTypeDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> farmerType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MFarmerType> list = farmerTypeDao.findAll();
			List<MFarmerTypeDto> dtos = new ArrayList<>();
			for (MFarmerType data : list) {
				MFarmerTypeDto dto = new MFarmerTypeDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> foundType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public HashMap<String, Object> gender() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MGender> list = genderDao.findAll();
			List<MGenderDto> dtos = new ArrayList<>();
			for (MGender data : list) {
				MGenderDto dto = new MGenderDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> martialStatus() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MMaritalStatus> list = martialStatusDao.findAll();
			List<MMartialStatusDto> dtos = new ArrayList<>();
			for (MMaritalStatus data : list) {
				MMartialStatusDto dto = new MMartialStatusDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> religion() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MReligion> list = religionDao.findAll();
			List<MReligionDto> dtos = new ArrayList<>();
			for (MReligion data : list) {
				MReligionDto dto = new MReligionDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> farmType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MFarmFieldsType> list = farmTypeDao.findAll();
			List<MFarmTypeDto> dtos = new ArrayList<>();
			for (MFarmFieldsType data : list) {
				MFarmTypeDto dto = new MFarmTypeDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	// mls bwt dto
	@Override
	public HashMap<String, Object> sugarCaneFarmType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MSugarcaneFarmType> list = sugarCaneFarmTypeDao.findAll();
			List<MFarmTypeDto> dtos = new ArrayList<>();
			for (MSugarcaneFarmType data : list) {
				MFarmTypeDto dto = new MFarmTypeDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> sugarCaneParamScoring() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MSugarcaneParamScoring> list = sugarCaneParamScoringDao.findAll();
			List<MSugarcaneParamScoringDto> dtos = new ArrayList<>();
			for (MSugarcaneParamScoring data : list) {
				MSugarcaneParamScoringDto dto = new MSugarcaneParamScoringDto();
				dto.setParam_code(data.getParamCode());
				dto.setParam_value(data.getParamValue());
				dto.setCriteria(data.getCriteria());
				dto.setWeight(data.getWeight());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> sugarCaneType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MSugarcaneType> list = sugarCaneTypeDao.findAll();
			List<MFarmTypeDto> dtos = new ArrayList<>();
			for (MSugarcaneType data : list) {
				MFarmTypeDto dto = new MFarmTypeDto();
				dto.setCode(data.getCode());
				dto.setShortText(data.getShortText());
				dto.setLongText(data.getLongText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> vehicle() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MobVehicle> list = vehicleDao.findAll();
			List<MVehilceDto> dtos = new ArrayList<>();
			for (MobVehicle data : list) {
				MVehilceDto dto = new MVehilceDto();
				dto.setCapacity(data.getCapacity());
				dto.setProductionYear(data.getProductionYear());
				dto.setVehicleNo(data.getVehicleNo());
				dto.setVehicleType(data.getVehicleType());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> vehicleType() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MVehicleType> list = vehicleTypeDao.findAll();
			List<MVehicleTypeDto> dtos = new ArrayList<>();
			for (MVehicleType data : list) {
				MVehicleTypeDto dto = new MVehicleTypeDto();
				dto.setId(data.getId());
				dto.setReference_code(data.getReferenceCode());
				dto.setVehicle_type_name(data.getVehicleTypeName());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> driver() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<Object[]> list = mobDriverDao.findAllMasterIsFreeNew();
			List<DriverDto> dtos = new ArrayList<>();
			if (list.size() == 0 || list == null) {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			} else {
				for (Object[] data : list) {
					DriverDto dto = new DriverDto();
					dto.setId((Integer) data[0]);
					dto.setFullname((String) data[1]);
					dto.setDriverType((String) data[2]);
					dto.setPhoneNum((String) data[3]);
					
					Integer count = mobDriverDao.countCekDriverInUser(dto.getId());
					if (count == 0) {
						dto.setStatus(0);
					}
					if (count == 1) {
						dto.setStatus(1);
					}
					
					dtos.add(dto);
				}
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
				
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> plantingPeriod() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			List<MPlantingPeriod> list = mPlantingPeriodDao.findAllMaster();
			List<MPlantingPeriodDto> dtos = new ArrayList<MPlantingPeriodDto>();
			for (MPlantingPeriod data : list) {
				MPlantingPeriodDto dto = new MPlantingPeriodDto();
				dto.setCode(data.getCode());
				dto.setLongText(data.getLongText());
				dto.setShortText(data.getShortText());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> reason() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			List<MReason> list = reasonDao.findAllMaster();
			List<MReasonDto> dtos = new ArrayList<MReasonDto>();
			for (MReason data : list) {
				MReasonDto dto = new MReasonDto();
				dto.setId(data.getId());
				dto.setReason(data.getReason());
				dtos.add(dto);
			}
			if (dtos != null) {
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
			} else {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", Integer.valueOf(0));
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> driverRiwayat() {
		HashMap<String, Object> result = new HashMap<>();
		try {
			List<MobDriver> list = mobDriverDao.findAllMaster();
			List<DriverDto> dtos = new ArrayList<>();
			if (list.size() == 0 || list == null) {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			} else {
				for (MobDriver data : list) {
					DriverDto dto = new DriverDto();
					dto.setId(data.getId());
					dto.setFullname(data.getFullname());
					dto.setDriverType(data.getDriverType());
					dto.setPhoneNum(data.getPhoneNum());
					
					Integer count = mobDriverDao.countCekDriverInUser(dto.getId());
					if (count == 0) {
						dto.setStatus(0);
					}
					if (count == 1) {
						dto.setStatus(1);
					}
					
					dtos.add(dto);
				}
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", dtos);
				
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "Gagal");
			result.put("Object", null);
		}
		return result;
	}
	
	@Override
	public HashMap<String, Object> farmerAndFarm(String username) {
		HashMap<String, Object> result = new HashMap<>();
		
		try {
			Integer personId = mUserDao.getIdUser(username);
			List<Integer> id = getAllMyTeam(personId);
			List<Integer> farmerId = employeeDao.findFarmer(id);
			List<Object[]> list = farmerDao.getFarmerByEmployeeByRiki(farmerId);
			List<FarmerOfflineDto> dtos = new ArrayList<>();
			List<FarmOfflineDto> dtos2 = new ArrayList<>();
			List<PaddyFieldAreaOfflineDto> dtos3 = new ArrayList<>();
			if (list.size() == 0) {
				result.put("status", 99);
				result.put("message", "Data tidak tersedia");
				result.put("Object", null);
			} else {
				for (Object[] data : list) {
					FarmerOfflineDto dto = new FarmerOfflineDto();
					dto.setId((Integer) data[0] == null ? 0 : (Integer) data[0]);
					dto.setFullname((String) data[1] == null ? "" : (String) data[1]);
					dto.setVillage((String) data[2] == null ? "" : (String) data[2]);
					dto.setFarmerCode((String) data[3] == null ? "" : (String) data[3]);
					dto.setPhoto_text((String) data[4] == null ? "" : (String) data[4]);
					
					List<Object[]> list2 = farmDao.listFarmByFarmer(dto.getId());
					dtos2 = new ArrayList<>();
					if (list2.size() == 0) {
						result.put("status", 99);
						result.put("message", "Data tidak tersedia");
						result.put("Object", null);
					} else {
						for (Object[] data2 : list2) {
							FarmOfflineDto dto2 = new FarmOfflineDto();
							dto2.setFarmerId((Integer) data2[0] == null ? 0 : (Integer) data2[0]);
							dto2.setFarmCode((String) data2[1] == null ? "" : (String) data2[1]);
							dto2.setFieldArea(((BigDecimal) data2[2]).equals(BigDecimal.ZERO) ? 0d : ((BigDecimal) data2[2]).doubleValue());
							dto2.setVillage((String) data2[3] == null ? "" : (String) data2[3]);
							
							List<Object[]> list3 = farmDao.listPaddyFieldByFarm(dto2.getFarmCode());
							dtos3 = new ArrayList<>();
							if (list3.size() == 0) {
								result.put("status", 99);
								result.put("message", "Data tidak tersedia");
								result.put("Object", null);
							} else {
								for(Object[] data3 : list3){
									PaddyFieldAreaOfflineDto dto3 = new PaddyFieldAreaOfflineDto();
									dto3.setPaddyFieldCode((String) data3[0]== null ? "" : (String) data3[0]);
									dto3.setFieldArea(((BigDecimal) data3[1]).equals(BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) data3[1]);
									
									DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
									try {
										String strTanggalMasuk = dateFormat.format((Date) data3[2]);
										dto3.setPlantingDate(strTanggalMasuk);
									} catch (Exception e) {
										dto3.setPlantingDate("");
									}
									
									dto3.setPlantingPeriod((String) data3[3]== null ? "" : (String) data3[3]);
									dto3.setFarmCode((String) data3[4]== null ? "" : (String) data3[4]);
									dtos3.add(dto3);
								}
								
							}
							dto2.setPaddyFieldAreaOfflineDtos(dtos3);
							dtos2.add(dto2);
						}
					}
					
					dto.setFarmOfflineDtos(dtos2);					
					dtos.add(dto);
					result.put("status", 1);
					result.put("message", "Sukses");
					result.put("Object", dtos);
				}
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e.getMessage());
			result.put("Object", null);
		}
		
		return result;
	}
	
	public List<Integer> getAllMyTeam(Integer personId) {
		List<Integer> listParam1 = new ArrayList<>();
		listParam1.add(personId);
		List<MobEmployee> list1 = employeeDao.findManagedEmployees(listParam1);
		
		List<Integer> listId = new ArrayList<>();
		for (MobEmployee e : list1) {
			listId.add(e.getId());
		}
		
		List<MobEmployee> list2 = new ArrayList<>();
		if (listId != null && listId.size() > 0) {
			for (int i = 1; i < 10; i++) {
				list2 = employeeDao.findManagedEmployees(listId);
				if (list2.size() > 0) {
					for (MobEmployee f : list2) {
						listId.add(f.getId());
					}
				}
			}
		}
		
		listId.add(personId);
		
		LinkedHashSet<Integer> hashSet = new LinkedHashSet<>(listId);
		List<Integer> listOutput = new ArrayList<>(hashSet);
		return listOutput;
	}
	
}
