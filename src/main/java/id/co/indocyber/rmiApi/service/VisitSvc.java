package id.co.indocyber.rmiApi.service;

import id.co.indocyber.rmiApi.dto.AllVisitDto;
import id.co.indocyber.rmiApi.dto.AllVisitListDto;
import id.co.indocyber.rmiApi.dto.InputVisitAllDto;
import id.co.indocyber.rmiApi.dto.MobScheduleVisitToFarmerHeaderDto;
import id.co.indocyber.rmiApi.dto.MobStandDto;
import id.co.indocyber.rmiApi.dto.NewInputVisitAllDto;
import id.co.indocyber.rmiApi.dto.VisitAppraisementDto;
import id.co.indocyber.rmiApi.dto.VisitOtherActivityDto;
import id.co.indocyber.rmiApi.dto.VisitScoringDto;

import java.util.HashMap;

public interface VisitSvc {
	public HashMap<String, Object> listVisit(int personId);

	public HashMap<String, Object> listFarmByFarmer(int farmerId);

	public HashMap<String, Object> listFarmByFarmerDetail(int farmCode);

	public HashMap<String, Object> visitOtherActivity(VisitOtherActivityDto dto, String username);

	public HashMap<String, Object> visitScoring(VisitScoringDto dto, String username);

	public HashMap<String, Object> visitAppraisement(VisitAppraisementDto dto, String username);

	public HashMap<String, Object> inputStand(MobStandDto dto, String username);

	public HashMap<String, Object> inputVisitHeader(MobScheduleVisitToFarmerHeaderDto dto, String username);

	public HashMap<String, Object> listOfFarm(int farmerId, String farmCode);

	public HashMap<String, Object> listVisitDetail(String visitNo, int farmerId, int personId);

	public HashMap<String, Object> visitAll(AllVisitListDto dto, String username, int appraisement, int scoring);

	public HashMap<String, Object> visitHistory(String search, String dateFrom, String dateTo, int personId);

	public HashMap<String, Object> listFarmer(String username);

	public HashMap<String, Object> ListVisitOtherActivity(String visitNo);

	public HashMap<String, Object> listStand(String visitNo,Integer farmerId, String farmCode);

	public HashMap<String, Object> listHistoryStand(String visitNo);

	public HashMap<String, Object> listVisitHistoryHeader(String visitNo, String paddyFieldCode);

	public HashMap<String, Object> inputVisitAll(InputVisitAllDto inputVisitAll, String username, int otherActivity, int stand, int visit, int appraisement, int scoring);

	public HashMap<String, Object> listStandNewVisit(Integer farmerId, String farmCode);

	public HashMap<String, Object> newInputVisitAll(NewInputVisitAllDto inputVisitAll, String username, int otherActivity, int stand, int visit, int appraisement, int scoring);

	public HashMap<String, Object> getListDelegasi(String username);

	public HashMap<String, Object> terimaTugas(String username, String visitNo);

	public HashMap<String, Object> delegasikan(String username, String visitNo, Integer onFarmId);

}
