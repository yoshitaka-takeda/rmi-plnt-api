package id.co.indocyber.rmiApi.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.container.ContainerException;

import id.co.indocyber.rmiApi.config.ConstantVariabel;
import id.co.indocyber.rmiApi.dao.MUserDao;
import id.co.indocyber.rmiApi.dto.LoginDto;
import id.co.indocyber.rmiApi.dto.LoginVMDto;
import id.co.indocyber.rmiApi.dto.RegImeiDto;
import id.co.indocyber.rmiApi.entity.MUser;
import id.co.indocyber.rmiApi.service.LoginSvc;


@Service("loginSvc")
@Transactional
public class LoginSvcImpl implements LoginSvc {
	
	@Autowired
	MUserDao mUserDao;
	
	
	@Override
	public HashMap<String, Object> login(LoginVMDto loginVMDto) {
		HashMap<String, Object> result = new HashMap<>();
		if (checkDuplicateUser(cekNoIndo(loginVMDto.getPhoneNum()))) {
			if (checkUserId(cekNoIndo(loginVMDto.getPhoneNum()))) {
				if (checkActiveUser(cekNoIndo(loginVMDto.getPhoneNum()))) {
					if (checkImei(cekNoIndo(loginVMDto.getPhoneNum()), loginVMDto.getImei())) {
						if (checkPassword(cekNoIndo(loginVMDto.getPhoneNum()), loginVMDto.getPassword())) {
							if (checkPassDefault(cekNoIndo(loginVMDto.getPhoneNum()), loginVMDto.getPassword())) {
								try {
									result.put("status", 1);
									result.put("message", "success");
									result.put("Object", getRole(cekNoIndo(loginVMDto.getPhoneNum()), loginVMDto.getPassword(), loginVMDto.getToken()));
								} catch (Exception e) {
									result.put("status", 0);
									result.put("message", e);
									result.put("Object", null);
								}
								
							} else {
								
								result.put("status", Integer.valueOf(2));
								result.put("message", "Password masih default");
								result.put("Object", null);
								
							}
						} else {
							result.put("status", Integer.valueOf(99));
							result.put("message", "Password salah");
							result.put("Object", null);
						}
						
					} else {
						result.put("status", Integer.valueOf(3));
						result.put("message", "Imei tidak ditemukan");
						result.put("Object", null);
					}
				} else {
					
					result.put("status", Integer.valueOf(99));
					result.put("message", "Username tidak aktif");
					result.put("Object", null);
				}
			} else {
				result.put("status", Integer.valueOf(99));
				result.put("message", "Username tidak ditemukan");
				result.put("Object", null);
			}
		} else {
			result.put("status", Integer.valueOf(99));
			result.put("message", "Username duplikat, atau no hp sudah ada yang menggunakan");
			result.put("Object", null);
		}
		return result;
	}
	
	public boolean checkDuplicateUser(String phoneNum) {
		boolean msg;
		Long mUser = mUserDao.checkDuplicateUser(phoneNum);
		if (mUser == 1) {
			msg = true;
		} else {
			msg = false;
		}
		return msg;
	}
	
	public boolean checkUserId(String phoneNum) {
		boolean msg;
		MUser mUser = mUserDao.checkUser(phoneNum);
		if (mUser != null) {
			msg = true;
		} else {
			msg = false;
		}
		return msg;
	}
	
	public boolean checkActiveUser(String phoneNum) {
		boolean msg;
		Boolean mUser = this.mUserDao.checkActiveUser(phoneNum);
		if (mUser.booleanValue()) {
			msg = true;
		} else {
			msg = false;
		}
		return msg;
	}
	
	public boolean checkPassDefault(String phoneNum, String password) {
		String sha_pwd = hash(phoneNum, password);
		boolean msg;
		MUser mUser = mUserDao.checkUser(phoneNum);
		String pswd = hash(phoneNum, ConstantVariabel._passDefault);
		if (mUser.getPswdDefault().equals(sha_pwd) || pswd.equals(sha_pwd) || mUser.getPassword().equals(pswd)) {
			msg = false;
		} else {
			msg = true;
		}
		return msg;
	}
	
	public boolean checkPassword(String phoneNum, String password) {
		String sha_pwd = hash(phoneNum, password);
		boolean msg;
		MUser mUser = mUserDao.checkPassword(phoneNum, sha_pwd);
		if (mUser != null) {
			msg = true;
		} else {
			msg = false;
		}
		return msg;
	}
	
	public boolean checkImei(String phoneNum, String imei) {
		boolean msg = true;
		MUser data1 = mUserDao.checkImei(phoneNum, imei);
		
		if (data1 != null) {
			msg = true;
		} else {
			msg = false;
		}
		
		return msg;
	}
	
	public LoginDto getRole(String phoneNum, String password, String token) {
		// List<LoginDto> mRoleDDtos = new ArrayList<>();
		String sha_pwd = hash(phoneNum, password);
		MUser mUser = mUserDao.checkUser(phoneNum);
		if (mUser.getToken() == null || mUser.getToken() == "") {
			mUser.setToken(token);
			mUserDao.save(mUser);
		} else {
			mUser.setToken(token);
			mUserDao.save(mUser);
		}
		List<Object[]> objects = mUserDao.getRole(phoneNum, sha_pwd);
		LoginDto loginDto = new LoginDto();
		for (Object[] o : objects) {
			loginDto.setRoleId((int) o[0]);
			loginDto.setRoleName((String) o[1]);
			loginDto.setUsername((String) o[2]);
			loginDto.setEmail((String) o[3]);
			loginDto.setFullname((String) o[4]);
			loginDto.setPersonId((int) o[5]);
			if (((String) o[6]).equals(null) || ((String) o[6]).equals("")) {
				loginDto.setPhotoText(ConstantVariabel._ipServer + "/rmi-assets/user/profile-" + cUName(loginDto.getUsername()) + ".jpg");
			} else {
				loginDto.setPhotoText((String) o[6]);
			}
			loginDto.setToken((String) o[7]);
			loginDto.setPhoneNum((String) o[8]);
			
			if (((int) o[0]) == 2) {
				String ref = "";
				try {
					ref = mUserDao.getreferenceCodeFarmer(phoneNum, sha_pwd);
				} catch (Exception e) {
					ref = "";
					System.err.println(e.getMessage());
				}
				loginDto.setReferenceCode(ref);
			}else{
				loginDto.setReferenceCode("");
			}
		}
		return loginDto;
	}
	
	@Override
	public HashMap<String, Object> registerImei(RegImeiDto dto) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Date now = new Date();
		if (checkDuplicateUser(dto.getPhoneNum())) {
			if (checkUserId(dto.getPhoneNum())) {
				if (checkActiveUser(dto.getPhoneNum())) {
					if (checkImei(dto.getPhoneNum(), dto.getImei()) == false) {
						if (checkDuplicateImei(dto.getImei())) {
							try {
								String pswd = hash(dto.getPhoneNum(), ConstantVariabel._passDefault);
								MUser mUser = mUserDao.checkUser(dto.getPhoneNum());
								mUser.setImei(dto.getImei());
								mUser.setPassword(pswd);
								mUser.setModifiedBy(mUser.getUsername());
								mUser.setModifiedDate(now);
								this.mUserDao.save(mUser);
								
								result.put("status", Integer.valueOf(1));
								result.put("message", "Success");
								result.put("Object", null);
							} catch (Exception e) {
								result.put("status", Integer.valueOf(0));
								result.put("message", e.getMessage());
								result.put("Object", null);
							}
						} else {
							result.put("status", Integer.valueOf(2));
							result.put("message", "Imei sudah ada yang menggunakan");
							result.put("Object", null);
						}
					} else {
						result.put("status", Integer.valueOf(3));
						result.put("message", "Imei sudah terdaftar");
						result.put("Object", null);
					}
				} else {
					result.put("status", Integer.valueOf(99));
					result.put("message", "Username tidak aktif");
					result.put("Object", null);
				}
			} else {
				result.put("status", Integer.valueOf(99));
				result.put("message", "Username tidak ditemukan");
				result.put("Object", null);
			}
		} else {
			result.put("status", Integer.valueOf(99));
			result.put("message", "Username duplikat, atau no hp sudah ada yang menggunakan");
			result.put("Object", null);
		}
		return result;
	}
	
	private boolean checkDuplicateImei(String imei) {
		boolean msg = true;
		MUser data1 = mUserDao.checkDuplicateImei(imei);
		
		if (data1 != null) {
			msg = false;
		} else {
			msg = true;
		}
		
		return msg;
	}
	
	private boolean checkImeiInUser(String email, String imei) {
		boolean msg = true;
		MUser data1 = mUserDao.checkDuplicateImei(imei);
		
		if (data1 != null) {
			msg = false;
		} else {
			msg = true;
		}
		
		return msg;
	}
	
	private String hash(String phoneNum, String password) {
		MUser mUser = mUserDao.checkUser(phoneNum);
		String username = mUser.getUsername();
		String toHash = username + "rejoso" + password + "manisindo";
		return DigestUtils.sha1Hex(toHash);
	}
	
	public String cekNoIndo(String noHp) {
		if (noHp.equals("-")) {
			return null;
		} else if (noHp.substring(0, 2).equals("08")) {
			return "0" + noHp.substring(1);
		} else if (noHp.substring(0, 3).equals("628")) {
			return "0" + noHp.substring(2);
		} else if (noHp.substring(0, 1).equals("8")) {
			return "0" + noHp;
		} else if (noHp.substring(0, 4).equals("+628")) {
			return "0" + noHp.substring(3);
		} else {
			return null;
		}
	}
	
	public String cUName(String username){
		return username.replace(".", "-");
	}
	
}
