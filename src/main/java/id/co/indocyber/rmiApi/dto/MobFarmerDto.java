package id.co.indocyber.rmiApi.dto;

public class MobFarmerDto {
	private int id;
	private String fullname, village, photo_text, farmerCode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getPhoto_text() {
		return photo_text;
	}

	public void setPhoto_text(String photo_text) {
		this.photo_text = photo_text;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}
}
