package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;

public class PaddyFieldAreaOfflineDto {
	private String farmCode;
	private String paddyFieldCode;
	private BigDecimal fieldArea;
	private String plantingDate;
	private String plantingPeriod;
	
	public String getFarmCode() {
		return farmCode;
	}
	
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	
	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}
	
	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}
	
	public BigDecimal getFieldArea() {
		return fieldArea;
	}
	
	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}
	
	public String getPlantingDate() {
		return plantingDate;
	}
	
	public void setPlantingDate(String plantingDate) {
		this.plantingDate = plantingDate;
	}
	
	public String getPlantingPeriod() {
		return plantingPeriod;
	}
	
	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}
	
}
