package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;

public class VisitScoringDto {
	private int scheduleVisitDetailId;
	private String paramCode;
	private String paramValue;
	private String criteria;
	private BigDecimal weight;
	private BigDecimal score;
	private String notes;

	public int getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(int scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
