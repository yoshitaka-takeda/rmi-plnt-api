package id.co.indocyber.rmiApi.dto;

public class SptaEditDto {
	private int sptaNum, vehicleType;
	private String vehicleNo, noDriver, driverName, farmCode;

	public int getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(int sptaNum) {
		this.sptaNum = sptaNum;
	}

	public int getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getNoDriver() {
		return noDriver;
	}

	public void setNoDriver(String noDriver) {
		this.noDriver = noDriver;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
}
