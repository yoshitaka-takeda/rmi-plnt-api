package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;

public class MSugarcaneParamScoringDto {
	private int param_code;
	private String param_value, criteria;
	private BigDecimal weight;

	public int getParam_code() {
		return param_code;
	}

	public void setParam_code(int param_code) {
		this.param_code = param_code;
	}

	public String getParam_value() {
		return param_value;
	}

	public void setParam_value(String param_value) {
		this.param_value = param_value;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

}
