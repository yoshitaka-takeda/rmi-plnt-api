package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;
import java.util.Date;

public class SptaHistoryFarmerDto {
	private Integer sptaNum;
	private String farmerName;
	private Integer farmerCode;
	private BigDecimal qty;
	private String status;
	private String vehicleNo, driverNo;
	private Integer driverId;
	private Integer vehicleType;
	private String documentDate;
	private String expiredDate;
	private BigDecimal rendemen;
	private BigDecimal brix;
	private BigDecimal pol;
	private String source;
	private Integer baseEntry;
	private Integer baseLine;
	private String referenceCode;
	private Double latitude, longitude;
	private String contractNo2;
	private String contractNo1;
	private String monitoringPos;

	public Integer getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(Integer sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getFarmerName() {
		return farmerName;
	}

	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}

	public Integer getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(Integer farmerCode) {
		this.farmerCode = farmerCode;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public Integer getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public BigDecimal getRendemen() {
		return rendemen;
	}

	public void setRendemen(BigDecimal rendemen) {
		this.rendemen = rendemen;
	}

	public BigDecimal getBrix() {
		return brix;
	}

	public void setBrix(BigDecimal brix) {
		this.brix = brix;
	}

	public BigDecimal getPol() {
		return pol;
	}

	public void setPol(BigDecimal pol) {
		this.pol = pol;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getBaseEntry() {
		return baseEntry;
	}

	public void setBaseEntry(Integer baseEntry) {
		this.baseEntry = baseEntry;
	}

	public Integer getBaseLine() {
		return baseLine;
	}

	public void setBaseLine(Integer baseLine) {
		this.baseLine = baseLine;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}
	
	public String getContractNo2() {
		return contractNo2;
	}
	
	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}
	
	public String getContractNo1() {
		return contractNo1;
	}
	
	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}

	public String getMonitoringPos() {
		return monitoringPos;
	}

	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}
}
