package id.co.indocyber.rmiApi.dto;

import java.util.List;

public class MobFarmDto {
	private Integer farmerId;
	private String farmCode, village;
	private Double fieldArea;
	private List<PaddyFieldAreaDto> fieldAreaDto;

	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public Double getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}

	public List<PaddyFieldAreaDto> getFieldAreaDto() {
		return fieldAreaDto;
	}

	public void setFieldAreaDto(List<PaddyFieldAreaDto> fieldAreaDto) {
		this.fieldAreaDto = fieldAreaDto;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

}
