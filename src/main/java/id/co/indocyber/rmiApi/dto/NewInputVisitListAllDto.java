package id.co.indocyber.rmiApi.dto;

import java.util.List;

public class NewInputVisitListAllDto {
	private List<NewInputVisitAllDto>	newInputVisitAllDtos;

	public List<NewInputVisitAllDto> getNewInputVisitAllDtos() {
		return newInputVisitAllDtos;
	}

	public void setNewInputVisitAllDtos(List<NewInputVisitAllDto> newInputVisitAllDtos) {
		this.newInputVisitAllDtos = newInputVisitAllDtos;
	}

}
