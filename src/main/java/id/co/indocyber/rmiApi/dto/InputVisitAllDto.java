package id.co.indocyber.rmiApi.dto;

public class InputVisitAllDto {
	private VisitOtherActivityDto otherActivityDto;
	private MobStandDto standDto;
	private AllVisitListDto visitDto;

	public VisitOtherActivityDto getOtherActivityDto() {
		return otherActivityDto;
	}

	public void setOtherActivityDto(VisitOtherActivityDto otherActivityDto) {
		this.otherActivityDto = otherActivityDto;
	}

	public MobStandDto getStandDto() {
		return standDto;
	}

	public void setStandDto(MobStandDto standDto) {
		this.standDto = standDto;
	}

	public AllVisitListDto getVisitDto() {
		return visitDto;
	}

	public void setVisitDto(AllVisitListDto visitDto) {
		this.visitDto = visitDto;
	}

	
}
