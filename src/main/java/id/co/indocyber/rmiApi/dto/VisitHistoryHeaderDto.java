package id.co.indocyber.rmiApi.dto;

public class VisitHistoryHeaderDto {
	private String fullname, village, status, photoText, statusSort, statusLong, visitNo;
	private int totalFarm, farmerId;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPhotoText() {
		return photoText;
	}

	public void setPhotoText(String photoText) {
		this.photoText = photoText;
	}

	public int getTotalFarm() {
		return totalFarm;
	}

	public void setTotalFarm(int totalFarm) {
		this.totalFarm = totalFarm;
	}

	public String getStatusSort() {
		return statusSort;
	}

	public void setStatusSort(String statusSort) {
		this.statusSort = statusSort;
	}

	public String getStatusLong() {
		return statusLong;
	}

	public void setStatusLong(String statusLong) {
		this.statusLong = statusLong;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

}
