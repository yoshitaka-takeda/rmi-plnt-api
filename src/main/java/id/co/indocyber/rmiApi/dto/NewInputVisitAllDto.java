package id.co.indocyber.rmiApi.dto;

public class NewInputVisitAllDto {
	private NewVisitOtherActivityDto	otherActivityDto;
	private NewVisitStandDto			standDto;
	private NewVisitListDto				visitDto;
	private String						farmerCode, farmCode;

	public NewVisitOtherActivityDto getOtherActivityDto() {
		return otherActivityDto;
	}

	public void setOtherActivityDto(NewVisitOtherActivityDto otherActivityDto) {
		this.otherActivityDto = otherActivityDto;
	}

	public NewVisitStandDto getStandDto() {
		return standDto;
	}

	public void setStandDto(NewVisitStandDto standDto) {
		this.standDto = standDto;
	}

	public NewVisitListDto getVisitDto() {
		return visitDto;
	}

	public void setVisitDto(NewVisitListDto visitDto) {
		this.visitDto = visitDto;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

}
