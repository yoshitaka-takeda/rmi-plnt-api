package id.co.indocyber.rmiApi.dto;

import java.util.Date;
import java.util.List;

public class FarmDto {
	private String village, photoText, fullName, visitNo, taskActivity, farmCode, farmVillage;
	private String visitDate, visitExpiredDate;
	private double fieldAreaTotal;
	private int farmerId;
	private List<PaddyFieldAreaDto> areaDto;

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public double getFieldAreaTotal() {
		return fieldAreaTotal;
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public void setFieldAreaTotal(double fieldAreaTotal) {
		this.fieldAreaTotal = fieldAreaTotal;
	}

	public List<PaddyFieldAreaDto> getAreaDto() {
		return areaDto;
	}

	public void setAreaDto(List<PaddyFieldAreaDto> areaDto) {
		this.areaDto = areaDto;
	}

	public String getPhotoText() {
		return photoText;
	}

	public void setPhotoText(String photoText) {
		this.photoText = photoText;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public String getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}

	public String getVisitExpiredDate() {
		return visitExpiredDate;
	}

	public void setVisitExpiredDate(String visitExpiredDate) {
		this.visitExpiredDate = visitExpiredDate;
	}

	public String getTaskActivity() {
		return taskActivity;
	}

	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getFarmVillage() {
		return farmVillage;
	}

	public void setFarmVillage(String farmVillage) {
		this.farmVillage = farmVillage;
	}

}
