package id.co.indocyber.rmiApi.dto;

import java.util.Date;

public class MobWeighbridgeResultDto {
	private String kodeMitra, namaMitra, noKontrak, namaPetani, noPolisi,
			namaSupir, posPantau;
	private String status;
	private Integer noSpta;
	private String tanggalMasuk, tangalKeluar;
	private Double beratKotor, beratKendaraan, netto, beratSetelahKualitas,
			rafaksi;
	private Double jumlahDiBayar;
	private String contractNo2;

	public String getKodeMitra() {
		return kodeMitra;
	}

	public void setKodeMitra(String kodeMitra) {
		this.kodeMitra = kodeMitra;
	}

	public String getNamaMitra() {
		return namaMitra;
	}

	public void setNamaMitra(String namaMitra) {
		this.namaMitra = namaMitra;
	}

	public String getNoKontrak() {
		return noKontrak;
	}

	public void setNoKontrak(String noKontrak) {
		this.noKontrak = noKontrak;
	}

	public String getNamaPetani() {
		return namaPetani;
	}

	public void setNamaPetani(String namaPetani) {
		this.namaPetani = namaPetani;
	}

	public String getNoPolisi() {
		return noPolisi;
	}

	public void setNoPolisi(String noPolisi) {
		this.noPolisi = noPolisi;
	}

	public String getNamaSupir() {
		return namaSupir;
	}

	public void setNamaSupir(String namaSupir) {
		this.namaSupir = namaSupir;
	}

	public String getPosPantau() {
		return posPantau;
	}

	public void setPosPantau(String posPantau) {
		this.posPantau = posPantau;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getNoSpta() {
		return noSpta;
	}

	public void setNoSpta(Integer noSpta) {
		this.noSpta = noSpta;
	}

	public String getTanggalMasuk() {
		return tanggalMasuk;
	}

	public void setTanggalMasuk(String tanggalMasuk) {
		this.tanggalMasuk = tanggalMasuk;
	}

	public String getTangalKeluar() {
		return tangalKeluar;
	}

	public void setTangalKeluar(String tangalKeluar) {
		this.tangalKeluar = tangalKeluar;
	}

	public Double getBeratKotor() {
		return beratKotor;
	}

	public void setBeratKotor(Double beratKotor) {
		this.beratKotor = beratKotor;
	}

	public Double getBeratKendaraan() {
		return beratKendaraan;
	}

	public void setBeratKendaraan(Double beratKendaraan) {
		this.beratKendaraan = beratKendaraan;
	}

	public Double getNetto() {
		return netto;
	}

	public void setNetto(Double netto) {
		this.netto = netto;
	}

	public Double getBeratSetelahKualitas() {
		return beratSetelahKualitas;
	}

	public void setBeratSetelahKualitas(Double beratSetelahKualitas) {
		this.beratSetelahKualitas = beratSetelahKualitas;
	}

	public Double getRafaksi() {
		return rafaksi;
	}

	public void setRafaksi(Double rafaksi) {
		this.rafaksi = rafaksi;
	}

	public Double getJumlahDiBayar() {
		return jumlahDiBayar;
	}

	public void setJumlahDiBayar(Double jumlahDiBayar) {
		this.jumlahDiBayar = jumlahDiBayar;
	}

	public String getContractNo2() {
		return contractNo2;
	}

	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}

}
