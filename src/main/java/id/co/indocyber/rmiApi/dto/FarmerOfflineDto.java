package id.co.indocyber.rmiApi.dto;

import java.util.List;

public class FarmerOfflineDto {
	private Integer id;
	private String fullname, village, photo_text, farmerCode;
	private List<FarmOfflineDto> farmOfflineDtos;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	public String getVillage() {
		return village;
	}
	
	public void setVillage(String village) {
		this.village = village;
	}
	
	public String getPhoto_text() {
		return photo_text;
	}
	
	public void setPhoto_text(String photo_text) {
		this.photo_text = photo_text;
	}
	
	public String getFarmerCode() {
		return farmerCode;
	}
	
	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}
	
	public List<FarmOfflineDto> getFarmOfflineDtos() {
		return farmOfflineDtos;
	}
	
	public void setFarmOfflineDtos(List<FarmOfflineDto> farmOfflineDtos) {
		this.farmOfflineDtos = farmOfflineDtos;
	}
	
}
