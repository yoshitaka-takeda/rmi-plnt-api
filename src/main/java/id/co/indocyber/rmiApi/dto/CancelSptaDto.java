package id.co.indocyber.rmiApi.dto;

public class CancelSptaDto {
	private int sptaNum;
	private String username;
	private String reason;
	private String locationCancel;

	public int getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(int sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLocationCancel() {
		return locationCancel;
	}

	public void setLocationCancel(String locationCancel) {
		this.locationCancel = locationCancel;
	}
}
