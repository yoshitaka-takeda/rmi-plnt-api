package id.co.indocyber.rmiApi.dto;

import java.util.Date;

public class NewVisitOtherActivityDto {
	private Date	entryDate;
	private String	activityText;

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getActivityText() {
		return activityText;
	}

	public void setActivityText(String activityText) {
		this.activityText = activityText;
	}

}
