package id.co.indocyber.rmiApi.dto;

import java.util.List;

public class AllVisitListDto {
	private List<AllVisitDto>	allVisitDtos;

	public List<AllVisitDto> getAllVisitDtos() {
		return allVisitDtos;
	}

	public void setAllVisitDtos(List<AllVisitDto> allVisitDtos) {
		this.allVisitDtos = allVisitDtos;
	}

}
