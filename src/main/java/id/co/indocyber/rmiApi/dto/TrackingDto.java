package id.co.indocyber.rmiApi.dto;

public class TrackingDto {
	private Double latitude, longitude;
	private int sptaNum;
	private String username;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public int getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(int sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
