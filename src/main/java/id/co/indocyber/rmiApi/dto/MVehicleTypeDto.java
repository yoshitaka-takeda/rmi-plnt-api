package id.co.indocyber.rmiApi.dto;

public class MVehicleTypeDto {
	private int id;
	private String vehicle_type_name, reference_code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVehicle_type_name() {
		return vehicle_type_name;
	}

	public void setVehicle_type_name(String vehicle_type_name) {
		this.vehicle_type_name = vehicle_type_name;
	}

	public String getReference_code() {
		return reference_code;
	}

	public void setReference_code(String reference_code) {
		this.reference_code = reference_code;
	}

}
