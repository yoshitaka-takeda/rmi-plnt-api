package id.co.indocyber.rmiApi.dto;

import java.sql.Date;

public class MobScheduleVisitToFarmerHeaderDto {
	private String visitNo;
	private Date estimateHarvestTime;
	private String planText;
	private String evaluationText;

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public Date getEstimateHarvestTime() {
		return estimateHarvestTime;
	}

	public void setEstimateHarvestTime(Date estimateHarvestTime) {
		this.estimateHarvestTime = estimateHarvestTime;
	}

	public String getPlanText() {
		return planText;
	}

	public void setPlanText(String planText) {
		this.planText = planText;
	}

	public String getEvaluationText() {
		return evaluationText;
	}

	public void setEvaluationText(String evaluationText) {
		this.evaluationText = evaluationText;
	}

}
