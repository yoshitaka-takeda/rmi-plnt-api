package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MobStandDto {
	private String visitNo;
	private String entryDate;
	private BigDecimal farmArea;
	private BigDecimal tonnageEstimation;
	private BigDecimal lastStandTonnage;
	private BigDecimal lastStandArea;
	private BigDecimal standTonnage;
	private BigDecimal standArea;
	private String activityText;

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public BigDecimal getFarmArea() {
		return farmArea;
	}

	public void setFarmArea(BigDecimal farmArea) {
		this.farmArea = farmArea;
	}

	public BigDecimal getTonnageEstimation() {
		return tonnageEstimation;
	}

	public void setTonnageEstimation(BigDecimal tonnageEstimation) {
		this.tonnageEstimation = tonnageEstimation;
	}

	public BigDecimal getLastStandTonnage() {
		return lastStandTonnage;
	}

	public void setLastStandTonnage(BigDecimal lastStandTonnage) {
		this.lastStandTonnage = lastStandTonnage;
	}

	public BigDecimal getLastStandArea() {
		return lastStandArea;
	}

	public void setLastStandArea(BigDecimal lastStandArea) {
		this.lastStandArea = lastStandArea;
	}

	public BigDecimal getStandTonnage() {
		return standTonnage;
	}

	public void setStandTonnage(BigDecimal standTonnage) {
		this.standTonnage = standTonnage;
	}

	public BigDecimal getStandArea() {
		return standArea;
	}

	public void setStandArea(BigDecimal standArea) {
		this.standArea = standArea;
	}

	public String getActivityText() {
		return activityText;
	}

	public void setActivityText(String activityText) {
		this.activityText = activityText;
	}
}
