package id.co.indocyber.rmiApi.dto;

import java.util.List;

public class NewVisitListDto {
	private List<NewVisitDto> newVisitDtos;

	public List<NewVisitDto> getNewVisitDtos() {
		return newVisitDtos;
	}

	public void setNewVisitDtos(List<NewVisitDto> newVisitDtos) {
		this.newVisitDtos = newVisitDtos;
	}
	
}
