package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DriverSptaDto {
	private String tanggalDokumen, tanggalExpired;
	private Integer register, sptaNum;
	private String namaPemilik, kecamatan, desa, namaDriver, noHp, noKendaraan, tipePetani, tipeTruk;
	private int status;
	private String contractNo1, contractNo2, farmerCode, coopName, monitoringPos;
	private BigDecimal brix;
	
	public String getTanggalDokumen() {
		return tanggalDokumen;
	}
	
	public void setTanggalDokumen(String tanggalDokumen) {
		this.tanggalDokumen = tanggalDokumen;
	}
	
	public String getTanggalExpired() {
		return tanggalExpired;
	}
	
	public void setTanggalExpired(String tanggalExpired) {
		this.tanggalExpired = tanggalExpired;
	}
	
	public Integer getRegister() {
		return register;
	}
	
	public void setRegister(Integer register) {
		this.register = register;
	}
	
	public Integer getSptaNum() {
		return sptaNum;
	}
	
	public void setSptaNum(Integer sptaNum) {
		this.sptaNum = sptaNum;
	}
	
	public String getNamaPemilik() {
		return namaPemilik;
	}
	
	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}
	
	public String getKecamatan() {
		return kecamatan;
	}
	
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	
	public String getDesa() {
		return desa;
	}
	
	public void setDesa(String desa) {
		this.desa = desa;
	}
	
	public String getNamaDriver() {
		return namaDriver;
	}
	
	public void setNamaDriver(String namaDriver) {
		this.namaDriver = namaDriver;
	}
	
	public String getNoHp() {
		return noHp;
	}
	
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	
	public String getNoKendaraan() {
		return noKendaraan;
	}
	
	public void setNoKendaraan(String noKendaraan) {
		this.noKendaraan = noKendaraan;
	}
	
	public String getTipePetani() {
		return tipePetani;
	}
	
	public void setTipePetani(String tipePetani) {
		this.tipePetani = tipePetani;
	}
	
	public String getTipeTruk() {
		return tipeTruk;
	}
	
	public void setTipeTruk(String tipeTruk) {
		this.tipeTruk = tipeTruk;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getContractNo1() {
		return contractNo1;
	}
	
	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}
	
	public String getContractNo2() {
		return contractNo2;
	}
	
	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}
	
	public String getFarmerCode() {
		return farmerCode;
	}
	
	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}
	
	public String getCoopName() {
		return coopName;
	}
	
	public void setCoopName(String coopName) {
		this.coopName = coopName;
	}
	
	public String getMonitoringPos() {
		return monitoringPos;
	}
	
	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}
	
	public BigDecimal getBrix() {
		return brix;
	}
	
	public void setBrix(BigDecimal brix) {
		this.brix = brix;
	}
	
}
