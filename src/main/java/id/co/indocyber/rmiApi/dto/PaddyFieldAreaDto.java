package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;

public class PaddyFieldAreaDto {
	private int visitDetailId;
	private String paddyFieldCode;
	private BigDecimal fieldArea;
	private String plantingDate;
	private String plantingPeriod;
	private Double scoring;
	private Double ranking;
	
	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}
	
	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}
	
	public BigDecimal getFieldArea() {
		return fieldArea;
	}
	
	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}
	
	public Double getScoring() {
		return scoring;
	}
	
	public void setScoring(Double scoring) {
		this.scoring = scoring;
	}
	
	public Double getRanking() {
		return ranking;
	}
	
	public void setRanking(Double ranking) {
		this.ranking = ranking;
	}
	
	public int getVisitDetailId() {
		return visitDetailId;
	}
	
	public void setVisitDetailId(int visitDetailId) {
		this.visitDetailId = visitDetailId;
	}
	
	public String getPlantingDate() {
		return plantingDate;
	}
	
	public void setPlantingDate(String plantingDate) {
		this.plantingDate = plantingDate;
	}
	
	public String getPlantingPeriod() {
		return plantingPeriod;
	}
	
	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}
	
}
