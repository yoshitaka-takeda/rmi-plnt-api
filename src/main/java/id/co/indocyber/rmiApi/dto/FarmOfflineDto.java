package id.co.indocyber.rmiApi.dto;

import java.util.List;

public class FarmOfflineDto {
	private Integer farmerId;
	private String farmCode, village;
	private Double fieldArea;
	private List<PaddyFieldAreaOfflineDto> paddyFieldAreaOfflineDtos;
	
	public Integer getFarmerId() {
		return farmerId;
	}
	
	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}
	
	public String getFarmCode() {
		return farmCode;
	}
	
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	
	public String getVillage() {
		return village;
	}
	
	public void setVillage(String village) {
		this.village = village;
	}
	
	public Double getFieldArea() {
		return fieldArea;
	}
	
	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}
	
	public List<PaddyFieldAreaOfflineDto> getPaddyFieldAreaOfflineDtos() {
		return paddyFieldAreaOfflineDtos;
	}
	
	public void setPaddyFieldAreaOfflineDtos(List<PaddyFieldAreaOfflineDto> paddyFieldAreaOfflineDtos) {
		this.paddyFieldAreaOfflineDtos = paddyFieldAreaOfflineDtos;
	}
	
}
