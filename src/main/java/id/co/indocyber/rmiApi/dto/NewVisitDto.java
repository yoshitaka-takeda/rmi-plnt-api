package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class NewVisitDto {
	private List<NewVisitScoringDto>	visitScoring;

	// ==== taksasi
	private String						paddyFieldCode;
	private String						periodName;
	private BigDecimal 					tinggiBatangSekarang;
	private BigDecimal 					tinggiBatangDitebang;
	private BigDecimal 					beratBatangperMeter;
	private BigDecimal 					beratPerBatang;
	private int 						jumlahBatangTiapLeng;
	private int 						jumlahLengperHA;
	private BigDecimal 					jumlahBatangperHA;
	private BigDecimal 					prodPerHektarKu;
	private BigDecimal 					jumlahProduksiKU;
	private String 						jadwalTebangPeriode;
	private BigDecimal 					rerataAngkaBrix;
	private BigDecimal 					estimasiRendemen;
	private BigDecimal 					kuHablurPerHa;
	private BigDecimal 					jumlahHablur;

	private Double						latitude;
	private Double						longitude;

	// ==== header
	private Date						estimateHarvestTime;
	private String						planText;
	private String						evaluationText;

	public List<NewVisitScoringDto> getVisitScoring() {
		return visitScoring;
	}

	public void setVisitScoring(List<NewVisitScoringDto> visitScoring) {
		this.visitScoring = visitScoring;
	}

	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}


	public BigDecimal getTinggiBatangSekarang() {
		return tinggiBatangSekarang;
	}

	public void setTinggiBatangSekarang(BigDecimal tinggiBatangSekarang) {
		this.tinggiBatangSekarang = tinggiBatangSekarang;
	}

	public BigDecimal getTinggiBatangDitebang() {
		return tinggiBatangDitebang;
	}

	public void setTinggiBatangDitebang(BigDecimal tinggiBatangDitebang) {
		this.tinggiBatangDitebang = tinggiBatangDitebang;
	}

	public BigDecimal getBeratBatangperMeter() {
		return beratBatangperMeter;
	}

	public void setBeratBatangperMeter(BigDecimal beratBatangperMeter) {
		this.beratBatangperMeter = beratBatangperMeter;
	}

	public BigDecimal getBeratPerBatang() {
		return beratPerBatang;
	}

	public void setBeratPerBatang(BigDecimal beratPerBatang) {
		this.beratPerBatang = beratPerBatang;
	}

	public int getJumlahBatangTiapLeng() {
		return jumlahBatangTiapLeng;
	}

	public void setJumlahBatangTiapLeng(int jumlahBatangTiapLeng) {
		this.jumlahBatangTiapLeng = jumlahBatangTiapLeng;
	}

	public int getJumlahLengperHA() {
		return jumlahLengperHA;
	}

	public void setJumlahLengperHA(int jumlahLengperHA) {
		this.jumlahLengperHA = jumlahLengperHA;
	}

	public BigDecimal getJumlahBatangperHA() {
		return jumlahBatangperHA;
	}

	public void setJumlahBatangperHA(BigDecimal jumlahBatangperHA) {
		this.jumlahBatangperHA = jumlahBatangperHA;
	}

	public BigDecimal getProdPerHektarKu() {
		return prodPerHektarKu;
	}

	public void setProdPerHektarKu(BigDecimal prodPerHektarKu) {
		this.prodPerHektarKu = prodPerHektarKu;
	}

	public BigDecimal getJumlahProduksiKU() {
		return jumlahProduksiKU;
	}

	public void setJumlahProduksiKU(BigDecimal jumlahProduksiKU) {
		this.jumlahProduksiKU = jumlahProduksiKU;
	}

	public String getJadwalTebangPeriode() {
		return jadwalTebangPeriode;
	}

	public void setJadwalTebangPeriode(String jadwalTebangPeriode) {
		this.jadwalTebangPeriode = jadwalTebangPeriode;
	}

	public BigDecimal getRerataAngkaBrix() {
		return rerataAngkaBrix;
	}

	public void setRerataAngkaBrix(BigDecimal rerataAngkaBrix) {
		this.rerataAngkaBrix = rerataAngkaBrix;
	}

	public BigDecimal getEstimasiRendemen() {
		return estimasiRendemen;
	}

	public void setEstimasiRendemen(BigDecimal estimasiRendemen) {
		this.estimasiRendemen = estimasiRendemen;
	}

	public BigDecimal getKuHablurPerHa() {
		return kuHablurPerHa;
	}

	public void setKuHablurPerHa(BigDecimal kuHablurPerHa) {
		this.kuHablurPerHa = kuHablurPerHa;
	}

	public BigDecimal getJumlahHablur() {
		return jumlahHablur;
	}

	public void setJumlahHablur(BigDecimal jumlahHablur) {
		this.jumlahHablur = jumlahHablur;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Date getEstimateHarvestTime() {
		return estimateHarvestTime;
	}

	public void setEstimateHarvestTime(Date estimateHarvestTime) {
		this.estimateHarvestTime = estimateHarvestTime;
	}

	public String getPlanText() {
		return planText;
	}

	public void setPlanText(String planText) {
		this.planText = planText;
	}

	public String getEvaluationText() {
		return evaluationText;
	}

	public void setEvaluationText(String evaluationText) {
		this.evaluationText = evaluationText;
	}

}
