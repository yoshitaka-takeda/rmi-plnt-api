package id.co.indocyber.rmiApi.dto;

import java.util.Date;

public class MobScheduleVisitToFarmerDetailDto {
	private int id;
	private String visitNo;
	private String farmCode;
	private String paddyFieldCode;
	private Date visitStartDate;
	private Date visitEndDate;
	private Double longitude;
	private Double latitude;
	private Date estimateHarvestTime;
	private String planText;
	private String evaluationText;
	private String taskActivity;
	private byte isActive;
	private String createdBy;
	private Date createdDate;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getVisitNo() {
		return visitNo;
	}
	
	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}
	
	public String getFarmCode() {
		return farmCode;
	}
	
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	
	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}
	
	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}
	
	public Date getVisitStartDate() {
		return visitStartDate;
	}
	
	public void setVisitStartDate(Date visitStartDate) {
		this.visitStartDate = visitStartDate;
	}
	
	public Date getVisitEndDate() {
		return visitEndDate;
	}
	
	public void setVisitEndDate(Date visitEndDate) {
		this.visitEndDate = visitEndDate;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Date getEstimateHarvestTime() {
		return estimateHarvestTime;
	}
	
	public void setEstimateHarvestTime(Date estimateHarvestTime) {
		this.estimateHarvestTime = estimateHarvestTime;
	}
	
	public String getPlanText() {
		return planText;
	}
	
	public void setPlanText(String planText) {
		this.planText = planText;
	}
	
	public String getEvaluationText() {
		return evaluationText;
	}
	
	public void setEvaluationText(String evaluationText) {
		this.evaluationText = evaluationText;
	}
	
	public String getTaskActivity() {
		return taskActivity;
	}
	
	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}
	
	public byte getIsActive() {
		return isActive;
	}
	
	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
