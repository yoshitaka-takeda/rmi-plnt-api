package id.co.indocyber.rmiApi.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MobPaddyFieldDto {
	private Integer id;
	private Integer farmerId;
	private String farmCode;
	private String paddyFieldCode;
	private Integer lineNum;
	private BigDecimal fieldArea;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private BigDecimal weightEstimation;
	private String sugarcaneType;
	private Integer ageOfSugarcane;
	private Date plantingDate;
	private String sugarcaneSpecies;
	private BigDecimal sugarcaneScore;
	private BigDecimal sugarcaneQty;
	private String plantingPeriod;
	private BigDecimal weighEstimation;
	private Character status;
	private String referenceCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public Integer getLineNum() {
		return lineNum;
	}

	public void setLineNum(Integer lineNum) {
		this.lineNum = lineNum;
	}

	public BigDecimal getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getWeightEstimation() {
		return weightEstimation;
	}

	public void setWeightEstimation(BigDecimal weightEstimation) {
		this.weightEstimation = weightEstimation;
	}

	public String getSugarcaneType() {
		return sugarcaneType;
	}

	public void setSugarcaneType(String sugarcaneType) {
		this.sugarcaneType = sugarcaneType;
	}

	public Integer getAgeOfSugarcane() {
		return ageOfSugarcane;
	}

	public void setAgeOfSugarcane(Integer ageOfSugarcane) {
		this.ageOfSugarcane = ageOfSugarcane;
	}

	public Date getPlantingDate() {
		return plantingDate;
	}

	public void setPlantingDate(Date plantingDate) {
		this.plantingDate = plantingDate;
	}

	public String getSugarcaneSpecies() {
		return sugarcaneSpecies;
	}

	public void setSugarcaneSpecies(String sugarcaneSpecies) {
		this.sugarcaneSpecies = sugarcaneSpecies;
	}

	public BigDecimal getSugarcaneScore() {
		return sugarcaneScore;
	}

	public void setSugarcaneScore(BigDecimal sugarcaneScore) {
		this.sugarcaneScore = sugarcaneScore;
	}

	public BigDecimal getSugarcaneQty() {
		return sugarcaneQty;
	}

	public void setSugarcaneQty(BigDecimal sugarcaneQty) {
		this.sugarcaneQty = sugarcaneQty;
	}

	public String getPlantingPeriod() {
		return plantingPeriod;
	}

	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}

	public BigDecimal getWeighEstimation() {
		return weighEstimation;
	}

	public void setWeighEstimation(BigDecimal weighEstimation) {
		this.weighEstimation = weighEstimation;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
}
