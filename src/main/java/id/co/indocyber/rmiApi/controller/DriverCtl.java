package id.co.indocyber.rmiApi.controller;

import java.util.Map;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.dto.TrackingDto;
import id.co.indocyber.rmiApi.service.DriverSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class DriverCtl {
	@Autowired
	private DriverSvc driverSvc;
	
	@RequestMapping(value = { "/loadingSugarcane" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> loadingSugarcane(@RequestBody TrackingDto dto) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.driverSvc.loading(dto);
			if (map.get("status").equals(Integer.valueOf(1)) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
	
	@RequestMapping(value = { "/destinationSugarcane" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> destinationSugarcane(@RequestBody TrackingDto dto) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.driverSvc.destination(dto);
			if (map.get("status").equals(Integer.valueOf(1)) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
	
	@RequestMapping(value = { "/exitSugarcane" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> exitSugarcane(@RequestBody TrackingDto dto) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.driverSvc.finish(dto);
			if (map.get("status").equals(Integer.valueOf(1)) || map.get("status").equals(4) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
}
