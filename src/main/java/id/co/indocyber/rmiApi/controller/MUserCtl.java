package id.co.indocyber.rmiApi.controller;

import java.util.Map;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.dto.ProfileDto;
import id.co.indocyber.rmiApi.service.MUserSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class MUserCtl {
	@Autowired
	MUserSvc mUserSvc;

	@RequestMapping(value = "/getProfile", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getProfile(@RequestHeader String phoneNum) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = mUserSvc.getProfile(phoneNum);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> customerUpdate(
			@RequestHeader String phoneNum, @RequestHeader String pass,
			@RequestHeader String newPass) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = mUserSvc.changePassword(phoneNum, pass,
					newPass);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage("Kosong");
			restResponse.setDataObject(e);
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> updateProfile(
			@RequestBody ProfileDto profil, @RequestHeader String phoneNum) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = mUserSvc.updateProfile(profil, phoneNum);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage("Kosong");
			restResponse.setDataObject(e);
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

}
