package id.co.indocyber.rmiApi.controller;

import java.util.Map;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.service.WeighBridgeResultSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class WeighBridgeResultCtl {
	@Autowired
	WeighBridgeResultSvc weighBridgeResultSvc;

	@RequestMapping(value = "/listWeighBridgeResult", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> listWeighBridgeResult(
			@RequestHeader String username,
			@RequestHeader("fromDate") String fromDate,
			@RequestHeader("toDate") String toDate) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = weighBridgeResultSvc
					.listWeighBridgeResult(username, fromDate, toDate);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/listWeighBridgeResultDriver", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> listWeighBridgeResultDriver(
			@RequestHeader String username,
			@RequestHeader("fromDate") String fromDate,
			@RequestHeader("toDate") String toDate) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = weighBridgeResultSvc
					.listWeighBridgeResultDriver(username, fromDate, toDate);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
}
