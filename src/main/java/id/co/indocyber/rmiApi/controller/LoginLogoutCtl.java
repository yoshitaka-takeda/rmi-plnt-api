package id.co.indocyber.rmiApi.controller;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.dto.LoginVMDto;
import id.co.indocyber.rmiApi.dto.RegImeiDto;
import id.co.indocyber.rmiApi.service.LoginSvc;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class LoginLogoutCtl {

	@Autowired
	private LoginSvc	loginSvc;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> login(@RequestBody LoginVMDto login) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = loginSvc.login(login);
			if (map.get("status").equals(1) || map.get("status").equals(2) || map.get("status").equals(3) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/regImei", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> regImei(@RequestBody RegImeiDto dto) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = loginSvc.registerImei(dto);
			if (map.get("status").equals(1) || map.get("status").equals(2) || map.get("status").equals(3) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
}
