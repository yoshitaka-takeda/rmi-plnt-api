package id.co.indocyber.rmiApi.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.dto.TokenDto;
import id.co.indocyber.rmiApi.dto.TrackingDto;
import id.co.indocyber.rmiApi.service.NotificationSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@RestController
@RequestMapping
public class CekNotifCtl {
	@Autowired
	private NotificationSvc notificationSvc;
	
	Boolean init = false;
	
	@RequestMapping(value = { "/notifSingle" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> testNotif(@RequestHeader String token, @RequestHeader String sptaNum) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> respon = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = notificationSvc.notifSptaFcm(token, sptaNum);
			if (map.get("status").equals(Integer.valueOf(1)) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return respon;
	}
	
	@RequestMapping(value = { "/notifMulty" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> testNotifMulty(@RequestBody TokenDto token, @RequestHeader String sptaNum) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> respon = new ResponseEntity<RestResponse>(null);
		try {
			
			Map<String, Object> map = notificationSvc.notifWeighBridgeFcm(token, sptaNum);
			if (map.get("status").equals(Integer.valueOf(1)) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return respon;
	}
	
	@RequestMapping(value = { "/notifVisit" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> notifVisit(@RequestHeader String token, @RequestHeader String noVisit) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> respon = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = notificationSvc.notifVisitFcm(token, noVisit);
			if (map.get("status").equals(Integer.valueOf(1)) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			} else {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			respon = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return respon;
	}
}
