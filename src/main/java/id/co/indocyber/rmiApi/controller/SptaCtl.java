package id.co.indocyber.rmiApi.controller;

import java.util.Map;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.dto.CancelSptaDto;
import id.co.indocyber.rmiApi.dto.EditSptaDriverTrukDto;
import id.co.indocyber.rmiApi.dto.SptaEditDto;
import id.co.indocyber.rmiApi.service.SptaSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class SptaCtl {
	@Autowired
	SptaSvc sptaSvc;

	@RequestMapping(value = "/getSpta", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getProfile(
			@RequestHeader String username, @RequestHeader String status) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = sptaSvc.listSpta(username, status);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/getHistorySpta", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getHistorySpta(
			@RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = sptaSvc.listHistorySpta(username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/editSpta", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> editSpta(@RequestBody SptaEditDto dto,
			@RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = sptaSvc.editSpta(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}else{
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/cancelSpta" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> cancelSpta(
			@RequestBody CancelSptaDto dto) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = this.sptaSvc.cancelSpta(dto);
			if (map.get("status").equals(1) || map.get("status").equals(2)
					|| map.get("status").equals(99)) {
				restResponse
						.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse,
						HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/getSptaDriver", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getSptaDriver(
			@RequestHeader String username, @RequestHeader String status) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = sptaSvc.listSptaDriver(username, status);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
	
	@RequestMapping(value = "/editSptaDriverTruk", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> editSptaDriverTruk(@RequestBody EditSptaDriverTrukDto dto,
			@RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = sptaSvc.editSptaDriverTruk(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

}
