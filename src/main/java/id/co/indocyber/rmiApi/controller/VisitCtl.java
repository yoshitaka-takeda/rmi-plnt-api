package id.co.indocyber.rmiApi.controller;

import java.util.Map;

import id.co.indocyber.rmiApi.common.RestResponse;
import id.co.indocyber.rmiApi.dto.AllVisitDto;
import id.co.indocyber.rmiApi.dto.AllVisitListDto;
import id.co.indocyber.rmiApi.dto.InputVisitAllDto;
import id.co.indocyber.rmiApi.dto.MobScheduleVisitToFarmerHeaderDto;
import id.co.indocyber.rmiApi.dto.MobStandDto;
import id.co.indocyber.rmiApi.dto.NewInputVisitAllDto;
import id.co.indocyber.rmiApi.dto.VisitAppraisementDto;
import id.co.indocyber.rmiApi.dto.VisitOtherActivityDto;
import id.co.indocyber.rmiApi.dto.VisitScoringDto;
import id.co.indocyber.rmiApi.service.VisitSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class VisitCtl {
	@Autowired
	VisitSvc	visitSvc;

	@RequestMapping(value = "/listVisit", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> listVisit(@RequestHeader int personId) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.listVisit(personId);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/listFarmByFarmer", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> listFarmByFarmer(@RequestHeader int farmerId) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.listFarmByFarmer(farmerId);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/visitOtherActivity", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> visitOtherActivity(@RequestBody VisitOtherActivityDto dto, @RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.visitOtherActivity(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	// @RequestMapping(value = "/visitScoring", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> visitScoring(@RequestBody VisitScoringDto dto, @RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.visitScoring(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	// @RequestMapping(value = "/visitAppraisement", method =
	// RequestMethod.POST)
	public ResponseEntity<RestResponse> visitAppraisement(@RequestBody VisitAppraisementDto dto, @RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.visitAppraisement(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/inputStand", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> inputStand(@RequestBody MobStandDto dto, @RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.inputStand(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	// @RequestMapping(value = "/inputVisitHeader", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> inputVisitHeader(@RequestBody MobScheduleVisitToFarmerHeaderDto dto, @RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.inputVisitHeader(dto, username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/inputVisit", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> inputVisitAll(@RequestBody AllVisitListDto dto, @RequestHeader String username, @RequestHeader int appraisement, @RequestHeader int scoring) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.visitAll(dto, username, appraisement, scoring);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/listOfFarm", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> listOfFarm(@RequestHeader int farmerId, @RequestHeader String farmCode) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.listOfFarm(farmerId, farmCode);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/listVisitDetail", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> listVisitDetail(@RequestHeader String visitNo, @RequestHeader int farmerId, @RequestHeader int personId) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.listVisitDetail(visitNo, farmerId, personId);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/visitHistory", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> visitHistory(@RequestHeader String search, @RequestHeader String dateFrom, @RequestHeader String dateTo, @RequestHeader int personId) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.visitHistory(search, dateFrom, dateTo, personId);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/listFarmerByEmployee" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> listFarmer(@RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.visitSvc.listFarmer(username);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/listVisitOtherActivity" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> listVisitOtherActivity(@RequestHeader String visitNo) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.visitSvc.ListVisitOtherActivity(visitNo);
			if (map.get("status").equals(1) || map.get("status").equals(2) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/listHistoryStand" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> listVisit(@RequestHeader String visitNo) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = visitSvc.listHistoryStand(visitNo);
			if (map.get("status").equals(1) || map.get("status").equals(2) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/listStand" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> listStand(@RequestHeader String visitNo, @RequestHeader Integer farmerId, @RequestHeader String farmCode) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.visitSvc.listStand(visitNo, farmerId, farmCode);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/listVisitHistoryHeader" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> listVisitHistoryHeader(@RequestHeader String visitNo, @RequestHeader String paddyFieldCode) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = visitSvc.listVisitHistoryHeader(visitNo, paddyFieldCode);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/inputVisitAll" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> inputVisitAll(@RequestBody InputVisitAllDto inputVisitAll, @RequestHeader String username, @RequestHeader int otherActivity, @RequestHeader int stand, @RequestHeader int visit, @RequestHeader int appraisement, @RequestHeader int scoring) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = visitSvc.inputVisitAll(inputVisitAll, username, otherActivity, stand, visit, appraisement, scoring);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/listStandNewVisit" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> listStandNewVisit(@RequestHeader Integer farmerId, @RequestHeader String farmCode) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.visitSvc.listStandNewVisit(farmerId, farmCode);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/newInputVisitAll" }, method = { RequestMethod.POST })
	public ResponseEntity<RestResponse> newInputVisitAll(@RequestBody NewInputVisitAllDto inputVisitAll, @RequestHeader String username, @RequestHeader int otherActivity, @RequestHeader int stand, @RequestHeader int visit, @RequestHeader int appraisement, @RequestHeader int scoring) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = visitSvc.newInputVisitAll(inputVisitAll, username, otherActivity, stand, visit, appraisement, scoring);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = { "/getListDelegasi" }, method = { RequestMethod.GET })
	public ResponseEntity<RestResponse> getListDelegasi(@RequestHeader String username) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<RestResponse>(null);
		try {
			Map<String, Object> map = this.visitSvc.getListDelegasi(username);
			if (map.get("status").equals(1) || map.get("status").equals(2) || map.get("status").equals(99)) {
				restResponse.setStatus(((Integer) map.get("status")).intValue());
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/assign", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> ambilTugas(@RequestHeader String username, @RequestHeader String visitNo) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.terimaTugas(username, visitNo);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}

	@RequestMapping(value = "/assignTo", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> delegasikan(@RequestHeader String username, @RequestHeader String visitNo, @RequestHeader Integer onFarmId) {
		RestResponse restResponse = new RestResponse();
		ResponseEntity<RestResponse> entity = new ResponseEntity<>(null);
		try {
			Map<String, Object> map = visitSvc.delegasikan(username, visitNo, onFarmId);
			if (map.get("status").equals(1) || map.get("status").equals(99)) {
				restResponse.setStatus((int) map.get("status"));
				restResponse.setMessage((String) map.get("message"));
				restResponse.setDataObject(map.get("Object"));
				entity = new ResponseEntity<>(restResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			restResponse.setStatus(0);
			restResponse.setMessage(e.getMessage());
			entity = new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return entity;
	}
}
