package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobFarm;
import id.co.indocyber.rmiApi.entity.MobFarmPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MFarmDao extends JpaRepository<MobFarm, MobFarmPK>{
	
	@Query("select b from MobFarm b where b.farmerId = :farmerId")
	public List<MobFarm> findToken(@Param("farmerId") String farmerId);
}
