package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MUser;
import id.co.indocyber.rmiApi.entity.MUserPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MUserDao extends JpaRepository<MUser, MUserPK> {

	@Query("select a from MUser a")
	public List<MUser> findUser();

	@Query(nativeQuery = true, value = "select count(a.username) from m_user a where a.is_active = 1 and a.phone_num=:email")
	public Long checkDuplicateUser(@Param("email") String email);
	
	@Query(nativeQuery = true, value = "select a.* from m_user a where a.is_active = 1 and a.phone_num=:email")
	public MUser checkUser(@Param("email") String email);

	@Query(nativeQuery = true, value = "select a.is_active from m_user a where a.phone_num = :email")
	Boolean checkActiveUser(@Param("email") String paramString);
	
	@Query(nativeQuery = true, value = "select a.* from m_user a where a.is_active = 1 and a.phone_num=:email and a.password = :password")
	public MUser checkPassword(@Param("email") String email,
			@Param("password") String password);

	@Query(nativeQuery = true, value = "select a.* from m_user a where a.is_active = 1 and a.phone_num=:email and a.IMEI =:imei")
	public MUser checkImei(@Param("email") String email,
			@Param("imei") String imei);

	@Query("select a from MUser a where a.username = :username")
	public MUser findUserByUsername(@Param("username") String username);
	
	@Query(nativeQuery = true, value = "select a.person_id from m_user a where a.is_active = 1 and a.username = :username")
	public Integer getIdUser(@Param("username") String username);
	
	// ==================
	
	@Query(nativeQuery = true, value = "select a.* from m_user a where a.IMEI =:imei")
	MUser checkDuplicateImei(@Param("imei") String paramString);

	@Query(nativeQuery = true, value = "select c.id, c.role_name, a.username, a.email, a.fullname, a.person_id, IFNULL(a.photo_text,''), a.token, a.phone_num "
			+ "from m_user as a "
			+ "join m_user_role as b on a.email = b.email "
			+ "join m_role as c on b.role_id = c.id "
			+ "where a.is_active = 1 and b.is_active = 1 and c.is_active = 1 and "
			+ "a.phone_num = :email and a.password = :password")
	public List<Object[]> getRole(@Param("email") String email,
			@Param("password") String password);

	@Query(nativeQuery = true, value = "select c.id "
			+ "from m_user as a "
			+ "join m_user_role as b on a.email = b.email "
			+ "join m_role as c on b.role_id = c.id "
			+ "where a.is_active = 1 and b.is_active = 1 and c.is_active = 1 and "
			+ "a.phone_num = :email")
	public int cekRole(@Param("email") String email);
	
	@Query(nativeQuery = true, value = "select d.reference_code "
			+ "from m_user as a "
			+ "join m_user_role as b on a.email = b.email "
			+ "join m_role as c on b.role_id = c.id "
			+ "JOIN mob_farmer AS d ON a.person_id = d.id "
			+ "where a.is_active = 1 and b.is_active = 1 and c.is_active = 1 and "
			+ "a.phone_num = :email and a.password = :password")
	public String getreferenceCodeFarmer(@Param("email") String email,
			@Param("password") String password);

	// ==================

	@Query(nativeQuery = true, value = "select b.fullname, IFNULL(b.address,''),IFNULL(b.rtrw,''),IFNULL(b.village,''),IFNULL(b.sub_district,''),IFNULL(b.district,''),IFNULL(b.city,''), "
			+ "IFNULL(b.province,''),IFNULL(b.zipcode,''), IFNULL(b.gender,''), b.phone_num, b.id, IFNULL(b.address_type,''), IFNULL(b.marital_status,''), IFNULL(b.religion,''), "
			+ "IFNULL(b.last_education,''), b.birthday, IFNULL(b.no_ktp,''), IFNULL(a.photo_text,''), a.username, IFNULL(b.reference_code,0) "
			+ "from m_user as a join "
			+ "mob_driver as b on a.person_id = b.id "
			+ "where a.is_active = 1 and b.is_active = 1 and a.phone_num = :email")
	public List<Object[]> getDriver(@Param("email") String email);

	@Query(nativeQuery = true, value = "select b.fullname, IFNULL(b.address,''),IFNULL(b.rtrw,''),IFNULL(b.village,''),IFNULL(b.sub_district,''),IFNULL(b.district,''),IFNULL(b.city,''), "
			+ "IFNULL(b.province,''),IFNULL(b.zipcode,''), IFNULL(b.gender,''), b.phone_num, b.id, IFNULL(b.address_type,''), IFNULL(b.status,''), IFNULL(b.religion,''), "
			+ "IFNULL(b.last_education,''), b.birthday, IFNULL(b.no_ktp,''), IFNULL(a.photo_text,''), a.username, IFNULL(b.reference_code,0) "
			+ "from m_user as a join "
			+ "mob_farmer as b on a.person_id = b.id "
			+ "where a.is_active = 1 and b.is_active = 1 and a.phone_num = :email")
	public List<Object[]> getFarmer(@Param("email") String email);

	@Query(nativeQuery = true, value = "select b.fullname, IFNULL(b.address,''),IFNULL(b.rtrw,''),IFNULL(b.village,''),IFNULL(b.sub_district,''),IFNULL(b.district,''),IFNULL(b.city,''), "
			+ "IFNULL(b.province,''),IFNULL(b.zipcode,''), IFNULL(b.gender,''), b.phone_num, b.id, IFNULL(b.address_type,''), IFNULL(b.marital_status,''), IFNULL(b.religion,''), "
			+ "IFNULL(b.last_education,''), b.birthday, IFNULL(b.no_ktp,''), IFNULL(a.photo_text,''), a.username, IFNULL(b.reference_code,0) "
			+ "from m_user as a join "
			+ "mob_employee as b on a.person_id = b.id "
			+ "where a.is_active = 1 and b.is_active = 1 and a.phone_num = :email")
	public List<Object[]> getEmployee(@Param("email") String email);
}
