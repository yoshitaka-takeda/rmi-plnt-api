package id.co.indocyber.rmiApi.dao;

import java.util.List;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitAppraisement;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitAppraisementPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface VisitAppraisementDao extends JpaRepository<MobScheduleVisitAppraisement, MobScheduleVisitAppraisementPK> {
	@Query(nativeQuery = true, value = "select a.schedule_visit_detail_id, a.param_code, a.param_value, a.criteria, a.weight, a.score, a.notes "
			+ "from mob_schedule_visit_scoring a join "
			+ "mob_schedule_visit_to_farmer_detail as b on a.schedule_visit_detail_id = b.id "
			+ "where b.visit_no = :visitNo and b.paddy_field_code = :paddyFieldCode ")
	public List<Object[]> getScoring(@Param("visitNo") String visitNo, @Param("paddyFieldCode") String paddyFieldCode);
	
	@Query(nativeQuery = true, value = "select c.period_name, c.current_sugarcane_height, c.cutdown_sugarcane_height, c.sugarcane_stem_weight, "
			+ "c.number_of_stem_per_leng, c.leng_factor_per_HA, c.number_of_sugarcane_per_HA, "
			+ "c.schedule_visit_detail_id, c.weight_per_sugarcane, c.prod_per_hectar_KU, "
			+ "c.amount_of_production,c.cutting_schedule, c.avg_brix_number, c.rendemen_estimation, c.ku_hablur_per_ha, c.amount_of_hablur "
			+ "from mob_schedule_visit_to_farmer_header a join "
			+ "mob_schedule_visit_to_farmer_detail b on a.visit_no = b.visit_no join "
			+ "mob_schedule_visit_appraisement c on b.id = c.schedule_visit_detail_id "
			+ "where a.visit_no = :visitNo and b.paddy_field_code = :paddyFieldCode ")
	public List<Object[]> getAppraisement(@Param("visitNo") String visitNo, @Param("paddyFieldCode") String paddyFieldCode);
}
