package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobFarm;
import id.co.indocyber.rmiApi.entity.MobFarmPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobFarmDao extends JpaRepository<MobFarm, MobFarmPK> {
	@Query(nativeQuery = true, value = "select a.farmer_id, a.farm_code, sum(b.field_area), a.village "
			+ "from mob_farm as a join "
			+ "mob_paddy_field as b on a.farm_code = b.farm_code "
			+ "where a.is_active=1 and a.farmer_id = :farmerId "
			+ "group by a.farm_code;")
	public List<Object[]> listFarmByFarmer(@Param("farmerId") int farmerId);

	@Query(nativeQuery = true, value = "SELECT b.village, a.farm_code, a.paddy_field_code "
			+ "from mob_schedule_visit_to_farmer_detail as a join "
			+ "mob_farm as b on a.farm_code = b.farm_code "
			+ "where a.is_active = 1 and b.is_active = 1 and a.visit_no = :visitNo limit 1")
	public List<Object[]> detailVisitFarm(@Param("visitNo") String visitNo);

	@Query(nativeQuery = true, value = "select b.paddy_field_code, b.field_area, a.id, b.planting_date, c.long_text "
			+ "from mob_schedule_visit_to_farmer_detail as a join "
			+ "mob_paddy_field as b on a.paddy_field_code = b.paddy_field_code join "
			+ "m_planting_period as c on b.planting_period = c.code "
			+ "where a.is_active = 1 and a.visit_no = :visitNo ORDER BY b.paddy_field_code ASC")
	public List<Object[]> listPaddyField(
			@Param("visitNo") String visitNo);
	
	@Query(nativeQuery = true, value = "select a.paddy_field_code, a.field_area, a.planting_date, b.long_text, a.farm_code "
			+ "from mob_paddy_field as a join "
			+ "m_planting_period as b on a.planting_period = b.code "
			+ "where a.is_active = 1 and a.farm_code = :farmeCode "
			+ "ORDER BY a.paddy_field_code asc")
	public List<Object[]> listPaddyFieldByFarm(
			@Param("farmeCode") String farmCode);

	@Query(nativeQuery = true, value = "select a.* from mob_paddy_field as a where a.is_active = 1 and a.farmer_id = :farmerId and a.farm_code = :farmCode")
	List<Object[]> listOfFarm(@Param("farmerId") int farmerId,
			@Param("farmCode") String farmCode);

}
