package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobSpta;
import id.co.indocyber.rmiApi.entity.MobSptaPK;
import id.co.indocyber.rmiApi.entity.MobWeighbridgeResult;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SptaDao extends JpaRepository<MobSpta, MobSptaPK> {

	@Query(nativeQuery=true, value="select b.spta_num, c.id, b.qty, b.`status`, b.vehicle_no, b.driver_id, b.vehicle_type, "
			+ "b.document_date, b.expired_date, IFNULL(b.rendemen,0),IFNULL(b.brix,0),IFNULL(b.pol,0), b.source, b.base_entry, b.base_line, "
			+ "c.reference_code, c.fullname, b.contract_no2, b.contract_no1	"
			+ "from mob_spta as b join "
			+ "mob_farmer as c on b.farmer_code = c.reference_code join "
			+ "m_user as a on c.id = a.person_id "
			+ "where a.is_active = 1 and b.is_active = 1 and a.username = :username and b.status = :status and "
			+ "(DATE_FORMAT(b.expired_date, '%Y%m%d') >= DATE_FORMAT(DATE(NOW()), '%Y%m%d')) "
			+ "and (DATE_FORMAT(b.document_date, '%Y%m%d') <= DATE_FORMAT(DATE(NOW()), '%Y%m%d')) "
			+ "and (b.vehicle_no IS NULL or b.vehicle_no='') and (b.driver_id IS NULL or b.driver_id = '') "
			+ "and (b.vehicle_type IS NULL or b.vehicle_type = '')")
	public List<Object[]> listSpta(@Param("username") String username,@Param("status") String status);
	
	@Query(nativeQuery=true, value="select b.spta_num, c.id, b.qty, b.`status`, b.vehicle_no, b.driver_id, b.vehicle_type, "
			+ "b.document_date, b.expired_date, IFNULL(b.rendemen,0),IFNULL(b.brix,0),IFNULL(b.pol,0), b.source, b.base_entry, b.base_line, "
			+ "c.reference_code, c.fullname, IFNULL(b.loading_sugarcane_latitude, 0), IFNULL(b.loading_sugarcane_longitude, 0), "
			+ "IFNULL(b.destination_latitude, 0), IFNULL(b.destination_longitude, 0), IFNULL(b.exit_latitude, 0), IFNULL(b.exit_longitude, 0), "
			+ "d.phone_num, b.contract_no2, b.contract_no1, b.monitoring_pos "
			+ "from mob_spta as b join "
			+ "mob_farmer as c on b.farmer_code = c.reference_code join "
			+ "m_user as a on c.id = a.person_id join "
			+ "mob_driver as d on b.driver_id = d.id join "
			+ "m_vehicle_type as e on b.vehicle_type = e.id "
			+ "where a.is_active = 1 and b.is_active = 1 and a.username = :username "
			+ "ORDER BY b.document_date DESC LIMIT 20")
	public List<Object[]> listHistorySpta(@Param("username") String username);
	
	@Query("select a from MobSpta a where a.sptaNum = :sptaNum")
	public MobSpta findSptaById(@Param("sptaNum") int sptaNum);
	
	@Query("select a from MobWeighbridgeResult a where a.documentNum = :sptaNum")
	public MobWeighbridgeResult findResultById(@Param("sptaNum") int sptaNum);
	
//	@Query(nativeQuery=true, value="select a.document_date,a.expired_date, a.spta_num, "
//			+ "a.farmer_type,e.id,e.fullname as namaPetani, e.sub_district,e.village, "
//			+ "b.fullname,b.phone_num,a.vehicle_no,g.vehicle_type_name, a.loading_sugarcane_by, a.destination_arrived_by, a.exit_by, "
//			+ "a.contract_no1, a.contract_no2, a.farmer_code, a.coop_name, a.monitoring_pos, IFNULL(a.brix,0) "
//			+ "from mob_spta a "
//			+ "join mob_driver b "
//			+ "on a.driver_id=b.id "
//			+ "join m_user c "
//			+ "on c.person_id = b.id "
//			+ "join m_user_role d "
//			+ "on c.email=d.email "
//			+ "join mob_farmer e "
//			+ "on a.farmer_code=e.reference_code "
//			+ "join m_farmer_type f "
//			+ "on e.farmer_type=f.code "
//			+ "join m_vehicle_type g "
//			+ "on a.vehicle_type=g.id "
//			+ "where d.role_id=3 and a.`status`=:status and "
//			+ "(DATE_FORMAT(a.expired_date, '%Y%m%d') >= DATE_FORMAT(DATE(NOW()), '%Y%m%d')) "
//			+ "and (DATE_FORMAT(a.document_date, '%Y%m%d') <= DATE_FORMAT(DATE(NOW()), '%Y%m%d')) "
//			+ "and c.username=:username "
//			+ "and ((a.loading_sugarcane_by IS NULL or a.loading_sugarcane_by='') "
//			+ "or (a.destination_arrived_by IS NULL or a.destination_arrived_by='') "
//			+ "or (a.exit_by IS NULL or a.exit_by=''))")
//	public List<Object[]> listSptaDriver(@Param("username") String username,@Param("status") String status);
	
	@Query(nativeQuery=true, value="select a.document_date,a.expired_date, a.spta_num, "
			+ "a.farmer_type,e.id,e.fullname as namaPetani, e.sub_district,e.village, "
			+ "b.fullname,b.phone_num,a.vehicle_no,g.vehicle_type_name, a.loading_sugarcane_by, a.destination_arrived_by, a.exit_by, "
			+ "a.contract_no1, a.contract_no2, a.farmer_code, a.coop_name, a.monitoring_pos, IFNULL(a.brix,0) "
			+ "from mob_spta a "
			+ "join mob_driver b "
			+ "on a.driver_id=b.id "
			+ "join m_user c "
			+ "on c.person_id = b.id "
			+ "join m_user_role d "
			+ "on c.email=d.email "
			+ "join mob_farmer e "
			+ "on a.farmer_code=e.reference_code "
			+ "join m_farmer_type f "
			+ "on e.farmer_type=f.code "
			+ "join m_vehicle_type g "
			+ "on a.vehicle_type=g.id "
			+ "where d.role_id=3 and a.`status`=:status and "
			+ "(DATE_FORMAT(a.expired_date, '%Y%m%d') >= DATE_FORMAT(DATE(NOW()), '%Y%m%d')) "
			+ "and (DATE_FORMAT(a.document_date, '%Y%m%d') <= DATE_FORMAT(DATE(NOW()), '%Y%m%d')) "
			+ "and c.username=:username "
			+ "and ((a.loading_sugarcane_by IS NULL or a.loading_sugarcane_by='') "
			+ "or (a.destination_arrived_by IS NULL or a.destination_arrived_by='') "
			+ "or (a.exit_by IS NULL or a.exit_by='')) "
			+ "Union "
			+ "select a.document_date,a.expired_date, a.spta_num, "
			+ "a.farmer_type,e.id,e.fullname as namaPetani, e.sub_district,e.village, "
			+ "b.fullname,b.phone_num,a.vehicle_no,g.vehicle_type_name, a.loading_sugarcane_by, "
			+ "a.destination_arrived_by, a.exit_by, "
			+ "a.contract_no1, a.contract_no2, a.farmer_code, a.coop_name, a.monitoring_pos, IFNULL(a.brix,0) "
			+ "from mob_spta a "
			+ "join mob_driver b "
			+ "on a.driver_id=b.id "
			+ "join m_user c "
			+ "on c.person_id = b.id "
			+ "join m_user_role d "
			+ "on c.email=d.email "
			+ "join mob_farmer e "
			+ "on a.farmer_code=e.reference_code "
			+ "join m_farmer_type f "
			+ "on e.farmer_type=f.code "
			+ "join m_vehicle_type g "
			+ "on a.vehicle_type=g.id "
			+ "where d.role_id=3 "
			+ "and c.username=:username "
			+ "and a.loading_sugarcane_by is Not NULL or a.loading_sugarcane_by='' "
			+ "and (a.exit_by IS NULL or a.exit_by ='')")
	public List<Object[]> listSptaDriver(@Param("username") String username,@Param("status") String status);

}
