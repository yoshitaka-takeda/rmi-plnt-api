package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MVehicleType;
import id.co.indocyber.rmiApi.entity.MVehicleTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MVehicleTypeDao extends JpaRepository<MVehicleType, MVehicleTypePK> {

}
