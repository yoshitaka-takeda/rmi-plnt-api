package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MPlantingPeriod;
import id.co.indocyber.rmiApi.entity.MPlantingPeriodPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MPlantingPeriodDao extends JpaRepository<MPlantingPeriod, MPlantingPeriodPK>{
	 @Query("select a from MPlantingPeriod a where a.isActive = 1")
	 List<MPlantingPeriod> findAllMaster();
}
