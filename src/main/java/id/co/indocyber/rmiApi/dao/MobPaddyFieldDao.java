package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.dto.MobScheduleVisitToFarmerDetailDto;
import id.co.indocyber.rmiApi.entity.MUser;
import id.co.indocyber.rmiApi.entity.MobPaddyField;
import id.co.indocyber.rmiApi.entity.MobPaddyFieldPK;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobPaddyFieldDao extends JpaRepository<MobPaddyField, MobPaddyFieldPK> {
	@Query(nativeQuery = true, value = "select a.farm_code, a.village "
			+ "from mob_farm as a "
			+ "where a.is_active=1 and a.farmer_id= :farmerID")
	public MUser checkUser(@Param("farmerID") int farmerID);
	
	@Query(nativeQuery = true, value = "select a.id, a.farmer_id, a.farm_code, a.paddy_field_code, a.line_num, "
			+ "a.field_area, a.latitude, a.longitude, a.weight_estimation, a.sugarcane_type, a.age_of_sugarcane, "
			+ "a.planting_date, a.sugarcane_species, a.sugarcane_score, a.sugarcane_qty, a.planting_period, "
			+ "a.weigh_estimation, a.`status`, a.reference_code, a.is_active, a.processed_by, a.processed_date "
			+ "from mob_paddy_field a where a.farm_code = :farmCode")
	public List<Object[]> listPaddyField(@Param("farmCode") String farmCode);
	
	@Query("select a from MobScheduleVisitToFarmerDetail a where a.farmCode = :farmCode")
	public List<MobScheduleVisitToFarmerDetail> listPaddyFieldNative(@Param("farmCode") String paddyFieldCode);
}
