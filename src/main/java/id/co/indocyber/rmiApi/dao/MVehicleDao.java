package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MobVehicle;
import id.co.indocyber.rmiApi.entity.MobVehiclePK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MVehicleDao extends JpaRepository<MobVehicle, MobVehiclePK> {
	@Query("select a from MobVehicle a where a.vehicleNo=:id")
	public MobVehicle findOneBy(@Param("id") String id);
}
