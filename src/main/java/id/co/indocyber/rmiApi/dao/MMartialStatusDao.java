package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MMaritalStatus;
import id.co.indocyber.rmiApi.entity.MMaritalStatusPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MMartialStatusDao extends JpaRepository<MMaritalStatus, MMaritalStatusPK> {

}
