package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MFarmerType;
import id.co.indocyber.rmiApi.entity.MFarmerTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MFarmerTypeDao extends JpaRepository<MFarmerType, MFarmerTypePK> {

}
