package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobWeighbridgeResult;
import id.co.indocyber.rmiApi.entity.MobWeighbridgeResultPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobWeighbridgeResultDao extends JpaRepository<MobWeighbridgeResult, Integer> {
	
	@Query(nativeQuery = true, value = "SELECT a.transportir_code, a.transportir_name, b.status, a.document_num, b.contract_no1,c.fullname, a.vehicle_no, "
			+ "f.fullname as driver, b.monitoring_pos, a.date_load1, a.date_load2, a.qty_weigh_gross, "
			+ "sum(a.qty_weigh_gross-a.qty_weigh_netto) as brtKndrn, a.qty_weigh_netto, a.rafaksi, a.MBS_perc, a.total_paid, b.contract_no2 "
			+ "FROM mob_weighbridge_result AS a "
			+ "join mob_spta AS b ON a.document_num = b.spta_num "
			+ "JOIN mob_farmer AS c ON b.farmer_code = c.reference_code "
			+ "JOIN m_user AS d ON d.person_id = c.id "
			+ "JOIN m_user_role AS e ON d.email = e.email "
			+ "JOIN mob_driver AS f ON b.driver_id = f.id "
			+ "WHERE d.username = :username AND e.role_id = 2 AND a.document_type = 'SPTA' AND "
			+ "(DATE_FORMAT(b.expired_date, '%Y%m%d') >= DATE_FORMAT(:dateFrom, '%Y%m%d')) "
			+ "and (DATE_FORMAT(b.document_date, '%Y%m%d') <= DATE_FORMAT(:dateTo, '%Y%m%d')) ")
	public List<Object[]> listWeighbridgeResult(
			@Param("username") String username,
			@Param("dateFrom") String dateFrom,
			@Param("dateTo") String dateTo);
	
	@Query(nativeQuery = true, value = "SELECT a.document_num, a.mitra_code, a.mitra_name, a.vehicle_no, "
			+ "a.driver_name, a.farmer_name, a.contract_no_1, a.contract_no_2, a.date_in, a.time_in, a.date_out, "
			+ "a.time_out, a.`status`, a.monitoringPos, a.weigh_bruto, a.weigh_netto, a.weigh_vehicle, a.qty_weigho, a.rafaksi, a.total_paid "
			+ "FROM mob_weighbridge_result AS a "
			+ "WHERE a.farmer_code = :farmerCode AND a.document_type = 'SPTA' AND "
			+ "(DATE_FORMAT(a.date_out, '%Y%m%d') >= DATE_FORMAT(:dateFrom, '%Y%m%d')) "
			+ "and (DATE_FORMAT(a.date_in, '%Y%m%d') <= DATE_FORMAT(:dateTo, '%Y%m%d'));")
	public List<Object[]> listWeighbridgeResultNew(
			@Param("farmerCode") String farmerCode,
			@Param("dateFrom") String dateFrom,
			@Param("dateTo") String dateTo);
	
	@Query(nativeQuery = true, value = "SELECT a.transportir_code, a.transportir_name, b.status, a.document_num, b.contract_no1, f.fullname as driver, a.vehicle_no, "
			+ "c.fullname, b.monitoring_pos, a.date_load1, a.date_load2, a.qty_weigh_gross, "
			+ "(a.qty_weigh_gross-a.qty_weigh_netto) as brtKndrn, a.qty_weigh_netto, a.rafaksi, a.MBS_perc, b.contract_no2 "
			+ "FROM mob_weighbridge_result AS a JOIN "
			+ "mob_spta AS b ON a.document_num = b.spta_num JOIN "
			+ "mob_driver AS c ON b.driver_id = c.id JOIN "
			+ "m_user AS d ON c.id = d.person_id JOIN "
			+ "m_user_role AS e ON d.email = e.email JOIN "
			+ "mob_farmer AS f ON b.farmer_code = f.reference_code "
			+ "where d.username=:username and e.role_id = 3 and a.document_type = 'SPTA' and "
			+ "(DATE_FORMAT(b.expired_date, '%Y%m%d') >= DATE_FORMAT(:dateFrom, '%Y%m%d')) "
			+ "and (DATE_FORMAT(b.document_date, '%Y%m%d') <= DATE_FORMAT(:dateTo, '%Y%m%d'))")
	public List<Object[]> listWeighbridgeResultDriver(
			@Param("username") String paramString1,
			@Param("dateFrom") String paramString2,
			@Param("dateTo") String paramString3);
	

	@Query(nativeQuery = true, value = "SELECT a.document_num, a.mitra_code, a.mitra_name, a.vehicle_no, "
			+ "a.driver_name, a.farmer_name, a.contract_no_1, a.contract_no_2, a.date_in, a.time_in, a.date_out, "
			+ "a.time_out, a.`status`, a.monitoringPos, a.weigh_bruto, a.weigh_netto, a.weigh_vehicle, a.qty_weigho, a.rafaksi "
			+ "FROM mob_weighbridge_result AS a "
			+ "WHERE a.driver_id = :driverId AND a.document_type = 'SPTA' AND "
			+ "(DATE_FORMAT(a.date_out, '%Y%m%d') >= DATE_FORMAT(:dateFrom, '%Y%m%d')) "
			+ "and (DATE_FORMAT(a.date_in, '%Y%m%d') <= DATE_FORMAT(:dateTo, '%Y%m%d'));")
	public List<Object[]> listWeighbridgeResultDriverNew(
			@Param("driverId") String driverId,
			@Param("dateFrom") String dateFrom,
			@Param("dateTo") String dateTo);
}
