package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MobScheduleVisitScoring;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitScoringPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitScoringDao extends JpaRepository<MobScheduleVisitScoring, MobScheduleVisitScoringPK> {

}
