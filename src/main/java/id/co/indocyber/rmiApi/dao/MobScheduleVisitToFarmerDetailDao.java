package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitToFarmerDetailDao extends JpaRepository<MobScheduleVisitToFarmerDetail, Integer>{
	@Query("SELECT a from MobScheduleVisitToFarmerDetail a where a.visitNo = :visitNo ")
	public MobScheduleVisitToFarmerDetail getOneByVisitNo(@Param("visitNo") String visitNo);
	
	@Query("SELECT a from MobScheduleVisitToFarmerDetail a where a.visitNo = :visitNo and a.paddyFieldCode = :paddyFieldCode ")
	public MobScheduleVisitToFarmerDetail getOneByVisitNoDetail(@Param("visitNo") String visitNo, @Param("paddyFieldCode") String paddyFieldCode);
	
	@Query("SELECT a from MobScheduleVisitToFarmerDetail a where a.visitNo = :visitNo ")
	public List<MobScheduleVisitToFarmerDetail> getByVisitNo(@Param("visitNo") String visitNo);
}
