package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MFarmFieldsType;
import id.co.indocyber.rmiApi.entity.MFarmFieldsTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MFarmTypeDao extends JpaRepository<MFarmFieldsType, MFarmFieldsTypePK> {

}
