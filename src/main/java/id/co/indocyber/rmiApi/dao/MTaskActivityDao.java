package id.co.indocyber.rmiApi.dao;


import id.co.indocyber.rmiApi.entity.MTaskActivity;
import id.co.indocyber.rmiApi.entity.MTaskActivityPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MTaskActivityDao extends JpaRepository<MTaskActivity, MTaskActivityPK> {
	
	@Query("select a from MTaskActivity a where a.code = :id")
	public MTaskActivity findOneBy(@Param("id") String id);

}
