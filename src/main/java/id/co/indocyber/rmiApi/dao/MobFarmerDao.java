package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobFarmer;
import id.co.indocyber.rmiApi.entity.MobFarmerPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobFarmerDao extends JpaRepository<MobFarmer, MobFarmerPK> {
	@Query("select a from MobFarmer a where a.id=:id")
	public MobFarmer findOneBy(@Param("id") int id);

	@Query(nativeQuery = true, value ="select a.id, a.fullname, a.village, a.reference_code, f.photo_text "
			+ "from mob_farmer as a join "
			+ "mob_mapping_farmer_employee as b on a.id = b.farmer_id join "
			+ "mob_employee as c on b.employee_id = c.id join "
			+ "m_user as d on c.id = d.person_id left join "
			+ "m_user as f on a.id = f.person_id "
			+ "where d.username = :username")
	public List<Object[]> getFarmerByEmployee(@Param("username") String username);
	
	@Query(nativeQuery = true, value ="select a.id, a.fullname, a.village, a.reference_code, "
			+ "(SELECT b.photo_text FROM m_user AS b WHERE b.person_id = a.id LIMIT 1) "
			+ "from mob_farmer as a "
			+ "WHERE a.id IN (:id)")
	public List<Object[]> getFarmerByEmployeeByRiki(@Param("id")List<Integer> id);
	
	@Query("select a from MobFarmer a where (a.referenceCode = :farmerCode or a.id = :farmerCode)")
	public MobFarmer findOneByFarmerCode(@Param("farmerCode") String farmerCode);
	
	@Query(nativeQuery = true, value ="SELECT a.reference_code "
			+ "FROM mob_farmer AS a "
			+ "JOIN m_user AS b ON a.id = b.person_id "
			+ "JOIN m_user_role AS c ON b.email = c.email "
			+ "WHERE c.role_id = 2 and b.username = :username")
	public String findRefCodeByUsername(@Param("username") String username);
	
	@Query(nativeQuery = true, value ="SELECT a.id "
			+ "FROM mob_driver AS a "
			+ "JOIN m_user AS b ON a.id = b.person_id "
			+ "JOIN m_user_role AS c ON b.email = c.email "
			+ "WHERE c.role_id = 3 and b.username = :username")
	public String findIdCodeByUsername(@Param("username") String username);
}
