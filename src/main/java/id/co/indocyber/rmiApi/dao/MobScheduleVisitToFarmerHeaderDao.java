package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHeader;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHeaderPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitToFarmerHeaderDao
		extends
		JpaRepository<MobScheduleVisitToFarmerHeader, MobScheduleVisitToFarmerHeaderPK> {
	@Query(nativeQuery = true, value = "select a.* from mob_schedule_visit_to_farmer_header a where a.visit_no=:visitNo")
	MobScheduleVisitToFarmerHeader findOneBy(@Param("visitNo") String paramString);
	
	@Query(nativeQuery = true, value = "select a.estimate_harvest_time, a.evaluation, a.plan_text "
			+ "from mob_schedule_visit_to_farmer_detail a "
			+ "where a.visit_no = :visitNo and a.paddy_field_code = :paddyFieldCode ")
	public List<Object[]> getScheduleVisitToFarmerHeader(@Param("visitNo") String visitNo,@Param("paddyFieldCode") String paddyFieldCode);
	

	@Query(nativeQuery = true, value = "select a.visit_no "
			+ "from mob_schedule_visit_to_farmer_header a "
			+ "where a.visit_no like :visitNo "
			+ "ORDER by a.visit_no desc "
			+ "limit 1")
	public String getLastVisitNoHeader(@Param("visitNo") String visitNo);
}
