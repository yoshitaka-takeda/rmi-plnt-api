package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MSugarcaneFarmType;
import id.co.indocyber.rmiApi.entity.MSugarcaneFarmTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MSugarCaneFarmTypeDao extends JpaRepository<MSugarcaneFarmType, MSugarcaneFarmTypePK>{

}
