package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MobScheduleVisitOtherActivity;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitOtherActivityPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VisitOtherActivityDao
		extends
		JpaRepository<MobScheduleVisitOtherActivity, MobScheduleVisitOtherActivityPK> {
	  @Query(nativeQuery = true, value = "select a.* from mob_schedule_visit_other_activities a where a.is_active = 1 and a.visit_no = :visitNo")
	  public MobScheduleVisitOtherActivity findActive(@Param("visitNo") String paramString);
}
