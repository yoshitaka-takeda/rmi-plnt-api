package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MAddressType;
import id.co.indocyber.rmiApi.entity.MAddressTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MAddressTypeDao extends JpaRepository<MAddressType, MAddressTypePK> {

}
