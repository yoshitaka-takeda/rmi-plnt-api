package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MReason;
import id.co.indocyber.rmiApi.entity.MReasonPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MReasonDao extends JpaRepository<MReason, MReasonPK> {
	@Query("select a from MReason a where a.isActive = 1")
	List<MReason> findAllMaster();

}
