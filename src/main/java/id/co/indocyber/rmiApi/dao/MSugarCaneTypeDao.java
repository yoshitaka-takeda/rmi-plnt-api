package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MSugarcaneType;
import id.co.indocyber.rmiApi.entity.MSugarcaneTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MSugarCaneTypeDao extends JpaRepository<MSugarcaneType, MSugarcaneTypePK> {

}
