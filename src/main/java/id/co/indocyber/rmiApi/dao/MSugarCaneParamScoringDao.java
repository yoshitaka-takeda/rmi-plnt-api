package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MSugarcaneParamScoring;
import id.co.indocyber.rmiApi.entity.MSugarcaneParamScoringPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MSugarCaneParamScoringDao extends JpaRepository<MSugarcaneParamScoring, MSugarcaneParamScoringPK> {

}
