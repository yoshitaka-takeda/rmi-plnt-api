package id.co.indocyber.rmiApi.dao;

import java.math.BigDecimal;
import java.util.List;

import id.co.indocyber.rmiApi.entity.MobSptaPK;
import id.co.indocyber.rmiApi.entity.MobStand;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobStandDao extends JpaRepository<MobStand, MobSptaPK> {
	@Query("SELECT a from MobStand a where a.visitNo = :visitNo ")
	public MobStand historyStand(@Param("visitNo") String visitNo);
	
	@Query(nativeQuery = true, value = "select Coalesce(sum(b.field_area),0) "
			+ "from mob_farm as a join "
			+ "mob_paddy_field as b on a.farm_code = b.farm_code "
			+ "where a.farmer_id = :farmerId and a.farm_code = "
			+ "( "
			+ "select a.farm_code "
			+ "from mob_schedule_visit_to_farmer_detail as a "
			+ "where a.visit_no = :visitNo "
			+ "GROUP by a.farm_code "
			+ ") "
			+ "GROUP by b.farm_code;")
	public BigDecimal getFieldArea(@Param("farmerId") Integer farmerId, @Param("visitNo") String visitNo);
	
	@Query(nativeQuery = true, value = "select Coalesce(sum(b.field_area),0) "
			+ "	from mob_farm as a join "
			+ "mob_paddy_field as b on a.farm_code = b.farm_code "
			+ "where a.farmer_id = :farmerId and a.farm_code = :farmCode "
			+ "GROUP by b.farm_code")
	public BigDecimal getNewFieldArea(@Param("farmerId") Integer farmerId, @Param("farmCode") String farmCode);
	
	@Query(nativeQuery= true, value="select Coalesce(sum(c.number_of_sugarcane_KU),0) "
			+ "from mob_schedule_visit_to_farmer_header as a join "
			+ "mob_schedule_visit_to_farmer_detail as b on a.visit_no=b.visit_no join "
			+ "mob_schedule_visit_appraisement c on b.id=c.schedule_visit_detail_id "
			+ "where a.visit_no=:visitNo")
	public BigDecimal getTonaseTaks(@Param("visitNo") String visitNo);
	
	@Query(nativeQuery= true, value="select a.* from mob_stand a, mob_schedule_visit_to_farmer_detail b, mob_farm c "
			+ "where a.visit_no=b.visit_no and b.farm_code = c.farm_code and c.farmer_id = :farmCode "
			+ "order by a.entry_date desc")
	public MobStand getStand(@Param("farmCode") String farmCode);
	
	// new stand
	
	@Query(nativeQuery= true, value="SELECT b.stand_tonnage, b.stand_area "
			+ "FROM mob_schedule_visit_to_farmer_detail AS a JOIN "
			+ "mob_stand AS b WHERE a.farm_code = :farmCode ORDER BY b.created_date DESC LIMIT 1")
	public List<Object[]> getLastStand(@Param("farmCode") String farmCode);
	
	@Query(nativeQuery= true, value="SELECT a.paddy_field_code "
			+ "FROM mob_paddy_field AS a "
			+ "WHERE a.farm_code = :farmCode")
	public List<String> getListPaddyFieldCode(@Param("farmCode") String farmCode);
	
	@Query(nativeQuery= true, value="SELECT IFNULL(b.number_of_sugarcane_KU,0) "
			+ "FROM mob_schedule_visit_to_farmer_detail AS a JOIN "
			+ "mob_schedule_visit_appraisement AS b ON a.id = b.schedule_visit_detail_id "
			+ "WHERE a.paddy_field_code = :paddyFieldCode ORDER BY b.created_date DESC LIMIT 1")
	public BigDecimal getTonnage(@Param("paddyFieldCode") String paddyFieldCode);
}
