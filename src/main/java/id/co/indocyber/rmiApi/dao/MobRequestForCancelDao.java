package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MobRequestForCancel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobRequestForCancelDao extends JpaRepository<MobRequestForCancel, Integer> {
	@Query("select a from MobRequestForCancel a where a.sptaNum=:sptaNum")
	public MobRequestForCancel findOneBy(@Param("sptaNum") int sptaNum);
}
