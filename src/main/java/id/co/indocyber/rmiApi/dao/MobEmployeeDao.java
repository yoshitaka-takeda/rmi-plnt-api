package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobEmployee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobEmployeeDao extends JpaRepository<MobEmployee, Integer> {
	@Query("select a from MobEmployee a where a.id=:id")
	public MobEmployee findOneBy(@Param("id") int id);
	
	@Query("select a from MobEmployee a where a.id in :id")
	public List<MobEmployee> findOneByIdList(@Param("id") List<Integer> id);
	
	@Query(nativeQuery = true, value = "select a.id from mob_employee a where a.manager in :id")
	public List<Integer> findOneByIdList2(@Param("id") List<Integer> id);
	
	@Query(nativeQuery = true, value ="select b.token "
			+ "from mob_employee a join m_user b on a.id = b.person_id "
			+ "where a.id = :employeeId") 
	public String findTokenPetugas(@Param("employeeId") Integer employeeId);
	
	@Query(nativeQuery = true, value = "select emp.* from mob_employee emp where emp.manager IN (:id)")
	List<MobEmployee> findManagedEmployees(@Param("id")List<Integer> id);
	
	@Query(nativeQuery = true, value = "select distinct map.farmer_id from mob_mapping_farmer_employee map where map.employee_id IN (:id) and map.is_active is true")
	List<Integer> findFarmer(@Param("id")List<Integer> id);
}
