package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MEducationType;
import id.co.indocyber.rmiApi.entity.MEducationTypePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MEducationDao extends JpaRepository<MEducationType, MEducationTypePK> {

}
