package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHeader;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHeaderPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VisitDao
		extends
		JpaRepository<MobScheduleVisitToFarmerHeader, MobScheduleVisitToFarmerHeaderPK> {

	@Query(nativeQuery = true, value = "select  a.visit_no, b.fullname, b.id, b.village, b.reference_code, a.visit_date, a.visit_expired_date, "
			+ "(SELECT c.photo_text from m_user AS c JOIN m_user_role AS d ON c.email = d.email WHERE d.role_id = 2 and c.person_id = b.id LIMIT 1 ) as photo_text, a.task_activity "
			+ "from mob_schedule_visit_to_farmer_header as a join "
			+ "mob_farmer as b on a.farmer_id = b.id  "
			+ "where a.assign_to = :personId and (a.task_activity = '03' or a.task_activity = '02') and "
			+ "(DATE_FORMAT(a.visit_expired_date, '%Y%m%d') >= DATE_FORMAT(:visitExpiredDate, '%Y%m%d')) "
			+ "and (DATE_FORMAT(a.visit_date, '%Y%m%d') <= DATE_FORMAT(:visitDate, '%Y%m%d')) "
			+ "ORDER by a.visit_date, a.visit_no DESC")
	public List<Object[]> getListVisit(@Param("personId") int personId, @Param("visitDate") String visitDate, @Param("visitExpiredDate") String visitExpiredDate);

	@Query(nativeQuery = true, value = "select  a.visit_no, d.fullname, a.farmer_id, d.village, "
			+ "a.visit_date,a.visit_expired_date,(select f.photo_text from m_user as f where f.person_id=d.id LIMIT 1) as photo_text, "
			+ "a.task_activity, d.reference_code, (SELECT e.farm_code FROM mob_schedule_visit_to_farmer_detail AS e WHERE e.visit_no = a.visit_no GROUP BY e.farm_code) AS farmCode "
			+ "from mob_schedule_visit_to_farmer_header as a join "
			+ "mob_farmer as d on a.farmer_id=d.id "
			+ "where a.assign_to = :personId and a.visit_no = :visitNo")
	public List<Object[]> getListVisitDetail(@Param("personId") int personId,
			@Param("visitNo") String visitNo);

	@Query(nativeQuery = true, value = "SELECT sum(a.score) "
			+ "from mob_schedule_visit_scoring as a "
			+ "where a.schedule_visit_detail_id = :id")
	public Double getScoringByDetail(@Param("id") int id);

	@Query(nativeQuery = true, value = "SELECT sum(a.score) "
			+ "from mob_schedule_visit_scoring as a "
			+ "GROUP by a.schedule_visit_detail_id "
			+ "order by sum(a.score) desc")
	public List<Double> getRankingByDetail();

	@Query(nativeQuery = true, value = "select a.visit_no, d.fullname, a.farmer_id, d.village, (select f.photo_text from m_user AS f JOIN m_user_role AS h ON f.email = h.email WHERE h.role_id NOT IN (2,3) AND f.person_id=d.id) as photo_text, "
			+ "a.task_activity "
			+ "from mob_schedule_visit_to_farmer_header as a join "
			+ "mob_schedule_visit_to_farmer_detail as b on a.visit_no = b.visit_no join "
			+ "mob_farmer as d on a.farmer_id=d.id join "
			+ "mob_employee as e on a.assign_to = e.id join "
			+ "mob_farm as g on b.farm_code = g.farm_code "
			+ "where a.task_activity = '00' and a.assign_to = :personId and (d.fullname like :search or d.village like :search or g.village like :search) and "
			+ "DATE_FORMAT(a.visit_date,'%Y%m%d') BETWEEN DATE_FORMAT(:dateFrom,'%Y%m%d') and DATE_FORMAT(:dateTo,'%Y%m%d') "
			+ "GROUP by a.visit_no ORDER by a.visit_date, a.visit_no desc")
	public List<Object[]> getListVisitHistory(@Param("personId") int personId,
			@Param("search") String search, @Param("dateFrom") String dateFrom,
			@Param("dateTo") String dateTo);
	
	@Query(nativeQuery = true, value = "select COUNT(a.id) "
			+ "from mob_schedule_visit_to_farmer_detail as a "
			+ "where a.visit_no = :visitNo ")
	public Integer getTotalFarmByDetail(@Param("visitNo") String visitNo);
	
	@Query(nativeQuery = true, value = "select a.id "
			+ "from mob_employee as a join "
			+ "m_user as b on a.id = b.person_id "
			+ "where b.username = :username")
	public Integer getListIdDelegasi(@Param("username") String username);
	
	@Query(nativeQuery = true, value = "select a.id, a.fullname, IFNULL(a.reference_code,IFNULL(a.id,0)) noId, b.photo_text, d.role_name "
			+ "from mob_employee as a join "
			+ "m_user as b on a.id = b.person_id join "
			+ "m_user_role AS c ON b.email = c.email join "
			+ "m_role AS d ON c.role_id = d.id "
			+ "where c.role_id in (1,5,6,7,8,11) and a.id in :id")
	public List<Object[]> getListDelegasi(@Param("id") List<Integer> id);
	
	@Query(nativeQuery = true, value = "select a.id "
			+ "from mob_employee as a join "
			+ "m_user as b on a.id = b.person_id "
			+ "where a.id in :id")
	public List<Object[]> getListIdMember(@Param("id") List<Integer> id);
	
}
