package id.co.indocyber.rmiApi.dao;

import java.util.List;

import id.co.indocyber.rmiApi.entity.MobDriver;
import id.co.indocyber.rmiApi.entity.MobDriverPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobDriverDao extends JpaRepository<MobDriver, MobDriverPK> {
	@Query("select a from MobDriver a where a.isActive = 1")
	public List<MobDriver> findAllMaster();
	
	@Query(nativeQuery = true, value = "select a.id, a.fullname, a.driver_type, a.phone_num "
			+ "from mob_driver as a "
			+ "where a.is_active = 1 and a.id not in (select a.id "
			+ "from mob_driver as a join "
			+ "mob_spta as b on a.id = b.driver_id "
			+ "where b.document_date = DATE(NOW()) "
			+ "GROUP BY a.id)")
	public List<Object[]> findAllMasterIsFree();

	@Query(nativeQuery = true, value = "select a.id, a.fullname, a.driver_type, a.phone_num "
			+ "from mob_driver as a "
			+ "where a.is_active = 1")
	public List<Object[]> findAllMasterIsFreeNew();
	
	@Query("select a from MobDriver a where a.id=:id")
	public MobDriver findOneBy(@Param("id") int id);

	@Query(nativeQuery = true, value = "select a.* from mob_driver a "
			+ "where IF(SUBSTRING(a.phone_num, 1, 2) = '08',CONCAT('0', SUBSTRING(a.phone_num, 2)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 3) = '628',CONCAT('0', SUBSTRING(a.phone_num, 3)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 4) = '+628',CONCAT('0', SUBSTRING(a.phone_num, 4)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 1) = '8',CONCAT('0',a.phone_num),'')))) = :id")
	public MobDriver findOneByNoHp(@Param("id") String id);

	@Query(nativeQuery = true, value = "select COUNT(a.id) from mob_driver a "
			+ "where IF(SUBSTRING(a.phone_num, 1, 2) = '08',CONCAT('0', SUBSTRING(a.phone_num, 2)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 3) = '628',CONCAT('0', SUBSTRING(a.phone_num, 3)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 4) = '+628',CONCAT('0', SUBSTRING(a.phone_num, 4)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 1) = '8',CONCAT('0',a.phone_num),'')))) =:id GROUP by a.phone_num")
	Integer countDriver(@Param("id") String paramString);

	@Query(nativeQuery = true, value = "select COUNT(a.id) "
			+ "from mob_driver as a join "
			+ "m_user as b on a.id = b.person_id join "
			+ "m_user_role as c on b.email = c.email "
			+ "where c.role_id = 3 and IF(SUBSTRING(a.phone_num, 1, 2) = '08',CONCAT('0', SUBSTRING(a.phone_num, 2)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 3) = '628',CONCAT('0', SUBSTRING(a.phone_num, 3)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 4) = '+628',CONCAT('0', SUBSTRING(a.phone_num, 4)), "
			+ "IF(SUBSTRING(a.phone_num, 1, 1) = '8',CONCAT('0',a.phone_num),'')))) = :id "
			+ "group by a.phone_num")
	Integer countDriverInUser(@Param("id") String paramString);
	
	@Query(nativeQuery = true, value = "select count(a.id) "
			+ "from mob_driver as a join "
			+ "m_user as b on a.id = b.person_id join "
			+ "m_user_role as c on b.email = c.email "
			+ "where c.role_id = 3 and a.id = :id")
	Integer countCekDriverInUser(@Param("id") int paramString);
}
