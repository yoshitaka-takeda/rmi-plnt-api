package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MGender;
import id.co.indocyber.rmiApi.entity.MGenderPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MGenderDao extends JpaRepository<MGender, MGenderPK> {

}
