package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHistory;
import id.co.indocyber.rmiApi.entity.MobScheduleVisitToFarmerHistoryPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitToFarmerHistoryDao extends JpaRepository<MobScheduleVisitToFarmerHistory, MobScheduleVisitToFarmerHistoryPK> {
	@Query(nativeQuery = true, value = "select a.* from mob_schedule_visit_to_farmer_history a where a.visit_no=:visitNo and a.farmer_id = :farmerId and a.assign_to = :assignTo")
	MobScheduleVisitToFarmerHistory findOneBy(@Param("visitNo") String visitNo, @Param("farmerId") Integer farmerId, @Param("assignTo") Integer assignTo);
}
