package id.co.indocyber.rmiApi.dao;

import id.co.indocyber.rmiApi.entity.MReligion;
import id.co.indocyber.rmiApi.entity.MReligionPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MReligionDao extends JpaRepository<MReligion, MReligionPK> {

}
