package id.co.indocyber.rmiApi.config;

public class ConstantVariabel {
	public static final String _ipServer = "http://103.66.69.20:8080";
	public static final String _passDefault = "rmi2019";
	
	public static final String _pathImg = "/opt/tomcat/webapps/rmi-assets/user/profile-";
	public static final Integer _subStr = 19;
	
	public static final String _URL = _ipServer + "/rmi-assets/rmi2-1ce51-firebase-adminsdk-xyvf6-7aeb060358.json";
	public static final String _DB = "https://rmi2-1ce51.firebaseio.com";
	
	public static final String _URLPost = "https://fcm.googleapis.com/fcm/send";
	public static final String _KEY = "AAAAATgf04Q:APA91bHQfYuMJuyEjq_U9AeSRIq1AGeqKDFbvTzl31W7Ir0EUiTiOn5yisgWcw-ZfBvJMF8nXuuGB7_w9CzusbGrvDctd4J2hIH_ItO_S4LSWpQr1h4ebNkjhv7kJR9Ou8C-5kENG9iU";
	public static final String _Authorization = "key=" + _KEY;
	public static final String _ContentType = "application/json";
	public static final String _BodySpta = "Anda mendapatkan spta baru dengan no : ";
	public static final String _BodyWbResult = "Hasil timbangan dengan no Spta : ";
	public static final String _BodyVisit = "Anda mendapatkan jadwal kunjungan baru dengan no : ";
	public static final String _BodyDelegasi = "Anda mendapatkan jadwal kunjungan baru dengan no : ";
	public static final String _Title = "RMI";
	
	public static final String _updateSpta = "http://api.rejosomanisindo.com:6026/caneservices/mobspta/update";
	public static final String _tokenSync = "http://api.rejosomanisindo.com:6026/tokenservices/getresttoken";
	
}
