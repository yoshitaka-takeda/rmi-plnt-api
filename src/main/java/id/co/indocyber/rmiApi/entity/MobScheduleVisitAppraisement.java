package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_schedule_visit_appraisement database table.
 * 
 */
@Entity
@Table(name="mob_schedule_visit_appraisement")
@NamedQuery(name="MobScheduleVisitAppraisement.findAll", query="SELECT m FROM MobScheduleVisitAppraisement m")
@IdClass(MobScheduleVisitAppraisementPK.class)
public class MobScheduleVisitAppraisement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="schedule_visit_detail_id")
	private int scheduleVisitDetailId;

	@Id
	@Column(name="period_name")
	private String periodName;

	@Column(name="current_sugarcane_height")
	private BigDecimal currentSugarcaneHeight;

	@Column(name="cutdown_sugarcane_height")
	private BigDecimal cutdownSugarcaneHeight;

	@Column(name="sugarcane_stem_weight")
	private BigDecimal sugarcaneStemWeight;

	@Column(name="weight_per_sugarcane")
	private BigDecimal weightPerSugarcane;
	
	@Column(name="number_of_stem_per_leng")
	private int numberOfStemPerLeng;

	@Column(name="leng_factor_per_HA")
	private int leng_factor_per_HA;

	@Column(name="number_of_sugarcane_per_HA")
	private BigDecimal number_of_sugarcane_per_HA;
	
	@Column(name="prod_per_hectar_KU")
	private BigDecimal prodPerHectarKU;
	
	@Column(name="amount_of_production")
	private BigDecimal amountOfProduction;
	
	@Column(name="cutting_schedule")
	private String cuttingSchedule;
	
	@Column(name="avg_brix_number")
	private BigDecimal avgBrixNumber;
	
	@Column(name="rendemen_estimation")
	private BigDecimal rendemenEstimation;
	
	@Column(name="ku_hablur_per_ha")
	private BigDecimal kuHablurPerHa;
	
	@Column(name="amount_of_hablur")
	private BigDecimal amountOfHablur;

	@Column(name="number_of_sugarcane_KU")
	private BigDecimal number_of_sugarcane_KU;

	@Column(name="appraisement")
	private BigDecimal appraisement;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	public MobScheduleVisitAppraisement() {
	}

	public BigDecimal getAppraisement() {
		return this.appraisement;
	}

	public void setAppraisement(BigDecimal appraisement) {
		this.appraisement = appraisement;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}
	
	

	public BigDecimal getWeightPerSugarcane() {
		return weightPerSugarcane;
	}

	public void setWeightPerSugarcane(BigDecimal weightPerSugarcane) {
		this.weightPerSugarcane = weightPerSugarcane;
	}

	public BigDecimal getProdPerHectarKU() {
		return prodPerHectarKU;
	}

	public void setProdPerHectarKU(BigDecimal prodPerHectarKU) {
		this.prodPerHectarKU = prodPerHectarKU;
	}

	public BigDecimal getAmountOfProduction() {
		return amountOfProduction;
	}

	public void setAmountOfProduction(BigDecimal amountOfProduction) {
		this.amountOfProduction = amountOfProduction;
	}

	public BigDecimal getKuHablurPerHa() {
		return kuHablurPerHa;
	}

	public void setKuHablurPerHa(BigDecimal kuHablurPerHa) {
		this.kuHablurPerHa = kuHablurPerHa;
	}

	public BigDecimal getAmountOfHablur() {
		return amountOfHablur;
	}

	public void setAmountOfHablur(BigDecimal amountOfHablur) {
		this.amountOfHablur = amountOfHablur;
	}

	public String getCuttingSchedule() {
		return cuttingSchedule;
	}

	public void setCuttingSchedule(String cuttingSchedule) {
		this.cuttingSchedule = cuttingSchedule;
	}

	public BigDecimal getAvgBrixNumber() {
		return avgBrixNumber;
	}

	public void setAvgBrixNumber(BigDecimal avgBrixNumber) {
		this.avgBrixNumber = avgBrixNumber;
	}

	public BigDecimal getRendemenEstimation() {
		return rendemenEstimation;
	}

	public void setRendemenEstimation(BigDecimal rendemenEstimation) {
		this.rendemenEstimation = rendemenEstimation;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCurrentSugarcaneHeight() {
		return this.currentSugarcaneHeight;
	}

	public void setCurrentSugarcaneHeight(BigDecimal currentSugarcaneHeight) {
		this.currentSugarcaneHeight = currentSugarcaneHeight;
	}

	public BigDecimal getCutdownSugarcaneHeight() {
		return this.cutdownSugarcaneHeight;
	}

	public void setCutdownSugarcaneHeight(BigDecimal cutdownSugarcaneHeight) {
		this.cutdownSugarcaneHeight = cutdownSugarcaneHeight;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public int getLeng_factor_per_HA() {
		return this.leng_factor_per_HA;
	}

	public void setLeng_factor_per_HA(int leng_factor_per_HA) {
		this.leng_factor_per_HA = leng_factor_per_HA;
	}

	public int getNumberOfStemPerLeng() {
		return this.numberOfStemPerLeng;
	}

	public void setNumberOfStemPerLeng(int numberOfStemPerLeng) {
		this.numberOfStemPerLeng = numberOfStemPerLeng;
	}

	public BigDecimal getNumber_of_sugarcane_KU() {
		return this.number_of_sugarcane_KU;
	}

	public void setNumber_of_sugarcane_KU(BigDecimal number_of_sugarcane_KU) {
		this.number_of_sugarcane_KU = number_of_sugarcane_KU;
	}

	public BigDecimal getNumber_of_sugarcane_per_HA() {
		return this.number_of_sugarcane_per_HA;
	}

	public void setNumber_of_sugarcane_per_HA(BigDecimal number_of_sugarcane_per_HA) {
		this.number_of_sugarcane_per_HA = number_of_sugarcane_per_HA;
	}

	public String getPeriodName() {
		return this.periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public int getScheduleVisitDetailId() {
		return this.scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(int scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public BigDecimal getSugarcaneStemWeight() {
		return this.sugarcaneStemWeight;
	}

	public void setSugarcaneStemWeight(BigDecimal sugarcaneStemWeight) {
		this.sugarcaneStemWeight = sugarcaneStemWeight;
	}

}