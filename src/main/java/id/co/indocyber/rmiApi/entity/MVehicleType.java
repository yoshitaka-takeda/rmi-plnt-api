package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the m_vehicle_type database table.
 * 
 */
@Entity
@Table(name="m_vehicle_type")
@NamedQuery(name="MVehicleType.findAll", query="SELECT m FROM MVehicleType m")
public class MVehicleType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Column(name="vehicle_type_name")
	private String vehicleTypeName;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="processed_by")
	private String processedBy;

	@Column(name="processed_date")
	private Date processedDate;

	public MVehicleType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getVehicleTypeName() {
		return this.vehicleTypeName;
	}

	public void setVehicleTypeName(String vehicleTypeName) {
		this.vehicleTypeName = vehicleTypeName;
	}

}