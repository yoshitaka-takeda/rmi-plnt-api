package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the mob_schedule_visit_to_farmer_header database table.
 * 
 */
@Entity
@Table(name="mob_schedule_visit_to_farmer_header")
@NamedQuery(name="MobScheduleVisitToFarmerHeader.findAll", query="SELECT m FROM MobScheduleVisitToFarmerHeader m")
public class MobScheduleVisitToFarmerHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="visit_no")
	private String visitNo;

	@Column(name="visit_date")
	private Date visitDate;

	@Column(name="visit_expired_date")
	private Date visitExpiredDate;

	@Column(name="farmer_id")
	private int farmerId;

	@Column(name="assign_to")
	private int assignTo;

	@Column(name="notes")
	private String notes;

	@Column(name="task_activity")
	private String taskActivity;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="modified_by")
	private String modifiedBy;

	@Column(name="modified_date")
	private Date modifiedDate;

	public MobScheduleVisitToFarmerHeader() {
	}

	public int getAssignTo() {
		return this.assignTo;
	}

	public void setAssignTo(int assignTo) {
		this.assignTo = assignTo;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getFarmerId() {
		return this.farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getTaskActivity() {
		return this.taskActivity;
	}

	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}

	public Date getVisitDate() {
		return this.visitDate;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	public Date getVisitExpiredDate() {
		return this.visitExpiredDate;
	}

	public void setVisitExpiredDate(Date visitExpiredDate) {
		this.visitExpiredDate = visitExpiredDate;
	}

	public String getVisitNo() {
		return this.visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

}