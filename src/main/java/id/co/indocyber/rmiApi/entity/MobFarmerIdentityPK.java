package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the mob_farmer_identity database table.
 * 
 */
@Embeddable
public class MobFarmerIdentityPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	
	private int farmerId;
	private String idCardType;
	public int getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}
	public String getIdCardType() {
		return idCardType;
	}
	public void setIdCardType(String idCardType) {
		this.idCardType = idCardType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + farmerId;
		result = prime * result
				+ ((idCardType == null) ? 0 : idCardType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobFarmerIdentityPK other = (MobFarmerIdentityPK) obj;
		if (farmerId != other.farmerId)
			return false;
		if (idCardType == null) {
			if (other.idCardType != null)
				return false;
		} else if (!idCardType.equals(other.idCardType))
			return false;
		return true;
	}
	
	
}