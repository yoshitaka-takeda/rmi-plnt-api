package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the stg_m_paddy_field database table.
 * 
 */
@Entity
@Table(name="stg_m_paddy_field")
@NamedQuery(name="StgMPaddyField.findAll", query="SELECT s FROM StgMPaddyField s")
public class StgMPaddyField implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="farm_code")
	private String farmCode;

	@Column(name="paddy_field_code")
	private String paddyFieldCode;

	@Column(name="line_num")
	private int lineNum;

	private String coordinat;

	@Column(name="field_area")
	private BigDecimal fieldArea;

	@Column(name="weight_estimation")
	private BigDecimal weightEstimation;

	@Column(name="sugarcane_type")
	private String sugarcaneType;

	@Column(name="age_of_sugarcane")
	private int ageOfSugarcane;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="planting_date")
	private Date plantingDate;

	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="sugarcane_species")
	private String sugarcaneSpecies;

	@Column(name="farmer_code")
	private String farmerCode;

	@Column(name="sugarcane_score")
	private BigDecimal sugarcaneScore;

	@Column(name="sugarcane_qty")
	private BigDecimal sugarcaneQty;

	@Column(name="planting_period")
	private String plantingPeriod;

	public StgMPaddyField() {
	}

	public int getAgeOfSugarcane() {
		return this.ageOfSugarcane;
	}

	public void setAgeOfSugarcane(int ageOfSugarcane) {
		this.ageOfSugarcane = ageOfSugarcane;
	}

	public String getCoordinat() {
		return this.coordinat;
	}

	public void setCoordinat(String coordinat) {
		this.coordinat = coordinat;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFarmCode() {
		return this.farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getFarmerCode() {
		return this.farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public BigDecimal getFieldArea() {
		return this.fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public int getLineNum() {
		return this.lineNum;
	}

	public void setLineNum(int lineNum) {
		this.lineNum = lineNum;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPaddyFieldCode() {
		return this.paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public Date getPlantingDate() {
		return this.plantingDate;
	}

	public void setPlantingDate(Date plantingDate) {
		this.plantingDate = plantingDate;
	}

	public String getPlantingPeriod() {
		return this.plantingPeriod;
	}

	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}

	public BigDecimal getSugarcaneQty() {
		return this.sugarcaneQty;
	}

	public void setSugarcaneQty(BigDecimal sugarcaneQty) {
		this.sugarcaneQty = sugarcaneQty;
	}

	public BigDecimal getSugarcaneScore() {
		return this.sugarcaneScore;
	}

	public void setSugarcaneScore(BigDecimal sugarcaneScore) {
		this.sugarcaneScore = sugarcaneScore;
	}

	public String getSugarcaneSpecies() {
		return this.sugarcaneSpecies;
	}

	public void setSugarcaneSpecies(String sugarcaneSpecies) {
		this.sugarcaneSpecies = sugarcaneSpecies;
	}

	public String getSugarcaneType() {
		return this.sugarcaneType;
	}

	public void setSugarcaneType(String sugarcaneType) {
		this.sugarcaneType = sugarcaneType;
	}

	public BigDecimal getWeightEstimation() {
		return this.weightEstimation;
	}

	public void setWeightEstimation(BigDecimal weightEstimation) {
		this.weightEstimation = weightEstimation;
	}

}