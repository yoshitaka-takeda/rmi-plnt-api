package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

public class MobVehiclePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String vehicleNo;

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((vehicleNo == null) ? 0 : vehicleNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobVehiclePK other = (MobVehiclePK) obj;
		if (vehicleNo == null) {
			if (other.vehicleNo != null)
				return false;
		} else if (!vehicleNo.equals(other.vehicleNo))
			return false;
		return true;
	}
}
