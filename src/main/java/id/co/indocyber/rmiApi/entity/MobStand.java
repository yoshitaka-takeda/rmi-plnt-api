package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_stand database table.
 * 
 */
@Entity
@Table(name="mob_stand")
@NamedQuery(name="MobStand.findAll", query="SELECT m FROM MobStand m")
public class MobStand implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="visit_no")
	private String visitNo;

	@Column(name="entry_date")
	private Date entryDate;

	@Column(name="farm_area")
	private BigDecimal farmArea;

	@Column(name="tonnage_estimation")
	private BigDecimal tonnageEstimation;

	@Column(name="last_stand_tonnage")
	private BigDecimal lastStandTonnage;

	@Column(name="last_stand_area")
	private BigDecimal lastStandArea;

	@Column(name="stand_tonnage")
	private BigDecimal standTonnage;

	@Column(name="stand_area")
	private BigDecimal standArea;

	@Column(name="activity_text")
	private String activityText;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	public MobStand() {
	}

	public String getVisitNo() {
		return this.visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public String getActivityText() {
		return this.activityText;
	}

	public void setActivityText(String activityText) {
		this.activityText = activityText;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public BigDecimal getFarmArea() {
		return this.farmArea;
	}

	public void setFarmArea(BigDecimal farmArea) {
		this.farmArea = farmArea;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getLastStandArea() {
		return this.lastStandArea;
	}

	public void setLastStandArea(BigDecimal lastStandArea) {
		this.lastStandArea = lastStandArea;
	}

	public BigDecimal getLastStandTonnage() {
		return this.lastStandTonnage;
	}

	public void setLastStandTonnage(BigDecimal lastStandTonnage) {
		this.lastStandTonnage = lastStandTonnage;
	}

	public BigDecimal getStandArea() {
		return this.standArea;
	}

	public void setStandArea(BigDecimal standArea) {
		this.standArea = standArea;
	}

	public BigDecimal getStandTonnage() {
		return this.standTonnage;
	}

	public void setStandTonnage(BigDecimal standTonnage) {
		this.standTonnage = standTonnage;
	}

	public BigDecimal getTonnageEstimation() {
		return this.tonnageEstimation;
	}

	public void setTonnageEstimation(BigDecimal tonnageEstimation) {
		this.tonnageEstimation = tonnageEstimation;
	}

}