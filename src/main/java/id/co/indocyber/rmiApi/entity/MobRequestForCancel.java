package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the mob_request_for_cancel database table.
 * 
 */
@Entity
@Table(name="mob_request_for_cancel")
@NamedQuery(name="MobRequestForCancel.findAll", query="SELECT m FROM MobRequestForCancel m")
public class MobRequestForCancel implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@Column(name="id")
	private int id;

	@Column(name="spta_num")
	private int sptaNum;

	@Column(name="location_for_cancel")
	private String locationForCancel;

	@Column(name="reason_for_cancel")
	private String reasonForCancel;

	@Column(name="task_activity")
	private String taskActivity;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	public MobRequestForCancel() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(int sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getLocationForCancel() {
		return locationForCancel;
	}

	public void setLocationForCancel(String locationForCancel) {
		this.locationForCancel = locationForCancel;
	}

	public String getReasonForCancel() {
		return reasonForCancel;
	}

	public void setReasonForCancel(String reasonForCancel) {
		this.reasonForCancel = reasonForCancel;
	}

	public String getTaskActivity() {
		return taskActivity;
	}

	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
}