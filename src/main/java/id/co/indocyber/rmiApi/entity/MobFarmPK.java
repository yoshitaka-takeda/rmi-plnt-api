package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the mob_farm database table.
 * 
 */
@Embeddable
public class MobFarmPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	
	private int farmerId;
	private String farmCode;
	
	public int getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}
	public String getFarmCode() {
		return farmCode;
	}
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((farmCode == null) ? 0 : farmCode.hashCode());
		result = prime * result + farmerId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobFarmPK other = (MobFarmPK) obj;
		if (farmCode == null) {
			if (other.farmCode != null)
				return false;
		} else if (!farmCode.equals(other.farmCode))
			return false;
		if (farmerId != other.farmerId)
			return false;
		return true;
	}

	
}