package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the stg_m_farm database table.
 * 
 */
@Entity
@Table(name="stg_m_farm")
@NamedQuery(name="StgMFarm.findAll", query="SELECT s FROM StgMFarm s")
public class StgMFarm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="farmer_code")
	private String farmerCode;

	@Id
	@Column(name="farm_code")
	private String farmCode;

	@Column(name="coordinat")
	private String coordinat;

	@Column(name="field_area")
	private BigDecimal fieldArea;

	@Column(name="village_name")
	private String villageName;

	@Column(name="farm_main_type")
	private String farmMainType;

	@Column(name="farm_sub_type")
	private String farmSubType;

	@Column(name="status")
	private String status;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="province")
	private String province;

	@Column(name="district")
	private String district;

	@Column(name="sub_district")
	private String subDistrict;

	@Column(name="province_code")
	private String provinceCode;

	@Column(name="sub_district_code")
	private String subDistrictCode;

	@Column(name="village_code")
	private String villageCode;

	@Column(name="district_code")
	private String districtCode;

	public StgMFarm() {
	}

	public String getCoordinat() {
		return this.coordinat;
	}

	public void setCoordinat(String coordinat) {
		this.coordinat = coordinat;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getDistrictCode() {
		return this.districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getFarmCode() {
		return this.farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getFarmMainType() {
		return this.farmMainType;
	}

	public void setFarmMainType(String farmMainType) {
		this.farmMainType = farmMainType;
	}

	public String getFarmSubType() {
		return this.farmSubType;
	}

	public void setFarmSubType(String farmSubType) {
		this.farmSubType = farmSubType;
	}

	public String getFarmerCode() {
		return this.farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public BigDecimal getFieldArea() {
		return this.fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubDistrict() {
		return this.subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getSubDistrictCode() {
		return this.subDistrictCode;
	}

	public void setSubDistrictCode(String subDistrictCode) {
		this.subDistrictCode = subDistrictCode;
	}

	public String getVillageCode() {
		return this.villageCode;
	}

	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}

	public String getVillageName() {
		return this.villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

}