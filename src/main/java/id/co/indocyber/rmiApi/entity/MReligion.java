package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * The persistent class for the m_religion database table.
 * 
 */
@Entity
@Table(name="m_religion")
@NamedQuery(name="MReligion.findAll", query="SELECT m FROM MReligion m")
public class MReligion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code")
	private String code;

	@Column(name="short_text")
	private String shortText;

	@Column(name="long_text")
	private String longText;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="processed_by")
	private String processedBy;

	@Column(name="processed_date")
	private Date processedDate;

	public MReligion() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getLongText() {
		return this.longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getShortText() {
		return this.shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

}