package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

public class MobScheduleVisitToFarmerHeaderPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String visitNo;

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((visitNo == null) ? 0 : visitNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobScheduleVisitToFarmerHeaderPK other = (MobScheduleVisitToFarmerHeaderPK) obj;
		if (visitNo == null) {
			if (other.visitNo != null)
				return false;
		} else if (!visitNo.equals(other.visitNo))
			return false;
		return true;
	}

}
