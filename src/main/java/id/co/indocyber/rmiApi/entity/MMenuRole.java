package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * The persistent class for the m_menu_role database table.
 * 
 */
@Entity
@Table(name="m_menu_role")
@NamedQuery(name="MMenuRole.findAll", query="SELECT m FROM MMenuRole m")
public class MMenuRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Column(name="role_id")
	private int roleId;

	@Column(name="menu_code")
	private String menuCode;

	@Column(name="is_view")
	private byte isView;

	@Column(name="is_add")
	private byte isAdd;

	@Column(name="is_edit")
	private byte isEdit;

	@Column(name="is_delete")
	private byte isDelete;

	@Column(name="is_import")
	private byte isImport;

	@Column(name="is_export")
	private byte isExport;

	@Column(name="is_download")
	private byte isDownload;

	@Column(name="is_print")
	private byte isPrint;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="created")
	private String created;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="modified")
	private String modified;

	@Column(name="modified_date")
	private Date modifiedDate;

	public MMenuRole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public byte getIsAdd() {
		return this.isAdd;
	}

	public void setIsAdd(byte isAdd) {
		this.isAdd = isAdd;
	}

	public byte getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(byte isDelete) {
		this.isDelete = isDelete;
	}

	public byte getIsDownload() {
		return this.isDownload;
	}

	public void setIsDownload(byte isDownload) {
		this.isDownload = isDownload;
	}

	public byte getIsEdit() {
		return this.isEdit;
	}

	public void setIsEdit(byte isEdit) {
		this.isEdit = isEdit;
	}

	public byte getIsExport() {
		return this.isExport;
	}

	public void setIsExport(byte isExport) {
		this.isExport = isExport;
	}

	public byte getIsImport() {
		return this.isImport;
	}

	public void setIsImport(byte isImport) {
		this.isImport = isImport;
	}

	public byte getIsPrint() {
		return this.isPrint;
	}

	public void setIsPrint(byte isPrint) {
		this.isPrint = isPrint;
	}

	public byte getIsView() {
		return this.isView;
	}

	public void setIsView(byte isView) {
		this.isView = isView;
	}

	public String getMenuCode() {
		return this.menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

}