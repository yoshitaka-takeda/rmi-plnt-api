package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * The persistent class for the mob_farmer_identity database table.
 * 
 */
@Entity
@Table(name="mob_farmer_identity")
@NamedQuery(name="MobFarmerIdentity.findAll", query="SELECT m FROM MobFarmerIdentity m")
@IdClass(MobFarmerIdentityPK.class)
public class MobFarmerIdentity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="farmer_id")
	private int farmerId;
	
	@Id
	@Column(name="idcard_type")
	private int idCardType;

	@Column(name="fullname")
	private String fullname;

	@Column(name="start_date")
	private Date startDate;

	@Column(name="end_date")
	private Date endDate;

	@Column(name="author")
	private String author;

	@Column(name="city")
	private String city;

	@Column(name="province")
	private String province;

	@Column(name="photo_text")
	private String photoText;

	@Column(name="person_type")
	private String personType;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="processed_by")
	private String processedBy;

	@Column(name="processed_date")
	private Date processedDate;

	public MobFarmerIdentity() {
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public int getIdCardType() {
		return idCardType;
	}

	public void setIdCardType(int idCardType) {
		this.idCardType = idCardType;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getPersonType() {
		return this.personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getPhotoText() {
		return this.photoText;
	}

	public void setPhotoText(String photoText) {
		this.photoText = photoText;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}