package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the mob_employee database table.
 * 
 */
@Entity
@Table(name = "mob_employee")
@NamedQuery(name = "MobEmployee.findAll", query = "SELECT m FROM MobEmployee m")
public class MobEmployee implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "fullname")
	private String fullname;
	
	@Column(name = "birthday")
	private Date birthday;
	
	@Column(name = "place_of_birthday")
	private String placeOfBirthday;
	
	@Column(name = "address_type")
	private String addressType;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "rtrw")
	private String rtrw;
	
	@Column(name = "village")
	private String village;
	
	@Column(name = "sub_district")
	private String subDistrict;
	
	@Column(name = "district")
	private String district;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "province")
	private String province;
	
	@Column(name = "zipcode")
	private String zipcode;
	
	@Column(name = "phone_num")
	private String phoneNum;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "marital_status")
	private String martialStatus;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "religion")
	private String religion;
	
	@Column(name = "last_education")
	private String lastEducation;
	
	@Column(name = "nipeg")
	private String nipeg;
	
	@Column(name = "joint_date")
	private Date jointDate;
	
	@Column(name = "end_date")
	private Date endDate;
	
	@Column(name = "manager")
	private String manager;
	
	@Column(name = "no_ktp")
	private String noKtp;
	
	@Column(name = "no_npwp")
	private String noNpwp;
	
	@Column(name = "area_name")
	private String areaName;
	
	@Column(name = "reference_code")
	private String referenceCode;
	
	@Column(name = "is_active")
	private byte isActive;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modified_by")
	private String modifiedBy;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public MobEmployee() {
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	public Date getBirthday() {
		return birthday;
	}
	
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public String getPlaceOfBirthday() {
		return placeOfBirthday;
	}
	
	public void setPlaceOfBirthday(String placeOfBirthday) {
		this.placeOfBirthday = placeOfBirthday;
	}
	
	public String getAddressType() {
		return addressType;
	}
	
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getRtrw() {
		return rtrw;
	}
	
	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}
	
	public String getVillage() {
		return village;
	}
	
	public void setVillage(String village) {
		this.village = village;
	}
	
	public String getSubDistrict() {
		return subDistrict;
	}
	
	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}
	
	public String getDistrict() {
		return district;
	}
	
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getZipcode() {
		return zipcode;
	}
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	public String getPhoneNum() {
		return phoneNum;
	}
	
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMartialStatus() {
		return martialStatus;
	}
	
	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getReligion() {
		return religion;
	}
	
	public void setReligion(String religion) {
		this.religion = religion;
	}
	
	public String getLastEducation() {
		return lastEducation;
	}
	
	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}
	
	public String getNipeg() {
		return nipeg;
	}
	
	public void setNipeg(String nipeg) {
		this.nipeg = nipeg;
	}
	
	public Date getJointDate() {
		return jointDate;
	}
	
	public void setJointDate(Date jointDate) {
		this.jointDate = jointDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getManager() {
		return manager;
	}
	
	public void setManager(String manager) {
		this.manager = manager;
	}
	
	public String getNoKtp() {
		return noKtp;
	}
	
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	
	public String getNoNpwp() {
		return noNpwp;
	}
	
	public void setNoNpwp(String noNpwp) {
		this.noNpwp = noNpwp;
	}
	
	public String getReferenceCode() {
		return referenceCode;
	}
	
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	
	public byte getIsActive() {
		return isActive;
	}
	
	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public Date getModifiedDate() {
		return modifiedDate;
	}
	
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getAreaName() {
		return areaName;
	}
	
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
}