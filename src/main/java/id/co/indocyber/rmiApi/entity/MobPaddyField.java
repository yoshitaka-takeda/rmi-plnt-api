package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_paddy_field database table.
 * 
 */
@Entity
@Table(name="mob_paddy_field")
@NamedQuery(name="MobPaddyField.findAll", query="SELECT m FROM MobPaddyField m")
public class MobPaddyField implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Column(name="farmer_id")
	private int farmerId;

	@Column(name="farm_code")
	private String farmCode;

	@Column(name="paddy_field_code")
	private String paddyFieldCode;

	@Column(name="line_num")
	private int lineNum;

	@Column(name="field_area")
	private BigDecimal fieldArea;

	@Column(name="latitude")
	private BigDecimal latitude;

	@Column(name="longitude")
	private BigDecimal longitude;

	@Column(name="weight_estimation")
	private BigDecimal weightEstimation;

	@Column(name="sugarcane_type")
	private String sugarcaneType;

	@Column(name="age_of_sugarcane")
	private int ageOfSugarcane;

	@Column(name="planting_date")
	private Date plantingDate;

	@Column(name="sugarcane_species")
	private String sugarcaneSpecies;

	@Column(name="sugarcane_score")
	private BigDecimal sugarcaneScore;

	@Column(name="sugarcane_qty")
	private BigDecimal sugarcaneQty;

	@Column(name="planting_period")
	private String plantingPeriod;
	
	@Column(name="weigh_estimation")
	private String weighEstimation;
	
	@Column(name="status")
	private String status;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="processed_by")
	private String processedBy;

	@Column(name="processed_date")
	private Date processedDate;

	public MobPaddyField() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAgeOfSugarcane() {
		return this.ageOfSugarcane;
	}

	public void setAgeOfSugarcane(int ageOfSugarcane) {
		this.ageOfSugarcane = ageOfSugarcane;
	}

	public String getFarmCode() {
		return this.farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public int getFarmerId() {
		return this.farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public BigDecimal getFieldArea() {
		return this.fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public int getLineNum() {
		return this.lineNum;
	}

	public void setLineNum(int lineNum) {
		this.lineNum = lineNum;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getPaddyFieldCode() {
		return this.paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public Date getPlantingDate() {
		return this.plantingDate;
	}

	public void setPlantingDate(Date plantingDate) {
		this.plantingDate = plantingDate;
	}

	public String getPlantingPeriod() {
		return this.plantingPeriod;
	}

	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public BigDecimal getSugarcaneQty() {
		return this.sugarcaneQty;
	}

	public void setSugarcaneQty(BigDecimal sugarcaneQty) {
		this.sugarcaneQty = sugarcaneQty;
	}

	public BigDecimal getSugarcaneScore() {
		return this.sugarcaneScore;
	}

	public void setSugarcaneScore(BigDecimal sugarcaneScore) {
		this.sugarcaneScore = sugarcaneScore;
	}

	public String getSugarcaneSpecies() {
		return this.sugarcaneSpecies;
	}

	public void setSugarcaneSpecies(String sugarcaneSpecies) {
		this.sugarcaneSpecies = sugarcaneSpecies;
	}

	public String getSugarcaneType() {
		return this.sugarcaneType;
	}

	public void setSugarcaneType(String sugarcaneType) {
		this.sugarcaneType = sugarcaneType;
	}

	public BigDecimal getWeightEstimation() {
		return this.weightEstimation;
	}

	public void setWeightEstimation(BigDecimal weightEstimation) {
		this.weightEstimation = weightEstimation;
	}
	
	public String getWeighEstimation() {
		return weighEstimation;
	}
	
	public void setWeighEstimation(String weighEstimation) {
		this.weighEstimation = weighEstimation;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

}