package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

public class MobScheduleVisitToFarmerHistoryPK implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	private String				visitNo;
	private int					farmerId;
	private int					assignTo;

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public int getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(int assignTo) {
		this.assignTo = assignTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + assignTo;
		result = prime * result + farmerId;
		result = prime * result + ((visitNo == null) ? 0 : visitNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobScheduleVisitToFarmerHistoryPK other = (MobScheduleVisitToFarmerHistoryPK) obj;
		if (assignTo != other.assignTo)
			return false;
		if (farmerId != other.farmerId)
			return false;
		if (visitNo == null) {
			if (other.visitNo != null)
				return false;
		} else if (!visitNo.equals(other.visitNo))
			return false;
		return true;
	}

}
