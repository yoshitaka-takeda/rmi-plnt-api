package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the m_task_activity database table.
 * 
 */
@Entity
@Table(name="m_task_activity")
@NamedQuery(name="MTaskActivity.findAll", query="SELECT m FROM MTaskActivity m")
@IdClass(MTaskActivityPK.class)
public class MTaskActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code")
	private String code;

	@Column(name="short_text")
	private String shortText;
	
	@Column(name="long_text")
	private String longText;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="processed_by")
	private String processedBy;

	@Column(name="processed_date")
	private Date processedDate;

	public MTaskActivity() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getLongText() {
		return this.longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getShortText() {
		return this.shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

}