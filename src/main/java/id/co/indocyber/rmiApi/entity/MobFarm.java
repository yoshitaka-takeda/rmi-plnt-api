package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_farm database table.
 * 
 */
@Entity
@Table(name="mob_farm")
@NamedQuery(name="MobFarm.findAll", query="SELECT m FROM MobFarm m")
@IdClass(MobFarmPK.class)
public class MobFarm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="farmer_id")
	private int farmerId;

	@Id
	@Column(name="farm_code")
	private String farmCode;

	@Column(name="field_area")
	private BigDecimal fieldArea;

	@Column(name="village")
	private String village;

	@Column(name="sub_district")
	private String subDistrict;

	@Column(name="district")
	private String district;

	@Column(name="province")
	private String province;

	@Column(name="latitude")
	private BigDecimal latitude;

	@Column(name="longitude")
	private BigDecimal longitude;

	@Column(name="farm_main_type")
	private String farmMainType;

	@Column(name="farm_sub_type")
	private String farmSubType;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="status")
	private String status;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="processed_by")
	private String processedBy;

	@Column(name="processed_date")
	private Date processedDate;

	public MobFarm() {
	}

	
	public int getFarmerId() {
		return farmerId;
	}


	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}


	public String getFarmCode() {
		return farmCode;
	}


	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}


	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getFarmMainType() {
		return this.farmMainType;
	}

	public void setFarmMainType(String farmMainType) {
		this.farmMainType = farmMainType;
	}

	public String getFarmSubType() {
		return this.farmSubType;
	}

	public void setFarmSubType(String farmSubType) {
		this.farmSubType = farmSubType;
	}

	public BigDecimal getFieldArea() {
		return this.fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubDistrict() {
		return this.subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getVillage() {
		return this.village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

}