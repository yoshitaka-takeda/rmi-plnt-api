package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;


public class MobScheduleVisitScoringPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int scheduleVisitDetailId;
	private String paramCode;
	public int getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}
	public void setScheduleVisitDetailId(int scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}
	public String getParamCode() {
		return paramCode;
	}
	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((paramCode == null) ? 0 : paramCode.hashCode());
		result = prime * result + scheduleVisitDetailId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobScheduleVisitScoringPK other = (MobScheduleVisitScoringPK) obj;
		if (paramCode == null) {
			if (other.paramCode != null)
				return false;
		} else if (!paramCode.equals(other.paramCode))
			return false;
		if (scheduleVisitDetailId != other.scheduleVisitDetailId)
			return false;
		return true;
	}

}
