package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;
/**
 * The persistent class for the m_user_role database table.
 * 
 */
@Entity
@Table(name = "m_user_role")
@NamedQuery(name = "MUserRole.findAll", query = "SELECT m FROM MUserRole m")
@IdClass(MUserRolePK.class)
public class MUserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "email")
	private String email;

	@Id
	@Column(name = "role_id")
	private int roleId;

	@Id
	@Column(name = "module_apps")
	private String moduleApps;

	@Column(name = "is_active")
	private byte isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	public MUserRole() {
	}

	

	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getModuleApps() {
		return moduleApps;
	}

	public void setModuleApps(String moduleApps) {
		this.moduleApps = moduleApps;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	

}