package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the m_user_role database table.
 * 
 */
@Embeddable
public class MUserRolePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String email;
	private int roleId;
	private String moduleApps;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getModuleApps() {
		return moduleApps;
	}
	public void setModuleApps(String moduleApps) {
		this.moduleApps = moduleApps;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((moduleApps == null) ? 0 : moduleApps.hashCode());
		result = prime * result + roleId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MUserRolePK other = (MUserRolePK) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (moduleApps == null) {
			if (other.moduleApps != null)
				return false;
		} else if (!moduleApps.equals(other.moduleApps))
			return false;
		if (roleId != other.roleId)
			return false;
		return true;
	}
	
}