package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the mob_spta database table.
 * 
 */
@Entity
@Table(name = "mob_spta")
@NamedQuery(name = "MobSpta.findAll", query = "SELECT m FROM MobSpta m")
public class MobSpta implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "spta_num")
	private Integer sptaNum;
	@Column(name = "farmer_code")
	private String farmerCode;
	@Column(name = "contract_no1")
	private String contractNo1;
	@Column(name = "contract_no2")
	private String contractNo2;
	@Column(name = "farmer_type")
	private String farmerType;
	@Column(name = "farm_code")
	private String farmCode;
	@Column(name = "coop_name")
	private String coopName;
	@Column(name = "qty")
	private BigDecimal qty;
	@Column(name = "status")
	private String status;
	@Column(name = "vehicle_no")
	private String vehicleNo;
	@Column(name = "driver_id")
	private Integer driverId;
	@Column(name = "vehicle_type")
	private Integer vehicleType;
	@Column(name = "document_date")
	private Date documentDate;
	@Column(name = "expired_date")
	private Date expiredDate;
	@Column(name = "monitoring_pos")
	private String monitoringPos;
	@Column(name = "rendemen")
	private BigDecimal rendemen;
	@Column(name = "brix")
	private BigDecimal brix;
	@Column(name = "pol")
	private BigDecimal pol;
	@Column(name = "source")
	private String source;
	@Column(name = "base_entry")
	private Integer baseEntry;
	@Column(name = "base_line")
	private Integer baseLine;
	@Column(name = "loading_sugarcane_latitude")
	private Double loadingSugarcaneLatitude;
	@Column(name = "loading_sugarcane_longitude")
	private Double loadingSugarcaneLongitude;
	@Column(name = "loading_sugarcane_date")
	private Date loadingSugarcaneDate;
	@Column(name = "loading_sugarcane_by")
	private String loadingSugarcaneBy;
	@Column(name = "destination_latitude")
	private Double destinationLatitude;
	@Column(name = "destination_longitude")
	private Double destinationLongitude;
	@Column(name = "destination_arrived_date")
	private Date destinationArrivedDate;
	@Column(name = "destination_arrived_by")
	private String destinationArrivedBy;
	@Column(name = "exit_latitude")
	private Double exitLatitude;
	@Column(name = "exit_longitude")
	private Double exitLongitude;
	@Column(name = "exit_date")
	private Date exitDate;
	@Column(name = "exit_by")
	private String exitBy;
	@Column(name = "reference_code")
	private String referenceCode;
	@Column(name = "is_active")
	private byte isActive;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public MobSpta() {
	}

	public Integer getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(Integer sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public String getContractNo1() {
		return contractNo1;
	}

	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}

	public String getContractNo2() {
		return contractNo2;
	}

	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}

	public String getFarmerType() {
		return farmerType;
	}

	public void setFarmerType(String farmerType) {
		this.farmerType = farmerType;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getCoopName() {
		return coopName;
	}

	public void setCoopName(String coopName) {
		this.coopName = coopName;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public Integer getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getMonitoringPos() {
		return monitoringPos;
	}

	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}

	public BigDecimal getRendemen() {
		return rendemen;
	}

	public void setRendemen(BigDecimal rendemen) {
		this.rendemen = rendemen;
	}

	public BigDecimal getBrix() {
		return brix;
	}

	public void setBrix(BigDecimal brix) {
		this.brix = brix;
	}

	public BigDecimal getPol() {
		return pol;
	}

	public void setPol(BigDecimal pol) {
		this.pol = pol;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getBaseEntry() {
		return baseEntry;
	}

	public void setBaseEntry(Integer baseEntry) {
		this.baseEntry = baseEntry;
	}

	public Integer getBaseLine() {
		return baseLine;
	}

	public void setBaseLine(Integer baseLine) {
		this.baseLine = baseLine;
	}

	public Double getLoadingSugarcaneLatitude() {
		return loadingSugarcaneLatitude;
	}

	public void setLoadingSugarcaneLatitude(Double loadingSugarcaneLatitude) {
		this.loadingSugarcaneLatitude = loadingSugarcaneLatitude;
	}

	public Double getLoadingSugarcaneLongitude() {
		return loadingSugarcaneLongitude;
	}

	public void setLoadingSugarcaneLongitude(Double loadingSugarcaneLongitude) {
		this.loadingSugarcaneLongitude = loadingSugarcaneLongitude;
	}

	public Date getLoadingSugarcaneDate() {
		return loadingSugarcaneDate;
	}

	public void setLoadingSugarcaneDate(Date loadingSugarcaneDate) {
		this.loadingSugarcaneDate = loadingSugarcaneDate;
	}

	public String getLoadingSugarcaneBy() {
		return loadingSugarcaneBy;
	}

	public void setLoadingSugarcaneBy(String loadingSugarcaneBy) {
		this.loadingSugarcaneBy = loadingSugarcaneBy;
	}

	public Double getDestinationLatitude() {
		return destinationLatitude;
	}

	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public Double getDestinationLongitude() {
		return destinationLongitude;
	}

	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public Date getDestinationArrivedDate() {
		return destinationArrivedDate;
	}

	public void setDestinationArrivedDate(Date destinationArrivedDate) {
		this.destinationArrivedDate = destinationArrivedDate;
	}

	public String getDestinationArrivedBy() {
		return destinationArrivedBy;
	}

	public void setDestinationArrivedBy(String destinationArrivedBy) {
		this.destinationArrivedBy = destinationArrivedBy;
	}

	public Double getExitLatitude() {
		return exitLatitude;
	}

	public void setExitLatitude(Double exitLatitude) {
		this.exitLatitude = exitLatitude;
	}

	public Double getExitLongitude() {
		return exitLongitude;
	}

	public void setExitLongitude(Double exitLongitude) {
		this.exitLongitude = exitLongitude;
	}

	public Date getExitDate() {
		return exitDate;
	}

	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}

	public String getExitBy() {
		return exitBy;
	}

	public void setExitBy(String exitBy) {
		this.exitBy = exitBy;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
}