package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * The persistent class for the m_sugarcane_param_scoring database table.
 * 
 */
@Entity
@Table(name = "m_sugarcane_param_scoring")
@NamedQuery(name = "MSugarcaneParamScoring.findAll", query = "SELECT m FROM MSugarcaneParamScoring m")
public class MSugarcaneParamScoring implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "param_code")
	private int paramCode;

	@Column(name = "param_value")
	private String paramValue;

	@Column(name = "criteria")
	private String criteria;

	@Column(name = "weight")
	private BigDecimal weight;

	@Column(name = "is_active")
	private byte isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	public MSugarcaneParamScoring() {
	}

	public int getParamCode() {
		return paramCode;
	}

	public void setParamCode(int paramCode) {
		this.paramCode = paramCode;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCriteria() {
		return this.criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public BigDecimal getWeight() {
		return this.weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

}