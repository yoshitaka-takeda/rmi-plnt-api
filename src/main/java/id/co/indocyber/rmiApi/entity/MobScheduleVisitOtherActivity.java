package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the mob_schedule_visit_other_activities database table.
 * 
 */
@Entity
@Table(name="mob_schedule_visit_other_activities")
@NamedQuery(name="MobScheduleVisitOtherActivity.findAll", query="SELECT m FROM MobScheduleVisitOtherActivity m")
@IdClass(MobScheduleVisitOtherActivityPK.class)
public class MobScheduleVisitOtherActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="visit_no")
	private String visitNo;

	@Column(name="entry_date")
	private Date entryDate;

	@Column(name="activity_text")
	private String activityText;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	public MobScheduleVisitOtherActivity() {
	}

	public String getActivityText() {
		return this.activityText;
	}

	public void setActivityText(String activityText) {
		this.activityText = activityText;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getVisitNo() {
		return this.visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

}