package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

public class MobScheduleVisitAppraisementPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int scheduleVisitDetailId;
	private String periodName;
	
	public int getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}
	public void setScheduleVisitDetailId(int scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}
	public String getPeriodName() {
		return periodName;
	}
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((periodName == null) ? 0 : periodName.hashCode());
		result = prime * result + scheduleVisitDetailId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobScheduleVisitAppraisementPK other = (MobScheduleVisitAppraisementPK) obj;
		if (periodName == null) {
			if (other.periodName != null)
				return false;
		} else if (!periodName.equals(other.periodName))
			return false;
		if (scheduleVisitDetailId != other.scheduleVisitDetailId)
			return false;
		return true;
	}

}
