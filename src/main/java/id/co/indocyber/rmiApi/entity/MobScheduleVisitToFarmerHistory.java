package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the mob_schedule_visit_to_farmer_history database
 * table.
 * 
 */
@Entity
@Table(name = "mob_schedule_visit_to_farmer_history")
@NamedQuery(name = "MobScheduleVisitToFarmerHistory.findAll", query = "SELECT m FROM MobScheduleVisitToFarmerHistory m")
@IdClass(MobScheduleVisitToFarmerHistoryPK.class)
public class MobScheduleVisitToFarmerHistory implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Id
	@Column(name = "visit_no")
	private String				visitNo;

	@Id
	@Column(name = "farmer_id")
	private int				farmerId;

	@Id
	@Column(name = "assign_to")
	private int					assignTo;

	@Column(name = "assign_date")
	private Date				assignDate;

	@Column(name = "is_active")
	private byte				isActive;

	@Column(name = "task_action")
	private String				taskAction;

	@Column(name = "created_by")
	private String				createdBy;

	@Column(name = "created_date")
	private Date				createdDate;

	public MobScheduleVisitToFarmerHistory() {
	}

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public int getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(int assignTo) {
		this.assignTo = assignTo;
	}

	public Date getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getTaskAction() {
		return taskAction;
	}

	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}