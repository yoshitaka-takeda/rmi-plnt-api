package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_schedule_visit_scoring database table.
 * 
 */
@Entity
@Table(name="mob_schedule_visit_scoring")
@NamedQuery(name="MobScheduleVisitScoring.findAll", query="SELECT m FROM MobScheduleVisitScoring m")
@IdClass(MobScheduleVisitScoringPK.class)
public class MobScheduleVisitScoring implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="schedule_visit_detail_id")
	private int scheduleVisitDetailId;
	
	@Id
	@Column(name="param_code")
	private String paramCode;

	@Column(name="param_value")
	private String paramValue;

	@Column(name="criteria")
	private String criteria;

	@Column(name="weight")
	private BigDecimal weight;

	@Column(name="score")
	private BigDecimal score;

	@Column(name="notes")
	private String notes;

	@Column(name="is_active")
	private byte isActive;
	
	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	public MobScheduleVisitScoring() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCriteria() {
		return this.criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getParamCode() {
		return this.paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public int getScheduleVisitDetailId() {
		return this.scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(int scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public BigDecimal getScore() {
		return this.score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public BigDecimal getWeight() {
		return this.weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

}