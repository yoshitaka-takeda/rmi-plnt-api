package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the stg_spta database table.
 * 
 */
@Entity
@Table(name="stg_spta")
@NamedQuery(name="StgSpta.findAll", query="SELECT s FROM StgSpta s")
public class StgSpta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="register_num")
	private int registerNum;

	@Column(name="base_entry")
	private int baseEntry;

	@Column(name="base_line")
	private int baseLine;

	private BigDecimal brix;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Temporal(TemporalType.DATE)
	@Column(name="document_date")
	private Date documentDate;

	@Column(name="driver_name")
	private String driverName;

	@Temporal(TemporalType.DATE)
	@Column(name="expired_date")
	private Date expiredDate;

	@Column(name="farmer_code")
	private String farmerCode;

	private BigDecimal pol;

	private BigDecimal qty;

	private BigDecimal rendemen;

	private String source;

	@Column(name="spta_num")
	private int sptaNum;

	private String status;

	@Column(name="vehicle_no")
	private String vehicleNo;

	@Column(name="vehicle_type")
	private String vehicleType;

	public StgSpta() {
	}

	public int getRegisterNum() {
		return this.registerNum;
	}

	public void setRegisterNum(int registerNum) {
		this.registerNum = registerNum;
	}

	public int getBaseEntry() {
		return this.baseEntry;
	}

	public void setBaseEntry(int baseEntry) {
		this.baseEntry = baseEntry;
	}

	public int getBaseLine() {
		return this.baseLine;
	}

	public void setBaseLine(int baseLine) {
		this.baseLine = baseLine;
	}

	public BigDecimal getBrix() {
		return this.brix;
	}

	public void setBrix(BigDecimal brix) {
		this.brix = brix;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDocumentDate() {
		return this.documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public String getDriverName() {
		return this.driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Date getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getFarmerCode() {
		return this.farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public BigDecimal getPol() {
		return this.pol;
	}

	public void setPol(BigDecimal pol) {
		this.pol = pol;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getRendemen() {
		return this.rendemen;
	}

	public void setRendemen(BigDecimal rendemen) {
		this.rendemen = rendemen;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getSptaNum() {
		return this.sptaNum;
	}

	public void setSptaNum(int sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVehicleNo() {
		return this.vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

}