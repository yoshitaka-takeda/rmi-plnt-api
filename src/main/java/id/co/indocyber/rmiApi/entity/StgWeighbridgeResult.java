package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the stg_weighbridge_result database table.
 * 
 */
@Entity
@Table(name="stg_weighbridge_result")
@NamedQuery(name="StgWeighbridgeResult.findAll", query="SELECT s FROM StgWeighbridgeResult s")
public class StgWeighbridgeResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	private short createTime;

	@Id
	private int docEntry;

	private int docNum;

	private String remark;

	@Column(name="U_SOL_BATCH")
	private String uSolBatch;

	@Column(name="U_SOL_CARDCODE")
	private String uSolCardcode;

	@Column(name="U_SOL_CARDNAME")
	private String uSolCardname;

	@Column(name="U_SOL_CHK_INSENTIFJARAK")
	private String uSolChkInsentifjarak;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="U_SOL_DT_LOAD1")
	private Date uSolDtLoad1;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="U_SOL_DT_LOAD2")
	private Date uSolDtLoad2;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="U_SOL_DT_MBS")
	private Date uSolDtMbs;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="U_SOL_DT_WEIGH1")
	private Date uSolDtWeigh1;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="U_SOL_DT_WEIGH2")
	private Date uSolDtWeigh2;

	@Column(name="U_SOL_INSENTIF1")
	private BigDecimal uSolInsentif1;

	@Column(name="U_SOL_INSENTIF2")
	private BigDecimal uSolInsentif2;

	@Column(name="U_SOL_INSENTIF3")
	private BigDecimal uSolInsentif3;

	@Column(name="U_SOL_INSENTIFJARAK")
	private String uSolInsentifjarak;

	@Column(name="U_SOL_ITEMCODE")
	private String uSolItemcode;

	@Column(name="U_SOL_ITEMNAME")
	private String uSolItemname;

	@Column(name="U_SOL_KAPAL")
	private String uSolKapal;

	@Column(name="U_SOL_KENDARAAN")
	private String uSolKendaraan;

	@Column(name="U_SOL_MBS_NO")
	private int uSolMbsNo;

	@Column(name="U_SOL_MBS_PERC")
	private BigDecimal uSolMbsPerc;

	@Column(name="U_SOL_NOPOL")
	private String uSolNopol;

	@Column(name="U_SOL_OPERATORW1")
	private String uSolOperatorw1;

	@Column(name="U_SOL_OPERATORW2")
	private String uSolOperatorw2;

	@Column(name="U_SOL_PERCENTAGE")
	private BigDecimal uSolPercentage;

	@Column(name="U_SOL_PO")
	private String uSolPo;

	@Column(name="U_SOL_QTY_SJ")
	private BigDecimal uSolQtySj;

	@Column(name="U_SOL_QTY_SPM")
	private BigDecimal uSolQtySpm;

	@Column(name="U_SOL_QTY_SPTA")
	private BigDecimal uSolQtySpta;

	@Column(name="U_SOL_QTY_WEIGH1")
	private BigDecimal uSolQtyWeigh1;

	@Column(name="U_SOL_QTY_WEIGH2")
	private BigDecimal uSolQtyWeigh2;

	@Column(name="U_SOL_QTY_WEIGHO")
	private BigDecimal uSolQtyWeigho;

	@Column(name="U_SOL_RAFAKSI")
	private BigDecimal uSolRafaksi;

	@Column(name="U_SOL_SO")
	private int uSolSo;

	@Column(name="U_SOL_SPPB")
	private int uSolSppb;

	@Column(name="U_SOL_SPT")
	private int uSolSpt;

	@Column(name="U_SOL_SPTA")
	private int uSolSpta;

	@Column(name="U_SOL_STATUS")
	private String uSolStatus;

	@Column(name="U_SOL_SUPIR")
	private String uSolSupir;

	@Column(name="U_SOL_SURATJALAN")
	private String uSolSuratjalan;

	@Column(name="U_SOL_T_LOAD1")
	private short uSolTLoad1;

	@Column(name="U_SOL_T_LOAD2")
	private short uSolTLoad2;

	@Column(name="U_SOL_T_MBS")
	private short uSolTMbs;

	@Column(name="U_SOL_T_WEIGH1")
	private short uSolTWeigh1;

	@Column(name="U_SOL_T_WEIGH2")
	private short uSolTWeigh2;

	@Column(name="U_SOL_TIMBANGPELABUHAN")
	private BigDecimal uSolTimbangpelabuhan;

	@Column(name="U_SOL_TIPE")
	private String uSolTipe;

	@Column(name="U_SOL_TIPE_TIMBANG")
	private String uSolTipeTimbang;

	@Column(name="U_SOL_TRANSPORTIR")
	private String uSolTransportir;

	public StgWeighbridgeResult() {
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public short getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(short createTime) {
		this.createTime = createTime;
	}

	public int getDocEntry() {
		return this.docEntry;
	}

	public void setDocEntry(int docEntry) {
		this.docEntry = docEntry;
	}

	public int getDocNum() {
		return this.docNum;
	}

	public void setDocNum(int docNum) {
		this.docNum = docNum;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUSolBatch() {
		return this.uSolBatch;
	}

	public void setUSolBatch(String uSolBatch) {
		this.uSolBatch = uSolBatch;
	}

	public String getUSolCardcode() {
		return this.uSolCardcode;
	}

	public void setUSolCardcode(String uSolCardcode) {
		this.uSolCardcode = uSolCardcode;
	}

	public String getUSolCardname() {
		return this.uSolCardname;
	}

	public void setUSolCardname(String uSolCardname) {
		this.uSolCardname = uSolCardname;
	}

	public String getUSolChkInsentifjarak() {
		return this.uSolChkInsentifjarak;
	}

	public void setUSolChkInsentifjarak(String uSolChkInsentifjarak) {
		this.uSolChkInsentifjarak = uSolChkInsentifjarak;
	}

	public Date getUSolDtLoad1() {
		return this.uSolDtLoad1;
	}

	public void setUSolDtLoad1(Date uSolDtLoad1) {
		this.uSolDtLoad1 = uSolDtLoad1;
	}

	public Date getUSolDtLoad2() {
		return this.uSolDtLoad2;
	}

	public void setUSolDtLoad2(Date uSolDtLoad2) {
		this.uSolDtLoad2 = uSolDtLoad2;
	}

	public Date getUSolDtMbs() {
		return this.uSolDtMbs;
	}

	public void setUSolDtMbs(Date uSolDtMbs) {
		this.uSolDtMbs = uSolDtMbs;
	}

	public Date getUSolDtWeigh1() {
		return this.uSolDtWeigh1;
	}

	public void setUSolDtWeigh1(Date uSolDtWeigh1) {
		this.uSolDtWeigh1 = uSolDtWeigh1;
	}

	public Date getUSolDtWeigh2() {
		return this.uSolDtWeigh2;
	}

	public void setUSolDtWeigh2(Date uSolDtWeigh2) {
		this.uSolDtWeigh2 = uSolDtWeigh2;
	}

	public BigDecimal getUSolInsentif1() {
		return this.uSolInsentif1;
	}

	public void setUSolInsentif1(BigDecimal uSolInsentif1) {
		this.uSolInsentif1 = uSolInsentif1;
	}

	public BigDecimal getUSolInsentif2() {
		return this.uSolInsentif2;
	}

	public void setUSolInsentif2(BigDecimal uSolInsentif2) {
		this.uSolInsentif2 = uSolInsentif2;
	}

	public BigDecimal getUSolInsentif3() {
		return this.uSolInsentif3;
	}

	public void setUSolInsentif3(BigDecimal uSolInsentif3) {
		this.uSolInsentif3 = uSolInsentif3;
	}

	public String getUSolInsentifjarak() {
		return this.uSolInsentifjarak;
	}

	public void setUSolInsentifjarak(String uSolInsentifjarak) {
		this.uSolInsentifjarak = uSolInsentifjarak;
	}

	public String getUSolItemcode() {
		return this.uSolItemcode;
	}

	public void setUSolItemcode(String uSolItemcode) {
		this.uSolItemcode = uSolItemcode;
	}

	public String getUSolItemname() {
		return this.uSolItemname;
	}

	public void setUSolItemname(String uSolItemname) {
		this.uSolItemname = uSolItemname;
	}

	public String getUSolKapal() {
		return this.uSolKapal;
	}

	public void setUSolKapal(String uSolKapal) {
		this.uSolKapal = uSolKapal;
	}

	public String getUSolKendaraan() {
		return this.uSolKendaraan;
	}

	public void setUSolKendaraan(String uSolKendaraan) {
		this.uSolKendaraan = uSolKendaraan;
	}

	public int getUSolMbsNo() {
		return this.uSolMbsNo;
	}

	public void setUSolMbsNo(int uSolMbsNo) {
		this.uSolMbsNo = uSolMbsNo;
	}

	public BigDecimal getUSolMbsPerc() {
		return this.uSolMbsPerc;
	}

	public void setUSolMbsPerc(BigDecimal uSolMbsPerc) {
		this.uSolMbsPerc = uSolMbsPerc;
	}

	public String getUSolNopol() {
		return this.uSolNopol;
	}

	public void setUSolNopol(String uSolNopol) {
		this.uSolNopol = uSolNopol;
	}

	public String getUSolOperatorw1() {
		return this.uSolOperatorw1;
	}

	public void setUSolOperatorw1(String uSolOperatorw1) {
		this.uSolOperatorw1 = uSolOperatorw1;
	}

	public String getUSolOperatorw2() {
		return this.uSolOperatorw2;
	}

	public void setUSolOperatorw2(String uSolOperatorw2) {
		this.uSolOperatorw2 = uSolOperatorw2;
	}

	public BigDecimal getUSolPercentage() {
		return this.uSolPercentage;
	}

	public void setUSolPercentage(BigDecimal uSolPercentage) {
		this.uSolPercentage = uSolPercentage;
	}

	public String getUSolPo() {
		return this.uSolPo;
	}

	public void setUSolPo(String uSolPo) {
		this.uSolPo = uSolPo;
	}

	public BigDecimal getUSolQtySj() {
		return this.uSolQtySj;
	}

	public void setUSolQtySj(BigDecimal uSolQtySj) {
		this.uSolQtySj = uSolQtySj;
	}

	public BigDecimal getUSolQtySpm() {
		return this.uSolQtySpm;
	}

	public void setUSolQtySpm(BigDecimal uSolQtySpm) {
		this.uSolQtySpm = uSolQtySpm;
	}

	public BigDecimal getUSolQtySpta() {
		return this.uSolQtySpta;
	}

	public void setUSolQtySpta(BigDecimal uSolQtySpta) {
		this.uSolQtySpta = uSolQtySpta;
	}

	public BigDecimal getUSolQtyWeigh1() {
		return this.uSolQtyWeigh1;
	}

	public void setUSolQtyWeigh1(BigDecimal uSolQtyWeigh1) {
		this.uSolQtyWeigh1 = uSolQtyWeigh1;
	}

	public BigDecimal getUSolQtyWeigh2() {
		return this.uSolQtyWeigh2;
	}

	public void setUSolQtyWeigh2(BigDecimal uSolQtyWeigh2) {
		this.uSolQtyWeigh2 = uSolQtyWeigh2;
	}

	public BigDecimal getUSolQtyWeigho() {
		return this.uSolQtyWeigho;
	}

	public void setUSolQtyWeigho(BigDecimal uSolQtyWeigho) {
		this.uSolQtyWeigho = uSolQtyWeigho;
	}

	public BigDecimal getUSolRafaksi() {
		return this.uSolRafaksi;
	}

	public void setUSolRafaksi(BigDecimal uSolRafaksi) {
		this.uSolRafaksi = uSolRafaksi;
	}

	public int getUSolSo() {
		return this.uSolSo;
	}

	public void setUSolSo(int uSolSo) {
		this.uSolSo = uSolSo;
	}

	public int getUSolSppb() {
		return this.uSolSppb;
	}

	public void setUSolSppb(int uSolSppb) {
		this.uSolSppb = uSolSppb;
	}

	public int getUSolSpt() {
		return this.uSolSpt;
	}

	public void setUSolSpt(int uSolSpt) {
		this.uSolSpt = uSolSpt;
	}

	public int getUSolSpta() {
		return this.uSolSpta;
	}

	public void setUSolSpta(int uSolSpta) {
		this.uSolSpta = uSolSpta;
	}

	public String getUSolStatus() {
		return this.uSolStatus;
	}

	public void setUSolStatus(String uSolStatus) {
		this.uSolStatus = uSolStatus;
	}

	public String getUSolSupir() {
		return this.uSolSupir;
	}

	public void setUSolSupir(String uSolSupir) {
		this.uSolSupir = uSolSupir;
	}

	public String getUSolSuratjalan() {
		return this.uSolSuratjalan;
	}

	public void setUSolSuratjalan(String uSolSuratjalan) {
		this.uSolSuratjalan = uSolSuratjalan;
	}

	public short getUSolTLoad1() {
		return this.uSolTLoad1;
	}

	public void setUSolTLoad1(short uSolTLoad1) {
		this.uSolTLoad1 = uSolTLoad1;
	}

	public short getUSolTLoad2() {
		return this.uSolTLoad2;
	}

	public void setUSolTLoad2(short uSolTLoad2) {
		this.uSolTLoad2 = uSolTLoad2;
	}

	public short getUSolTMbs() {
		return this.uSolTMbs;
	}

	public void setUSolTMbs(short uSolTMbs) {
		this.uSolTMbs = uSolTMbs;
	}

	public short getUSolTWeigh1() {
		return this.uSolTWeigh1;
	}

	public void setUSolTWeigh1(short uSolTWeigh1) {
		this.uSolTWeigh1 = uSolTWeigh1;
	}

	public short getUSolTWeigh2() {
		return this.uSolTWeigh2;
	}

	public void setUSolTWeigh2(short uSolTWeigh2) {
		this.uSolTWeigh2 = uSolTWeigh2;
	}

	public BigDecimal getUSolTimbangpelabuhan() {
		return this.uSolTimbangpelabuhan;
	}

	public void setUSolTimbangpelabuhan(BigDecimal uSolTimbangpelabuhan) {
		this.uSolTimbangpelabuhan = uSolTimbangpelabuhan;
	}

	public String getUSolTipe() {
		return this.uSolTipe;
	}

	public void setUSolTipe(String uSolTipe) {
		this.uSolTipe = uSolTipe;
	}

	public String getUSolTipeTimbang() {
		return this.uSolTipeTimbang;
	}

	public void setUSolTipeTimbang(String uSolTipeTimbang) {
		this.uSolTipeTimbang = uSolTipeTimbang;
	}

	public String getUSolTransportir() {
		return this.uSolTransportir;
	}

	public void setUSolTransportir(String uSolTransportir) {
		this.uSolTransportir = uSolTransportir;
	}

}