package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the mob_schedule_visit_to_farmer_detail database
 * table.
 * 
 */
@Entity
@Table(name = "mob_schedule_visit_to_farmer_detail")
@NamedQuery(name = "MobScheduleVisitToFarmerDetail.findAll", query = "SELECT m FROM MobScheduleVisitToFarmerDetail m")
public class MobScheduleVisitToFarmerDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "visit_no")
	private String visitNo;

	@Column(name = "farm_code")
	private String farmCode;

	@Column(name = "paddy_field_code")
	private String paddyFieldCode;

	@Column(name = "visit_start_date")
	private Date visitStartDate;

	@Column(name = "visit_end_date")
	private Date visitEndDate;

	@Column(name = "longitude")
	private Double longitude;

	@Column(name = "latitude")
	private Double latitude;

	@Column(name = "estimate_harvest_time")
	private Date estimateHarvestTime;

	@Column(name = "plan_text")
	private String planText;

	@Column(name = "evaluation")
	private String evaluationText;

	@Column(name = "task_activity")
	private String taskActivity;

	@Column(name = "is_active")
	private byte isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	public MobScheduleVisitToFarmerDetail() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public Date getVisitStartDate() {
		return visitStartDate;
	}

	public void setVisitStartDate(Date visitStartDate) {
		this.visitStartDate = visitStartDate;
	}

	public Date getVisitEndDate() {
		return visitEndDate;
	}

	public void setVisitEndDate(Date visitEndDate) {
		this.visitEndDate = visitEndDate;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Date getEstimateHarvestTime() {
		return estimateHarvestTime;
	}

	public void setEstimateHarvestTime(Date estimateHarvestTime) {
		this.estimateHarvestTime = estimateHarvestTime;
	}

	public String getPlanText() {
		return planText;
	}

	public void setPlanText(String planText) {
		this.planText = planText;
	}

	public String getEvaluationText() {
		return evaluationText;
	}

	public void setEvaluationText(String evaluationText) {
		this.evaluationText = evaluationText;
	}

	public String getTaskActivity() {
		return taskActivity;
	}

	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}