package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the mob_weighbridge_result database table.
 * 
 */
@Embeddable
public class MobWeighbridgeResultPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobWeighbridgeResultPK other = (MobWeighbridgeResultPK) obj;
		if (id != other.id)
			return false;
		return true;
	}

}