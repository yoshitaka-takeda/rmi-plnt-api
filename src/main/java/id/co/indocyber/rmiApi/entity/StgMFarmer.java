package id.co.indocyber.rmiApi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Date;


/**
 * The persistent class for the stg_m_farmer database table.
 * 
 */
@Entity
@Table(name="stg_m_farmer")
@NamedQuery(name="StgMFarmer.findAll", query="SELECT s FROM StgMFarmer s")
public class StgMFarmer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="register_num")
	private String registerNum;

	@Column(name="fullname")
	private String fullname;

	@Column(name="address")
	private String address;

	@Column(name="rtrw")
	private String rtrw;

	@Column(name="city")
	private String city;

	@Column(name="ktp_no")
	private String ktpNo;

	@Column(name="npwp_no")
	private String npwpNo;

	@Column(name="npwp_name")
	private String npwpName;

	@Column(name="phone_num")
	private String phoneNum;

	@Column(name="kk_no")
	private String kkNo;

	@Column(name="farmer_village_name")
	private String farmerVillageName;

	@Column(name="farmer_sub_distric_name")
	private String farmerSubDistricName;

	@Column(name="farmer_province_name")
	private String farmerProvinceName;

	@Column(name="status")
	private String status;

	@Column(name="gender")
	private String gender;

	@Column(name="religion")
	private String religion;

	@Column(name="farm_type")
	private String farmType;

	@Column(name="business_partner_code")
	private String businessPartnerCode;

	@Column(name="farmer_type")
	private String farmerType;

	@Column(name="last_education")
	private String lastEducation;

	@Column(name="address_type")
	private String addressType;

	@Column(name="field_total")
	private BigDecimal fieldTotal;

	@Column(name="fund")
	private String fund;

	@Column(name="birthday")
	private Date birthday;

	@Column(name="place_of_birthday")
	private String placeOfBirthday;

	@Column(name="parent_farmer_name")
	private String parentFarmerName;

	@Column(name="parent_farmer_district_name")
	private String parentFarmerDistrictName;

	@Column(name="parent_farmer_province_code")
	private String parentFarmerProvinceCode;

	@Column(name="parent_farmer_sub_distric_code")
	private String parentFarmerSubDistricCode;

	@Column(name="parent_farmer_district_code")
	private String parentFarmerDistrictCode;

	@Column(name="parent_farmer_village_code")
	private String parentFarmerVillageCode;

	@Column(name="bank_account")
	private String bankAccount;

	@Column(name="bank_account_name")
	private String bankAccountName;

	@Column(name="bank_name")
	private String bankName;

	@Column(name="parent_farmer_zipcode")
	private String parentFarmerZipcode;

	public StgMFarmer() {
	}

	public String getRegisterNum() {
		return this.registerNum;
	}

	public void setRegisterNum(String registerNum) {
		this.registerNum = registerNum;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressType() {
		return this.addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getBankAccount() {
		return this.bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getBankAccountName() {
		return this.bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getBusinessPartnerCode() {
		return this.businessPartnerCode;
	}

	public void setBusinessPartnerCode(String businessPartnerCode) {
		this.businessPartnerCode = businessPartnerCode;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFarmType() {
		return this.farmType;
	}

	public void setFarmType(String farmType) {
		this.farmType = farmType;
	}

	public String getFarmerProvinceName() {
		return this.farmerProvinceName;
	}

	public void setFarmerProvinceName(String farmerProvinceName) {
		this.farmerProvinceName = farmerProvinceName;
	}

	public String getFarmerSubDistricName() {
		return this.farmerSubDistricName;
	}

	public void setFarmerSubDistricName(String farmerSubDistricName) {
		this.farmerSubDistricName = farmerSubDistricName;
	}

	public String getFarmerType() {
		return this.farmerType;
	}

	public void setFarmerType(String farmerType) {
		this.farmerType = farmerType;
	}

	public String getFarmerVillageName() {
		return this.farmerVillageName;
	}

	public void setFarmerVillageName(String farmerVillageName) {
		this.farmerVillageName = farmerVillageName;
	}

	public BigDecimal getFieldTotal() {
		return this.fieldTotal;
	}

	public void setFieldTotal(BigDecimal fieldTotal) {
		this.fieldTotal = fieldTotal;
	}

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getFund() {
		return this.fund;
	}

	public void setFund(String fund) {
		this.fund = fund;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getKkNo() {
		return this.kkNo;
	}

	public void setKkNo(String kkNo) {
		this.kkNo = kkNo;
	}

	public String getKtpNo() {
		return this.ktpNo;
	}

	public void setKtpNo(String ktpNo) {
		this.ktpNo = ktpNo;
	}

	public String getLastEducation() {
		return this.lastEducation;
	}

	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}

	public String getNpwpName() {
		return this.npwpName;
	}

	public void setNpwpName(String npwpName) {
		this.npwpName = npwpName;
	}

	public String getNpwpNo() {
		return this.npwpNo;
	}

	public void setNpwpNo(String npwpNo) {
		this.npwpNo = npwpNo;
	}

	public String getParentFarmerDistrictCode() {
		return this.parentFarmerDistrictCode;
	}

	public void setParentFarmerDistrictCode(String parentFarmerDistrictCode) {
		this.parentFarmerDistrictCode = parentFarmerDistrictCode;
	}

	public String getParentFarmerDistrictName() {
		return this.parentFarmerDistrictName;
	}

	public void setParentFarmerDistrictName(String parentFarmerDistrictName) {
		this.parentFarmerDistrictName = parentFarmerDistrictName;
	}

	public String getParentFarmerName() {
		return this.parentFarmerName;
	}

	public void setParentFarmerName(String parentFarmerName) {
		this.parentFarmerName = parentFarmerName;
	}

	public String getParentFarmerProvinceCode() {
		return this.parentFarmerProvinceCode;
	}

	public void setParentFarmerProvinceCode(String parentFarmerProvinceCode) {
		this.parentFarmerProvinceCode = parentFarmerProvinceCode;
	}

	public String getParentFarmerSubDistricCode() {
		return this.parentFarmerSubDistricCode;
	}

	public void setParentFarmerSubDistricCode(String parentFarmerSubDistricCode) {
		this.parentFarmerSubDistricCode = parentFarmerSubDistricCode;
	}

	public String getParentFarmerVillageCode() {
		return this.parentFarmerVillageCode;
	}

	public void setParentFarmerVillageCode(String parentFarmerVillageCode) {
		this.parentFarmerVillageCode = parentFarmerVillageCode;
	}

	public String getParentFarmerZipcode() {
		return this.parentFarmerZipcode;
	}

	public void setParentFarmerZipcode(String parentFarmerZipcode) {
		this.parentFarmerZipcode = parentFarmerZipcode;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPlaceOfBirthday() {
		return this.placeOfBirthday;
	}

	public void setPlaceOfBirthday(String placeOfBirthday) {
		this.placeOfBirthday = placeOfBirthday;
	}

	public String getReligion() {
		return this.religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getRtrw() {
		return this.rtrw;
	}

	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}